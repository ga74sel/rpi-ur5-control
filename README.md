# rpi-ur5-control

Control framework for controlling an industrial 6DOF robot (UR5) with a cheap embedded system (rpi3).
Communication between computer, robot and rpi is done via ROS.  
The rpi offers a stack of controllers that can be switched during runtime (joint space controllers, cartesian space controllers).  
The control loop can be closed at 1KHz when using ethernet connections.

![](picture.png "Architecture")

## Documentation
Contains the report and papers.
For more details see [report](documentation/report.pdf)

## Robot Modeling
Contains the Matlab Code that can be used to automatically generate and export the dynamic and kinematic model of a robot.
For more details see [ReadMe](robot_modeling/readme.md)

## Code
Contains the C++ code that is running on the differnt plattforms.
For more details see [ReadMe](code/readme.md)
