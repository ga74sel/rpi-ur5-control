#include "controllers/cartesian_position_controller.h"

#include "controllers/cartesian_controller.h"

#include <controller_utilities/load_param.h>

#include <controller_utilities/math.h>

using namespace config;
using namespace controller;
using namespace controller_msgs;

CartesianPositionController::CartesianPositionController(model_interface::ModelBase& model, std::string name, bool publish_data)
    : ControllerBase(name, CARTESIAN ),
      model(model),
      is_publish_data(publish_data),
      Kd(Matrix6::Identity()),
      J(Matrix6::Zero()),
      Jp(Matrix6::Zero()),
      T_0(Matrix4::Zero()),
      I6(Matrix6::Identity()),
      N2(Matrix6::Identity()),
      I3(Matrix3::Identity()),
      lamda_sq(0.05),
      is_cmd_updated(false),
      mode(BREAK)
{
}

CartesianPositionController::~CartesianPositionController()
{
}

/* solve dampled least squares problem */
void CartesianPositionController::setLamda( config::scalar_t lamda_sq )
{
    this->lamda_sq = lamda_sq;
}

/* gain Kp */
void CartesianPositionController::setKp(const config::Matrix6& Kp)
{
    ref1.set_Kp(Kp.topLeftCorner(3, 3));
    ref1.set_Kp(Kp.bottomRightCorner(3, 3));
}
void CartesianPositionController::setKp(const config::scalar_t& kp)
{
    ref1.set_Kp(kp);
    ref2.set_Kp(kp);
}
config::Matrix6 CartesianPositionController::getKp() const
{
    return Matrix6::Identity();
}

/* gain Kd */
void CartesianPositionController::setKi(const config::Matrix6& Ki)
{
    ref1.set_Ki(Ki.topLeftCorner(3, 3));
    ref2.set_Ki(Ki.bottomRightCorner(3, 3));
}
void CartesianPositionController::setKi(const config::scalar_t& ki)
{
    ref1.set_Ki(ki);
    ref2.set_Ki(ki);
}
config::Matrix6 CartesianPositionController::getKi() const
{
    return Matrix6::Identity();
}

/* load gain values from ros_parameters service */
bool CartesianPositionController::loadGains(const std::string& ns)
{
    bool ret = true;

    // load adaptive controller gains
    if( model.init() ) {
        Yr = model.init_Yr();
        th = model.init_th();
    } else {
        ret = false; ROS_ERROR_STREAM(ControllerBase::get_name() << " error init model");
    }

    // load gains
    std::vector<double> vec_p, vec_i, vec_d;
    Matrix6 Kp, Ki;
    std::string err_str;
    if( load_param::load_param_vec(ns, "gains_p", vec_p, config::NJOINT, err_str) &&
        load_param::load_param_vec(ns, "gains_i", vec_i, config::NJOINT, err_str) &&
        load_param::load_param_vec(ns, "gains_d", vec_d, config::NJOINT, err_str) ) {
        Kp.setZero(); Ki.setZero();
        for(int i = 0; i < config::NJOINT; ++i) {
            Kp(i, i) = scalar_t(vec_p[i]);
            Ki(i, i) = scalar_t(vec_i[i]);
            Kd(i, i) = scalar_t(vec_d[i]);
        }
        ref1.set_Kp(Kp.topLeftCorner(3,3)); ref1.set_Ki(Ki.topLeftCorner(3,3));
        ref2.set_Kp(Kp.bottomRightCorner(3,3)); ref2.set_Ki(Ki.bottomRightCorner(3,3));
    } else {
        ret = false; ROS_ERROR_STREAM(ControllerBase::get_name() << " error setting gains: " << err_str);
    }

    return ret;
}

/* init controller parameters */
bool CartesianPositionController::init(ros::NodeHandle& nh)
{
    bool ret = true;

    // load gain values from rosparameter service ( print them )
    if( loadGains(ControllerBase::get_name()) ) {
        std::cout << ControllerBase::get_name() << " Kd=" << std::endl << Kd << std::endl;
        std::cout << ControllerBase::get_name() << " Kp1=" << std::endl << ref1.get_Kp() << std::endl;
        std::cout << ControllerBase::get_name() << " Ki1=" << std::endl << ref1.get_Ki() << std::endl;
        std::cout << ControllerBase::get_name() << " Kp2=" << std::endl << ref2.get_Kp() << std::endl;
        std::cout << ControllerBase::get_name() << " Ki2=" << std::endl << ref2.get_Ki() << std::endl;
    } else ret = false;

    // advertise services
    set_command_srv = nh.advertiseService(ControllerBase::get_name() + "/set_command", &CartesianPositionController::setCommandHandler, this);
    set_mode_srv = nh.advertiseService(ControllerBase::get_name() + "/set_mode", &CartesianPositionController::setModeHandler, this);
    reload_gains_srv = nh.advertiseService(ControllerBase::get_name() + "/reloadGains", &CartesianPositionController::relaodGainsHandler, this);
    info_srv = nh.advertiseService(ControllerBase::get_name() + "/info", &CartesianPositionController::infoHandler, this);

    // publish data if is_publishdata is set
    if( is_publish_data ) {
        controllerdata_pub = nh.advertise<controller_msgs::controllerdata>(ControllerBase::get_name() + "/controllerdata", 1);
    }
    return ret;
}

/* called before first time to update */
void CartesianPositionController::start(const ros::Time& time)
{
    state_des.setZero();

    ref1.reset_integral();
    ref2.reset_integral();
    is_cmd_updated = false;
    set_cmd_time = time;
}

 /* called after last call to update */
void CartesianPositionController::stop(const ros::Time& time)
{
}

/* periodically called */
VectorDOF CartesianPositionController::update(robot_msgs::JointState& state, const ros::Time& time, const ros::Duration& dt)
{
    Vector3 X1dpos;                         // pos
    Vector3 X1dp, X2dp;                     // velo
    Vector3 X1dpp, X2dpp;                   // acc
    VectorDOF tau1, tau2, tau, Sq;          // torqes

    Vector3 Xpos, Xp1, Xp2, Xrp, Xrpp, DeltaX1, DeltaXp1, DeltaX2, DeltaXp2;   // controller

    MatrixJ J1, J2;
    MatrixJ J1p, J2p;
    MatrixJinv J1_inv, J2_inv;

    Quaternion q2d;
    Matrix3 R2, R2d;

    // switch to break if no new update is recived
    if( (time - set_cmd_time).toSec() > 0.5)
        is_cmd_updated = false;
    // remain in break until first command arrived
    Mode mode_ = mode;
    if(!is_cmd_updated) {
        mode_ = BREAK;
    }

    // desired position, velo, accel for task 1 & 2 wrt base frame
    X1dpos = state_des.D.head(3);           // position 
    X1dp = state_des.Dp.head(3);            // linear velocity
    X1dpp = state_des.Dpp.head(3);          // linear acceleration

    q2d.coeffs() = state_des.D.tail(4);     // orientation
    R2d = q2d.toRotationMatrix();
    X2dp = state_des.Dp.tail(3);            // angular velocity
    X2dpp = state_des.Dpp.tail(3);          // angular acceleration

    // ------------------------------------------------------------
    // TASK 1 - ef position control
    // ------------------------------------------------------------

    // current ef postion wrt base frame
    model.T_ef(T_0, state.Q);
    Xpos = T_0.topRightCorner(3,1);

    // current ef velocity wrt base frame
    model.J_ef(J, state.Q);
    J1 = J.topRows(3);
    Xp1 = J1*state.Qp;

    // error signal, ef position
    DeltaX1 = Xpos - X1dpos;
    DeltaXp1 = Xp1 - X1dp;

    // reference, Xd -> Xr
    Xrp = ref1.refp(X1dp, DeltaX1, dt, mode_);
    Xrpp = ref1.refpp(X1dpp, DeltaX1, DeltaXp1, dt, mode_);

    // transform back to joint space, Xr -> Qr
    J1_inv = J1.transpose()*(J1*J1.transpose() + lamda_sq * I3).inverse();
    model.Jp_ef(Jp, state.Q, state.Qp);
    J1p = Jp.topRows(3);

    state.Qrp = J1_inv*Xrp;
    state.Qrpp = J1_inv*( Xrpp - J1p * state.Qrp );

    // compute torque, Qrp -> tau
    Sq = J1_inv*(Xp1 - Xrp);

    if( Sq.norm() > 1e-3 )
        th -= scalar_t(dt.toSec()) * Yr.transpose() * Sq;
    model.Yr(Yr, state.Q, state.Qp, state.Qrp, state.Qrpp);

    tau1 = -Kd*Sq + Yr*th; // gravity is compensated

    // ------------------------------------------------------------
    // TASK 2 - ef orientation control
    // ------------------------------------------------------------

    // current ef orientation wrt base frame
    R2 = T_0.topLeftCorner(3,3);

    // current ef velocity wrt base frame
    J2 = J.bottomRows(3);
    Xp2 = J2*state.Qp;

    // error signal, ef orientation
    DeltaX2 << -scalar_t(0.5)*( R2.col(0).cross(R2d.col(0)) + R2.col(1).cross(R2d.col(1)) + R2.col(2).cross(R2d.col(2)) );
    // error signal time derivtive
    DeltaXp2 = Xp2 - X2dp;

    // reference, Xd -> Xr
    Xrp = ref2.refp(X2dp, DeltaX2, dt, mode_);
    Xrpp = ref2.refpp(X2dpp, DeltaX2, DeltaXp2, dt, mode_);

    // transform back to joint space, Xr -> Qr
    J2_inv = J2.transpose()*(J2*J2.transpose() + lamda_sq*I3).inverse();
    J2p = Jp.bottomRows(3);

    state.Qrp = J2_inv*Xrp;
    state.Qrpp = J2_inv*( Xrpp - J2p*state.Qrp );

    // compute torque, Qrp -> tau
    Sq = J2_inv*(Xp2 - Xrp);
    tau2 = -Kd*Sq;

    // ------------------------------------------------------------
    // Projector
    // ------------------------------------------------------------
    N2 = I6 - J1.transpose()*J1_inv.transpose();
    tau = tau1 + N2*tau2;

    // publish internal controllerdata (debug)
    if( is_publish_data ) {
        controller_msgs::controllerdata msg;
        Quaternion q(R2);
        Vector6 Xp, E, Ep, Ei;
        Vector7 X;

        X << Xpos, q.coeffs();
        Xp << Xp1, Xp2;
        E << DeltaX1, DeltaX2;
        Ep << DeltaXp1, DeltaXp2;
        Ei << ref1.get_integral(), ref2.get_integral();

        msg.header.stamp = time;
        msg.D = std::vector<scalar_t>(state_des.D.data(), state_des.D.data() + state_des.D.size());
        msg.Dp = std::vector<scalar_t>(state_des.Dp.data(), state_des.Dp.data() + state_des.Dp.size());
        msg.X = std::vector<scalar_t>(X.data(), X.data() + X.size());
        msg.Xp = std::vector<scalar_t>(Xp.data(), Xp.data() + Xp.size());
        msg.E = std::vector<scalar_t>(E.data(), E.data() + E.size());
        msg.Ep = std::vector<scalar_t>(Ep.data(), Ep.data() + Ep.size());
        msg.Ei = std::vector<scalar_t>(Ei.data(), Ei.data() + Ei.size());
        msg.Out = std::vector<scalar_t>(tau.data(), tau.data() + tau.size());
        controllerdata_pub.publish(msg);
    }
    return tau;
}

/* service handler */
bool CartesianPositionController::setCommandHandler(robot_msgs::writecontroldataRequest& req, robot_msgs::writecontroldataResponse& res)
{
    // look srv
    std::lock_guard<std::mutex> lock(srv_mutex);

    // cpy desired state
    state_des.D = config::Vector7(req.data.D.data());
    state_des.Dp = config::Vector6(req.data.Dp.data());
    state_des.Dpp = config::Vector6(req.data.Dpp.data());
    is_cmd_updated = true;
    set_cmd_time = ros::Time::now();

    return true;
}

bool CartesianPositionController::setModeHandler(controller_msgs::setmodeRequest& req, controller_msgs::setmodeResponse& res)
{
    // look srv
    std::lock_guard<std::mutex> lock(srv_mutex);

    Mode mode_ = Mode(req.mode);
    if( mode != mode_ ) {
        mode = mode_;
        ref1.reset_integral();
        ref2.reset_integral();
        is_cmd_updated = false;
    }
    return true;
}

bool CartesianPositionController::relaodGainsHandler(controller_msgs::reloadgainsRequest& req, controller_msgs::reloadgainsResponse& res)
{
    // look srv
    std::lock_guard<std::mutex> lock(srv_mutex);

    // reset integral part, load gains
    ref1.reset_integral();
    ref2.reset_integral();
    return loadGains(ControllerBase::get_name());
}

bool CartesianPositionController::infoHandler(controller_msgs::infoRequest& req, controller_msgs::infoResponse& res)
{
    // look srv
    std::lock_guard<std::mutex> lock(srv_mutex);

    res.name = ControllerBase::get_name();
    res.running = uint8_t(ControllerBase::is_running());
    res.mode = uint8_t(mode);
    res.space = uint8_t(ControllerBase::get_space());

    Vector6 Kd_vec = Kd.diagonal();
    Vector3 Kp_vec = ref1.get_Kp().diagonal();
    Vector3 Ki_vec = ref1.get_Ki().diagonal();
    res.Kd = std::vector<scalar_t>(Kd_vec.data(), Kd_vec.data() + Kd_vec.size());
    res.Kp = std::vector<scalar_t>(Kp_vec.data(), Kp_vec.data() + Kp_vec.size());
    res.Ki = std::vector<scalar_t>(Ki_vec.data(), Ki_vec.data() + Ki_vec.size());
    return true;
}