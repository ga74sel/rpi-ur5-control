#ifndef CARTESIAN_POSITION_CONTROLLER_H_
#define CARTESIAN_POSITION_CONTROLLER_H_

#include <controller_interface/controller_base.h>

#include <controller_msgs/reloadgains.h>
#include <controller_msgs/controllerdata.h>
#include <controller_msgs/info.h>
#include <controller_msgs/setmode.h>
#include <controller_msgs/mode.h>

#include <robot_msgs/writecontroldata.h>

#include <controller_utilities/reference.h>

#include <mutex>

#include <model_interface/model_base.h>

#include "controllers/adaptive_controller.h"

namespace controller
{

/**
 * Implementation of cartesian position controller with orientation being inside the nullspace of the positioning task
 *
 * compute torque value based on desired input commands (set via set_command_srv)
 * internally uses AdaptiveController for parameter updates
 *
 */
class CartesianPositionController : public controller_interface::ControllerBase
{
public:
    typedef Eigen::Matrix<config::scalar_t, 3, config::NJOINT> MatrixJ;
    typedef Eigen::Matrix<config::scalar_t, config::NJOINT, 3> MatrixJinv;

public:
    CartesianPositionController(model_interface::ModelBase& model, std::string name, bool publish_data = false);
    virtual ~CartesianPositionController();

    /* solve dampled least squares problem */
    void setLamda( config::scalar_t lamda_sq );

    /* gain Kp */
    void setKp(const config::Matrix6& Kp);
    void setKp(const config::scalar_t& kp);
    config::Matrix6 getKp() const;

    /* gain Ki */
    void setKi(const config::Matrix6& Ki);
    void setKi(const config::scalar_t& ki);
    config::Matrix6 getKi() const;

    /* set integral limits */
    void setLimits(const config::scalar_t& i_min, const config::scalar_t& i_max );

    /* load gain values from ros_parameters service */
    bool loadGains(const std::string& ns);

private:
    /* init controller parameters */
    bool init(ros::NodeHandle& nh);

    /* called before first time to update */
    void start(const ros::Time& time);

    /* periodically called */
    config::VectorDOF update(robot_msgs::JointState& state, const ros::Time& time, const ros::Duration& dt);

    /* called after last call to update */
    void stop(const ros::Time& time);

    /* service handler */
    bool setCommandHandler(robot_msgs::writecontroldataRequest& req, robot_msgs::writecontroldataResponse& res);
    bool setModeHandler(controller_msgs::setmodeRequest& req, controller_msgs::setmodeResponse& res);
    bool relaodGainsHandler(controller_msgs::reloadgainsRequest& req, controller_msgs::reloadgainsResponse& res);
    bool infoHandler(controller_msgs::infoRequest& req, controller_msgs::infoResponse& res);

private:
    ros::ServiceServer set_command_srv;         // set a new desired state D
    ros::ServiceServer set_mode_srv;            // change between BREAK, PD, PID
    ros::ServiceServer reload_gains_srv;        // change gains during runtime
    ros::ServiceServer info_srv;                // get information about controller
    ros::Publisher controllerdata_pub;          // publish current controller data ( used for debugging )

    robot_msgs::JointState state_des;           // desired jointstate

    model_interface::ModelBase& model;          // dynamic, kinematic
    config::Matrix6 J, Jp, I6, N2;               // Jacobian, d/dt Jacobian, eye
    config::Matrix3 I3;
    config::Matrix4 T_0;                        // Coordinate Transformations
    config::scalar_t lamda_sq;                  // damped jacobobian

    config::Matrix6 Kd;                         // differential gain (joint space)
    reference::Reference<config::Vector3> ref1; // reference task1
    reference::Reference<config::Vector3> ref2; // reference task2

    controller_msgs::Mode mode;                 // BREAK, PD, PID

    bool is_publish_data;                       // publish internal data
    bool is_cmd_updated;                        // true if desired state is recieved

    ros::Time set_cmd_time;                     // time step at which set_mode was recived

    std::mutex srv_mutex;

    /* TESTING: normally in adaptive controller */
    config::MatrixX Yr;                     // regressor
    config::VectorX th;                     // unknown parameters
};

}


#endif // CARTESIAN_POSITION_CONTROLLER_H
