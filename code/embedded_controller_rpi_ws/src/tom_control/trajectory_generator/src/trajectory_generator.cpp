#include "trajectory_generator/trajectory_generator.h"
#include "trajectory_generator/state_base.h"

#include <controller_manager_msgs/setweights.h>

#include <robot_msgs/writecontroldata.h>
#include <robot_msgs/readcontroldata.h>

#include <QElapsedTimer>

using namespace trajectory_generator;

TrajectoryGenerator::TrajectoryGenerator(const std::string &jointstate_srv_name, double rate, const ros::NodeHandle& nh_)
    : RtThreads::Thread(RtThreads::Thread::RtThreadMode, "trajectory_generator"),
      rate(rate),
      nh(nh_),
      state(CONSTRUCTED),
      cur_idx(0),
      warning_time(10.0 / rate)
{
    if( RtThreads::Thread::isRtThread() ) {
        RtThreads::Thread::rtThread()->setPriority(99);
        RtThreads::Thread::rtThread()->setStackSize(16*1024*1024);
        RtThreads::Thread::rtThread()->setStackPreFaultSize(64*1024*1024);
        ROS_INFO_STREAM("TrajectoryGenerator: using rtthread");
    }
    else {
        ROS_INFO_STREAM("TrajectoryGenerator: using qthread");
    }


    ROS_INFO_STREAM("TrajectoryGenerator: wait for jointstate service");
    ros::service::waitForService(jointstate_srv_name, ros::Duration(10.0));
    ROS_INFO_STREAM("TrajectoryGenerator: found jointstate service");

    joint_state_srv = nh.serviceClient<robot_msgs::readcontroldata>(jointstate_srv_name, true);
    set_weights_client = nh.serviceClient<controller_manager_msgs::setweights>("controller_manager/set_weights", true);
    jointstate.setZero();
}

TrajectoryGenerator::~TrajectoryGenerator()
{
    stop_request();
}

bool TrajectoryGenerator::start_request()
{
    // initalize everything
    if( !init() ) {
        ROS_ERROR_STREAM("TrajectoryGenerator: error init");
        return false;
    }
    ros::Time cur_t = ros::Time::now();
    prev_t = cur_t;
    set_weights(states[cur_idx]->get_controller_name());
    states[cur_idx]->start_request( jointstate, cur_t );
    state = INITIALIZED;

    // go
    RtThreads::Thread::start();
    return true;
}

void TrajectoryGenerator::stop_request()
{
    if( state != RUNNING )
        return;
    
    state = STOPPING;
    QElapsedTimer timer;

    while(state != CONSTRUCTED) {
        RtThreads::Thread::usleep(10*1000);
        if( timer.elapsed() > 500 ) {
            RtThreads::Thread::terminate();
            RtThreads::Thread::wait(100);
            ROS_WARN_STREAM("ControllerManager: Thread not responding. Trigger unclean termination");
            state = CONSTRUCTED;
            return;
        }
    }
}

/* add new state to Trajectorygenerator */
bool TrajectoryGenerator::add(StateBasePtr state)
{
    state->set_trajgen(shared_from_this(), states.size());
    states.push_back( state );

    return true;
}

/* main thread running here */
void TrajectoryGenerator::run()
{
    // start the loop
    state = RUNNING;

    ros::Rate loop_rate(rate);
    while(ros::ok() && state != STOPPING) {
        update();
        ros::spinOnce();
        loop_rate.sleep();
    }

    // ros stopped
    cur_idx = 0;
    states[cur_idx]->stop_request( jointstate, ros::Time::now() );
    state = CONSTRUCTED;
}

/* called before start */
bool TrajectoryGenerator::init()
{
    bool ret = true;
    jointstate.setZero();

    for( size_t i = 0; i < states.size(); ++i ) {
        if( !states[i]->init_request(nh) ) {
            ret = false;
            ROS_ERROR_STREAM("TrajectoryGenerator: error init state " << states[i]->get_name() );            
        }
    }
    if( states.empty() ) {
        ret = false;
        ROS_ERROR_STREAM("TrajectoryGenerator: no states added" );
    }
    ROS_INFO_STREAM("TrajectoryGenerator: wait for controller_manager/set_weights to be avialable");
    if( !ros::service::waitForService("controller_manager/set_weights", ros::Duration(10.0)) ) {
        ret = false;
        ROS_ERROR_STREAM("TrajectoryGenerator: switch_mode service timeout");
    }
    ROS_INFO_STREAM("TrajectoryGenerator: found controller_manager/set_weights");
    return ret;
}

/* repeadedly called by run function */
void TrajectoryGenerator::update()
{
    // read jointstate
    ros::Time cur_t;
    if( read_jointstate(jointstate, cur_t) ) {
        // get time
        ros::Duration dt = cur_t - prev_t;
        prev_t = cur_t;

         // check if switch required
        if( states[cur_idx]->transit() )
            switch_states(jointstate, cur_t);

        // update state
        states[cur_idx]->update_request(jointstate, cur_t, dt);

        // check if main loop is running to slow
        if( dt.toSec() > warning_time ) {
            ROS_WARN_STREAM("LOW UPDATE TIME GOT:" << dt.toSec());
        }
    }
    else {
        ROS_ERROR_STREAM("TrajectoryGenerator: error reading jointstate");
    }
}

bool TrajectoryGenerator::read_jointstate(robot_msgs::JointState& jointstate, ros::Time& time)
{
    robot_msgs::readcontroldata msg;
    if( joint_state_srv.call(msg) ) {

        // lock jointstate
        std::lock_guard<std::mutex> lock(jointstate_mutex);

        time = msg.response.data.header.stamp;
        jointstate.Q = config::Vector6(msg.response.data.Q.data());
        jointstate.Qp = config::Vector6(msg.response.data.Qp.data());
        jointstate.Qpp = config::Vector6(msg.response.data.Qpp.data());
        return true;
    }
    return false;
}

void TrajectoryGenerator::switch_states(robot_msgs::JointState& jointstate, const ros::Time& time) 
{
    std::string name_start, name_stop;

    // stop current state
    states[cur_idx]->stop_request(jointstate, time);
    name_stop = states[cur_idx]->get_controller_name();

    // get next state id
    int next_idx = states[cur_idx]->get_next_id();
    if( next_idx < 0 || next_idx > states.size() ) {
        state = INITIALIZED;
        return;
    }
    if( next_idx == cur_idx ) {
        state = INITIALIZED;
        return;
    }
    cur_idx = next_idx;
    name_start = states[cur_idx]->get_controller_name();

    // send weights to controller
    // in this case only one controller is active at a time
    if( name_start != name_stop )
        set_weights(name_start, name_stop);
    
    // start next state
    states[cur_idx]->start_request(jointstate, time);
}

void TrajectoryGenerator::set_weights(const std::string& name_start, const std::string& name_stop )
{
    controller_manager_msgs::setweights msg;
    controller_manager_msgs::weight weight;

    // activate, start controller
    weight.name = name_start;
    weight.weight = 1.0;
    msg.request.weights.push_back( weight );

    // deactivate, stop controller
    weight.name = name_stop;
    weight.weight = 0.0;
    msg.request.weights.push_back( weight );

    set_weights_client.call( msg );
}
