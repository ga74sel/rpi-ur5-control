/**
 * @file states.h
 *
 * @brief concrete implementation of different states
 *
 * @ingroup trajectory_generator
 *
 */

#ifndef STATES_H_
#define STATES_H_

#include "trajectory_generator/state_base.h"
#include "trajectory_generator/trajectory_base.h"

#include <robot_msgs/conv_joint_state.h>
#include <robot_msgs/writecontroldata.h>

#include <controller_utilities/math.h>

#include <controller_msgs/mode.h>
#include <controller_msgs/setmode.h>
#include <controller_msgs/info.h>

#include <model_interface/model_base.h> // forward kinematic

namespace trajectory_generator
{
    using namespace controller_msgs;

/**
* State for Break
*
* all motion is stopped
*/
class BreakTrajectoryState : public StateBase
{
public:
    /** 
    * @brief contructor
    *
    * @param name of state
    * @param controller name
    */
    BreakTrajectoryState(const std::string& name, const std::string& controller_name)
        : StateBase(name, controller_name),
          mode( BREAK )
    {
    }
    virtual ~BreakTrajectoryState()
    {
    }

private:
    /** 
    * @brief init interal parameter
    *
    * @param ros nh for namespace
    */
    bool init(ros::NodeHandle& nh) {
        set_command_client = nh.serviceClient<robot_msgs::writecontroldata>(StateBase::get_controller_name() + "/set_command", true);
        set_mode_client = nh.serviceClient<controller_msgs::setmode>(StateBase::get_controller_name() + "/set_mode", true);
        return true;
    }

    /** 
    * @brief start the state, called befor update
    *
    * @param jointstate
    * @param starting time
    */
    virtual void start(robot_msgs::JointState& state, const ros::Time& time) {
        controller_msgs::setmode msg;
        msg.request.mode = mode;
        set_mode_client.call( msg );
    }

    /** 
    * @brief performs update step of the state, called periodically
    *
    * @param jointstate
    * @param starting time
    * @param deltatime
    */
    virtual void update(robot_msgs::JointState& state, const ros::Time& time, const ros::Duration& dt) {
        // velocity, acceleration
        state.Dp.setZero();
        state.Dpp.setZero();

        // write new state
        robot_msgs::writecontroldata msg;
        robot_msgs::write_jointstate(msg, state, time, dt);
        set_command_client.call( msg );
    }

private:
    ros::ServiceClient set_command_client;
    ros::ServiceClient set_mode_client;
    Mode mode;
};

/** 
* State for trajectory in jointspace
* 
*/
class JointTrajectoryState : public StateBase
{
public:
    /** 
    * @brief contructor
    *
    * @param name of state
    * @param controller name
    * @param control mode BREAK, PID, PD
    * @param timelaw describes the evolution of time along the trajectory
    * @param path describes the sequence of points in space
    * @param relative interprete path end position as relative to current position
    */
    JointTrajectoryState(const std::string& name, const std::string& controller_name, const Mode& mode, 
                         std::unique_ptr<TimeLaw> timelaw, std::unique_ptr<JointPath> path, bool relative = false)
        : StateBase(name, controller_name),
          timelaw(std::move(timelaw)),
          path(std::move(path)),
          mode( mode ),
          relative( relative )
    {
    }
    virtual ~JointTrajectoryState()
    {
    }

    /** 
    * @brief change final position and duration of task
    * 
    * @param duration of task
    * @param final position
    * @param relative or absolute position
    */
    void set(config::scalar_t t, const config::Vector6& Q, bool relative = false) {
        this->relative = relative;
        timelaw->set_final(t);
        path->set_final(Q);
    }

private:
    /** 
    * @brief init interal parameter
    *
    * @param ros nh for namespace
    */
    bool init(ros::NodeHandle& nh) {
        set_command_client = nh.serviceClient<robot_msgs::writecontroldata>(StateBase::get_controller_name() + "/set_command", true);
        set_mode_client = nh.serviceClient<controller_msgs::setmode>(StateBase::get_controller_name() + "/set_mode", true);
        return true;
    }

    /** 
    * @brief start the state, called befor update
    *
    * @param jointstate
    * @param starting time
    */
    virtual void start(robot_msgs::JointState& state, const ros::Time& time) {
        config::Vector6 Qd;

        // switch to desired controller mode
        controller_msgs::setmode msg;
        msg.request.mode = mode;
        set_mode_client.call( msg );

        // set joint position
        start_time = time;
        timelaw->start_request();
        path->start_request(state.Q, Qd, relative);

        // write desired state into jointstate
        state.D << Qd, 0;
    }

    /** 
    * @brief performs update step of the state, called periodically
    *
    * @param jointstate
    * @param starting time
    * @param deltatime
    */
    virtual void update(robot_msgs::JointState& state, const ros::Time& time, const ros::Duration& dt) {
        config::scalar_t t, s, sp, spp;
        config::Vector6 p_vec, t_vec, c_vec;

        // time since start
        t = (time - start_time).toSec();

        // time law
        timelaw->update(t, s, sp, spp);

        // path
        path->update(s, p_vec, t_vec, c_vec);
        // position velocity, acceleration
        state.D << p_vec, 0;
        state.Dp = t_vec * sp;
        state.Dpp = t_vec * spp + c_vec * sp * sp;

        // write new state
        robot_msgs::writecontroldata msg;
        robot_msgs::write_jointstate(msg, state, time, dt);
        set_command_client.call( msg );
    }

private:
    ros::ServiceClient set_command_client;
    ros::ServiceClient set_mode_client;

    std::unique_ptr<TimeLaw> timelaw;
    std::unique_ptr<JointPath> path;
    Mode mode;
    bool relative;
    ros::Time start_time;
};

/**
* Cartesian space trajectory 
*
*/
class CartesianTrajectoryState : public StateBase
{
public:
    /** 
    * @brief contructor
    *
    * @param name of state
    * @param controller name
    * @param control mode BREAK, PID, PD
    * @param timelaw describes the evolution of time along the trajectory
    * @param rotationpath describes the rotion of the end effector
    * @param path describes the sequence of points in space
    * @param relative interprete path end position as relative to current position
    */
    CartesianTrajectoryState(const std::string& name, model_interface::ModelBase& model, const std::string& controller_name, const Mode& mode,
                             std::unique_ptr<TimeLaw> timelaw, std::unique_ptr<CartesianPath> path, 
                             std::unique_ptr<RotationPath> rotationpath, bool relative = false)
        : StateBase(name, controller_name),
          timelaw(std::move(timelaw)),
          path(std::move(path)),
          rotationpath(std::move(rotationpath)),
          model(model),
          mode( mode ),
          relative( relative )
    {
    }
    virtual ~CartesianTrajectoryState()
    {
    }

    /** 
    * @brief change final position and duration of task
    * 
    * @param duration of task
    * @param final position
    * @param final rotation
    * @param relative or absolute position
    */
    void set(config::scalar_t t, const config::Vector3& X, const config::Matrix3& R, bool relative = false) {
        this->relative = relative;
        timelaw->set_final(t);
        path->set_final(X);
        rotationpath->set_final(R);
    }

private:
    /** 
    * @brief init interal parameter
    *
    * @param ros nh for namespace
    */
    bool init(ros::NodeHandle& nh) {
        bool ret = true;
        if( !model.init() ) {
            ROS_INFO_STREAM("CartesianTrajectoryState: error init model");
            ret = false;
        }
        set_command_client = nh.serviceClient<robot_msgs::writecontroldata>(StateBase::get_controller_name() + "/set_command", true);
        set_mode_client = nh.serviceClient<controller_msgs::setmode>(StateBase::get_controller_name() + "/set_mode", true);
        return ret;
    }
    
    /** 
    * @brief start the state, called befor update
    *
    * @param jointstate
    * @param starting time
    */
    virtual void start(robot_msgs::JointState& state, const ros::Time& time) {
        config::Vector3 Xd;
        config::Matrix3 Rd;
        config::Quaternion qd;

        // swtich to desired controller mode
        controller_msgs::setmode msg;
        msg.request.mode = mode;
        set_mode_client.call( msg );

        // forward kinematic
        config::Matrix4 Tef_0;
        model.T_ef(Tef_0, state.Q);

        // set position and roation
        start_time = time;
        timelaw->start_request();
        path->start_request( Tef_0.topRightCorner(3, 1), Xd, relative );
        rotationpath->start_request( Tef_0.topLeftCorner(3, 3), Rd, relative );

        // write desired state into jointstate
        qd = config::Quaternion(Rd);
        state.D.head(3) << Xd;
        state.D.tail(4) = qd.coeffs();
    }

    /** 
    * @brief performs update step of the state, called periodically
    *
    * @param jointstate
    * @param starting time
    * @param deltatime
    */
    virtual void update(robot_msgs::JointState& state, const ros::Time& time, const ros::Duration& dt) {
        config::scalar_t t, s, sp, spp;
        config::Vector3 p_vec, t_vec, c_vec;

        config::Quaternion q;
        config::Vector4 qp, qpp;
        config::Vector3 w;

        // time since start
        t = (time - start_time).toSec();
        // time law
        timelaw->update(t, s, sp, spp);
        path->update(s, p_vec, t_vec, c_vec);
        // path
        rotationpath->update(s, q, qp, qpp);
        // position velocity, acceleration
        state.D.head(3) << p_vec;
        state.Dp.head(3) = t_vec * sp;
        state.Dpp.head(3) = t_vec * spp + c_vec * sp * sp;
        // orientation, angular velocity, angular acceleration
        math::quad_omega(q, sp*qp, w);
        state.D.tail(4) = q.coeffs();
        state.Dp.tail(3) = w;
        state.Dpp.tail(3).setZero();

        // write new state
        robot_msgs::writecontroldata msg;
        robot_msgs::write_jointstate(msg, state, time, dt);
        set_command_client.call( msg );
    }

private:
    ros::ServiceClient set_command_client;
    ros::ServiceClient set_mode_client;

    model_interface::ModelBase& model;

    std::unique_ptr<TimeLaw> timelaw;
    std::unique_ptr<TimeLaw> rotationtimelaw;
    std::unique_ptr<CartesianPath> path;
    std::unique_ptr<RotationPath> rotationpath;
    Mode mode;
    bool relative;
    ros::Time start_time;
};

/*
 * Dummy state
 * Prints out its name
 * Used for testing
 */
class PrintState : public StateBase
{
public:
    PrintState(const std::string& name)
        : StateBase(name, "")
    {
    }
    virtual ~PrintState()
    {
    }

private:
    /* init controller parameters */
    virtual bool init(ros::NodeHandle& nh) {
        ROS_INFO_STREAM(StateBase::get_name() << " : init() called");
        return true;
    }

    /* called before first time to update */
    virtual void start(robot_msgs::JointState& state, const ros::Time& time) {
        ROS_INFO_STREAM(StateBase::get_name() << " : start() called");
    }

    /* called periodically */
    virtual void update(robot_msgs::JointState& state, const ros::Time& time, const ros::Duration& dt) {
        ROS_INFO_STREAM(StateBase::get_name() << " : update() called");
    }

    /* called after last call to update */
    virtual void stop(robot_msgs::JointState& state, const ros::Time& time) {
        ROS_INFO_STREAM(StateBase::get_name() << " : stop() called");
    }
};

}

#endif
