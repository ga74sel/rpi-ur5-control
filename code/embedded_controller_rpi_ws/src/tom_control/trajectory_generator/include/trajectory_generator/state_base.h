/**
 * @file state_base.h
 *
 * @brief state description
 *
 * @ingroup trajectory_generator
 *
 */

#ifndef STATE_BASE_H_
#define STATE_BASE_H_

#include <memory>
#include <ros/ros.h>
#include <robot_msgs/joint_state.h>

#include "trajectory_generator/transition_base.h"

namespace trajectory_generator
{

// forward decleration trajectory_generator
class TrajectoryGenerator;
typedef std::shared_ptr<TrajectoryGenerator> TrajectoryGeneratorPtr;

/**
* Interface description of all states
*
* virtual functions must be reimplemented
* a state can have multible transition, a switch to the next state occures if all transitions are fullfiled
* If no transition is added to the state it is considered to be final and we will stay inside forever
*/
class StateBase
{
public:
    enum State {CONSTRUCTED, INITIALIZED, RUNNING};

    /** 
    * @brief contructor
    *
    * @param name of state
    * @param controller name
    */
    StateBase(const std::string& name, const std::string& controller_name)
        : name(name),
          controller_name(controller_name),
          state(CONSTRUCTED),
          id(0),
          next_id(-1),
          is_final(true)
        {}
    virtual ~StateBase() {}

    /** 
    * @brief add a new transition to this state
    *
    * @param transition ptr
    */
    bool add_transition(TransitionBasePtr transition) {
        is_final = false;
        transitions.push_back( transition );
    }

    /** 
    * @brief init interal parameter
    *
    * @param ros nh for namespace
    */
    bool init_request(ros::NodeHandle& nh) {
        // init all transitions
        for(size_t i = 0; i < transitions.size(); ++i) {
            transitions[i]->init_request(nh);
        }
        if( init(nh) ) {
            state = INITIALIZED;
            return true;
        }
        return false;
    }

    /** 
    * @brief connect to trajectorygenerator
    *
    * @param ptr to parent trajectorygenerator
    * @param id used by trajectorygenerator
    */
    void set_trajgen(TrajectoryGeneratorPtr trajgen, int id) {
        // save information about parent
        this->trajgen = trajgen;
        this->id = id;
    }

    /** 
    * @brief stop the state, called after stopping
    *
    * @param jointstate
    * @param starting time
    */
    bool stop_request(robot_msgs::JointState& jointstate, const ros::Time& time) {
        if( state == RUNNING ) {
            state = INITIALIZED;

            // stop all transitions
            for(size_t i = 0; i < transitions.size(); ++i) {
                transitions[i]->stop_request(jointstate, time);
            }
            stop( jointstate, time );
            return true;
        }
        return false;
    }

    /** 
    * @brief start the state, called befor update
    *
    * @param jointstate
    * @param starting time
    */
    bool start_request(robot_msgs::JointState& jointstate, const ros::Time& time) {
        if( state == INITIALIZED ) {
            state = RUNNING;
            ROS_INFO_STREAM("start: " << name);
            // first start state ( will populate jointstate )
            start( jointstate, time );
            // start all transitions
            for(size_t i = 0; i < transitions.size(); ++i) {
                transitions[i]->start_request(jointstate, time);
            }
            return true;
        }
        return false;
    }

    /** 
    * @brief performs update step of the state, called periodically
    *
    * @param jointstate
    * @param starting time
    * @param deltatime
    */
    void update_request(robot_msgs::JointState& jointstate, const ros::Time& time, const ros::Duration& dt) {
        if( state == RUNNING ) {
            for(size_t i = 0; i < transitions.size(); ++i) {
                transitions[i]->update_request(jointstate, time, dt);
            }
            update( jointstate, time, dt);
        }
    }

    /** 
    * @brief get state name
    */
    std::string get_name() const { return name; }

    /** 
    * @brief get controller name
    */
    std::string get_controller_name() const { return controller_name; }

    /** 
    * @brief get id of this state
    */
    int get_id() const { return id; }

    /** 
    * @brief get id of next state (if transitions fullfiled)
    */
    int get_next_id() const { return next_id; }

    TrajectoryGeneratorPtr getTrajectoryGenerator() { return trajgen; }

    /* check if transition to the next state is required */
    bool transit() {
        if( is_final )
            return false;

        bool fulfilled = true;
        for(size_t i = 0; i < transitions.size(); ++i) {
            if( !transitions[i]->is_fulfilled() )
                fulfilled = false;
            else {
                next_id = transitions[i]->get_dst_id();
            }
        }
        return fulfilled;    
    }

private:
    /** 
    * @brief init interal parameter
    *
    * @param ros nh for namespace
    */
    virtual bool init(ros::NodeHandle& nh) { return true; };

    /** 
    * @brief start the state, called befor update
    *
    * @param jointstate
    * @param starting time
    */
    virtual void start(robot_msgs::JointState& state, const ros::Time& time) {}

    /** 
    * @brief performs update step of the state, called periodically
    *
    * @param jointstate
    * @param starting time
    * @param deltatime
    */
    virtual void update(robot_msgs::JointState& state, const ros::Time& time, const ros::Duration& dt) = 0;

    /** 
    * @brief stop the state, called after last update
    *
    * @param jointstate
    * @param starting time
    */
    virtual void stop(robot_msgs::JointState& state, const ros::Time& time) {}

private:
    State state;         
    TrajectoryGeneratorPtr trajgen;
    std::string name;
    std::string controller_name;
    int id;
    int next_id;
    bool is_final;

    std::vector<TransitionBasePtr> transitions;      // vector of transitions
};

}

#endif
