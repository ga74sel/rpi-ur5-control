#ifndef TRAJECTORY_GENERATOR_H_
#define TRAJECTORY_GENERATOR_H_

#include <ros/ros.h>

#include <RtThreads/Thread.h>

#include <mutex>

#include <memory>

#include <robot_msgs/joint_state.h>
#include <robot_msgs/controldata.h>

/*
 * Generates Trajectories by calling a Sequence of States, each state defines a motion
 */
namespace trajectory_generator
{

// forward declaration Statebase
class StateBase;
typedef std::shared_ptr<StateBase> StateBasePtr;

class TrajectoryGenerator : public std::enable_shared_from_this<TrajectoryGenerator>, public RtThreads::Thread
{
public:
    enum State {CONSTRUCTED, INITIALIZED, RUNNING, STOPPING};

    TrajectoryGenerator(const std::string& jointstate_srv_name, double rate = 500.0, const ros::NodeHandle& nh = ros::NodeHandle());
    virtual ~TrajectoryGenerator();

    /* add new state to Trajectorygenerator */
    bool add(StateBasePtr state);
    
    /* init and start controller thread */
    bool start_request();

    /* stop all controllers and end thread */
    void stop_request();

private:
    /* called before start */
    bool init();

    /* main thread is running here */
    void run();

    /* repeatedly called by run function */
    void update();

    /* read jointstate */
    bool read_jointstate(robot_msgs::JointState& jointstate, ros::Time& time);

    /* switch between states */
    void switch_states(robot_msgs::JointState& jointstate, const ros::Time& time);
    void set_weights(const std::string& name_start, const std::string& name_stop = "" );

private:
    State state;

    ros::NodeHandle nh;

    ros::ServiceClient joint_state_srv;         // read current joint state
    ros::ServiceClient set_weights_client;      // set controller weights

    robot_msgs::JointState jointstate;          // Desired and Current Q, Qp, Qpp
    std::vector<StateBasePtr> states;           // vector of added states
    int cur_idx;                                // current state index within states vec

    double rate;
    ros::Time prev_t;
    double warning_time;

    std::mutex jointstate_mutex;
};

typedef std::shared_ptr<TrajectoryGenerator> TrajectoryGeneratorPtr;
typedef std::shared_ptr<const TrajectoryGenerator> TrajectoryGeneratorConstPtr;

}


#endif
