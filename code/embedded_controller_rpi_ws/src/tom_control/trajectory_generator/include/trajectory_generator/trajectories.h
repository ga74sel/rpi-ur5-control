#ifndef TRAJECTORIES_H_
#define TRAJECTORIES_H_

#include <algorithm>

#include "trajectory_generator/trajectory_base.h"

#include <controller_utilities/math.h>

namespace trajectory_generator
{

/** 
* Time law: t_rapez
* 
*/
class TrapezTimeLaw : public TimeLaw
{
public:
    /* @brief constructor
    *
    * t_f: task final time (duration)
    * t_r: ramp time (acceleration phase)
    */
    TrapezTimeLaw(scalar_t t_f, scalar_t t_r)
        : TimeLaw(t_f), t_r(t_r)
    {
    }
    virtual ~TrapezTimeLaw()
    {
    }

    void update(scalar_t t, scalar_t& s, scalar_t& sp, scalar_t& spp) {
        // longer than expected
        if( t > t_f ) {
            spp = 0.0; sp = 0.0; s = 1.0;
            return;
        }

        if( t < t_r ) {
            // Veclocity ramp up phase
            spp = spp_max;
            sp = spp_max * t;
            s = 0.5f * spp_max * t * t;
        } 
        else if( t < t_f - t_r ) {
            // Constant velocity phase
            spp = 0;
            sp = sp_max;
            s = sp_max * t - sp_max*sp_max / (2 * spp_max); 
        }
        else {
            spp = -spp_max;
            sp = -spp_max * (t - t_f);
            s = -0.5f*spp_max * (t-t_f) * (t-t_f) + sp_max * t_f - sp_max*sp_max / spp_max;
        }
    }
private:
    void start(scalar_t t_f) {
        this->t_f = t_f;
        sp_max = 1.0f  / (t_f - t_r);
        spp_max = sp_max / t_r;
    }

private:
    scalar_t t_f, t_r, sp_max, spp_max;
};

/* 
 * Time law: Cubic polynomial
 */
class CubicTimeLaw : public TimeLaw
{
public:
    CubicTimeLaw(scalar_t t_f)
        : TimeLaw(t_f)
    {
    }
    virtual ~CubicTimeLaw()
    {
    }

    void update(scalar_t t, scalar_t& s, scalar_t& sp, scalar_t& spp) {
        // contrains: s(t) = 0, s(t_f) = 1, sp(0) = sp(t_f) = 0
        // longer than expected
        if( t > t_f ) {
            spp = 0.0; sp = 0.0; s = 1.0;
            return;
        }

        scalar_t t2 = t * t;
        scalar_t t3 = t2 * t; 
        s = -2.0f / tf3 * t3 + 3.0f / tf2 * t2;
        sp = -6.0f / tf3 * t2 + 6.0f / tf2 * t;
        spp = -12.0f / tf3 * t + 6.0f / tf2;
    }
private:
    void start(scalar_t t_f) {
        this->t_f = t_f;
        tf2 = t_f*t_f;
        tf3 = tf2*t_f;
    }

private:
    scalar_t t_f, tf2, tf3;
};

/*
 * Time Law: linear increasing phase
 * f: frequency
 * phi_min: phase min 
 * phi_max: phase max, (important: phi_f > phi_i)
 * phi_i: phase start
 */
class LinearPhase : public TimeLaw
{
public:
    LinearPhase(scalar_t f, scalar_t phi_min = scalar_t(0), scalar_t phi_max = scalar_t(2.0 * M_PI), scalar_t phi_i = 0.0)
        : TimeLaw(0.0),
          f(f),
          w(scalar_t(2)*M_PI*f),
          phi_min(phi_min),
          phi_max(phi_max),
          phi_i(phi_i)
    {
    }
    virtual ~LinearPhase()
    {
    }

    void update(scalar_t t, scalar_t& s, scalar_t& sp, scalar_t& spp) {
        // linear increasing phase
        s = w * t + phi_i;
        sp = w;
        spp = 0;

        // keep phase within bounds
        scalar_t d = s - phi_max;
        if( d > phi_max)
            s = phi_min + d;
    }

    void start(scalar_t t_f) {
    }

private:
    scalar_t f, w, phi_min, phi_max, phi_i;
};

/*
 * Path: Linear Path
 * Q_i: inital joint position
 * Q_f: final joint position
 */
class LinearJointPath : public JointPath
{
public:
    LinearJointPath(const config::Vector6& Q_i, const config::Vector6& Q_f)
        : JointPath(Q_i, Q_f)
    {
    }
    LinearJointPath(const config::Vector6& Q_f)
        : JointPath(Q_f)
    {
    }
    virtual ~LinearJointPath()
    {
    }

    void update(float s, config::Vector6& Q, config::Vector6& t_vec, config::Vector6& c_vec) {
        if( s < scalar_t(1) ) {
            Q = Q_i + d_vec * s;
            t_vec = d_vec;
            c_vec.setZero();
        } else {
            Q = Q_i + d_vec;
            t_vec.setZero();
            c_vec.setZero();
        }
    }

private:
    void start(const config::Vector6& Q_i, const config::Vector6& Q_f) {
        this->Q_i = Q_i;
        d_vec = Q_f - Q_i;
    }
    
private:
    config::Vector6 Q_i, d_vec;
};

/*
 * Describe a sinusoidal motion in joint space
 */
class SinusoidalJointPath : public JointPath
{
public:
    SinusoidalJointPath(const config::Vector6& Q_i, scalar_t A )
        : JointPath(Q_i, config::Vector6::Zero()),
          Q_i(Q_i),
          A(A)
    {
    }
    SinusoidalJointPath(scalar_t A)
        : JointPath(config::Vector6::Zero()),
          A(A)
    {
    }
    virtual ~SinusoidalJointPath()
    {
    }

    void update(float s, config::Vector6& Q, config::Vector6& t_vec, config::Vector6& c_vec) {

        Q << A*sin( s + Q_i.array() ).matrix();
        t_vec << A*cos( s + Q_i.array() ).matrix();
        c_vec << -A*sin( s + Q_i.array() ).matrix();
    }

private:
    void start(const config::Vector6& Q_i, const config::Vector6& Q_f) {
        this->Q_i = Q_i;
    }
    
private:
    config::Vector6 Q_i;
    scalar_t A;
};

/*
 * Describe a linear path in cartesian space
 *  X_i:    inital joint position
 *  X_f:    final joint position
 */
class LinearCartesianPath : public CartesianPath
{
public: 
    LinearCartesianPath(const config::Vector3& X_i, const config::Vector3& X_f)
        : CartesianPath(X_i, X_f)
    {
    }
    LinearCartesianPath(const config::Vector3& X_f)
        : CartesianPath(X_f)
    {
    }
    virtual ~LinearCartesianPath()
    {
    }

    void update(float s, config::Vector3& X, config::Vector3& t_vec, config::Vector3& c_vec) {
        if( s < scalar_t(1) ) {
            X = X_i + d_vec * s;
            t_vec = d_vec;
            c_vec.setZero();
        } else {
            X = X_i + d_vec;
            t_vec.setZero();
            c_vec.setZero();           
        }
    }

private:
    void start(const config::Vector3& X_i, const config::Vector3& X_f) {
        this->X_i = X_i;
        d_vec = X_f - X_i;
    }

private:
    config::Vector3 d_vec, X_i;
};

/*
 * Describe a path in cartesian space
 *  X_i:    inital joint position
 *  X_f:    final joint position
 */
class SLERPRotationPath : public RotationPath
{
public: 
    SLERPRotationPath(const config::Matrix3& R_i, const config::Matrix3& R_f)
        : RotationPath(R_i, R_f)
    {
    }
    SLERPRotationPath(const config::Matrix3& R_f)
        : RotationPath(R_f)
    {
    }
    virtual ~SLERPRotationPath()
    {
    }

    void update(float s, config::Quaternion& q, config::Vector4& qp, config::Vector4& qpp) {
        // slerp interpolation
        math::slerp(q_i, q_f, q, s);
        // slerp derivative wrt to parameter s
        math::slerp_diff(q_i, q_f, qp, s); 
        // no acceleration
        qpp.setZero();
    }

private:
    void start(const config::Matrix3& R_i, const config::Matrix3& R_f) {
        q_i = config::Quaternion(R_i);
        q_f = config::Quaternion(R_f);
    }

protected:
    config::Quaternion q_i, q_f;
};

/*
 * Path: Arbitary circle in space
 * n: unit vector Axis of rotation
 * d: position of circle center within the plane defined by n
 * Xi: Inital Point at which circel motion will start
 */
class CircleCartesianPath : public CartesianPath
{
public: 
    CircleCartesianPath(const config::Vector3& X_i, const config::Vector3& n, const config::Vector3& d)
        : CartesianPath(X_i, config::Vector3::Zero()),
          n(n),
          d(d)
    {
    }
    CircleCartesianPath(const config::Vector3& n, const config::Vector3& d)
        : CartesianPath(config::Vector3::Zero()),
          n(n),
          d(d)
    {
    }
    virtual ~CircleCartesianPath()
    {
    }

    void update(float s, config::Vector3& X, config::Vector3& t_vec, config::Vector3& c_vec) {
        config::Vector3 v1, v2, v3;

        v1 << cos(s), sin(s), 0;
        v2 << -sin(s), cos(s), 0;
        v3 << -cos(s), -sin(s), 0;
        
        X << r*RC_W*v1 + c;
        t_vec << r*RC_W*v2;
        c_vec << r*RC_W*v3;
    }

private:
    void start(const config::Vector3& X_i, const config::Vector3& X_f) {
        define_circle(X_i);
    }

    void define_circle(const config::Vector3& X_i) {
        // compute center point such that C and Xi lie in the sampe plane
        config::Vector3 a = n / n.norm();
        c = d + (X_i - d).dot(a) * a;
        // rotation circle frame wrt world frame
        r = (X_i - c).norm();
        config::Vector3 rx = (X_i - c) / r;
        config::Vector3 rz = a;
        config::Vector3 ry = rz.cross(rx);
        RC_W << rx, ry, rz;
    }

private:
    scalar_t r;
    config::Vector3 n, d;
    config::Vector3 c;      // center point
    config::Matrix3 RC_W;   // circle frame wrt world frame
};

/*
 * Path: Arbitary circle in space
 * n: unit vector Axis of rotation
 * d: position of circle center within the plane defined by n
 * Xi: Inital Point at which circel motion will start
 */
class LemniscateCartesianPath : public CartesianPath
{
public: 
    LemniscateCartesianPath(const config::Vector3& X_i, const config::Vector3& n, const config::Vector3& d)
        : CartesianPath(X_i, config::Vector3::Zero()),
          n(n),
          d(d)
    {
    }
    LemniscateCartesianPath(const config::Vector3& n, const config::Vector3& d)
        : CartesianPath(config::Vector3::Zero()),
          n(n),
          d(d)
    {
    }
    virtual ~LemniscateCartesianPath()
    {
    }

    void update(float s, config::Vector3& X, config::Vector3& t_vec, config::Vector3& c_vec) {
        config::Vector3 v1, v2, v3;
        config::scalar_t c_s = cos(s);
        config::scalar_t s_s = sin(s);
        config::scalar_t d_nom = s_s*s_s + 1;
        config::scalar_t dd_nom = (c_s*c_s - 2)*(c_s*c_s - 2)*(c_s*c_s - 2);

        v1 << c_s, c_s*s_s, 0;
        v2 << s_s*(s_s*s_s - 3), -(3*s_s*s_s - 1), 0.0;
        v3 << c_s*(10.0*c_s*c_s + c_s*c_s*c_s*c_s - 8), (28*c_s*s_s + 3*(8*c_s*c_s*c_s*s_s - 4*c_s*s_s)) / 4, 0.0;

        ROS_INFO_STREAM_THROTTLE(1, "s=" << s << " x= " << v1[0] << " y= " << v1[1]);
        
        X << r/d_nom*RC_W*v1 + c;
        t_vec << r/d_nom*RC_W*v2;
        c_vec << r/dd_nom*RC_W*v3;

        ROS_INFO_STREAM_THROTTLE(1, "s=" << s << " X= " << X[0] << " Y= " << X[1]);
    }

private:
    void start(const config::Vector3& X_i, const config::Vector3& X_f) {
        define_lemniscate(X_i);
    }

    void define_lemniscate(const config::Vector3& X_i) {
        // compute center point such that C and Xi lie in the sampe plane
        config::Vector3 a = n / n.norm();
        c = d + (X_i - d).dot(a) * a;
        // rotation frame wrt world frame
        r = (X_i - c).norm();
        config::Vector3 rx = (X_i - c) / r;
        config::Vector3 rz = a;
        config::Vector3 ry = rz.cross(rx);
        RC_W << rx, ry, rz;

        ROS_INFO_STREAM("RC_W" << RC_W);
    }

private:
    scalar_t r;
    config::Vector3 n, d;
    config::Vector3 c;      // center point
    config::Matrix3 RC_W;   // circle frame wrt world frame
};

}

#endif
