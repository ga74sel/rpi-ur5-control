#ifndef TRANSITIONS_H_
#define TRANSITIONS_H_

#include "trajectory_generator/transition_base.h"

#include <model_interface/model_base.h>

/*
 * Implementation of concrete Transitions
 */

namespace trajectory_generator
{

/*
 * Transition based on elapsed time
 * Transiton criteria is fulfiled after time has passed
 */
class TimeTransition : public TransitionBase
{
public:
    TimeTransition(const std::string& name, StateBaseConstPtr src, StateBasePtr dst, double time)
        : TransitionBase( name, src, dst ),
          time(time),
          set(false)
    {
    }
    virtual ~TimeTransition() {}

private:
    /* init controller parameters */
    bool init(ros::NodeHandle& nh) {
        set = false;
        timer = nh.createTimer(ros::Duration(time), &TimeTransition::callback, this, true);
        timer.stop();
        timer.setPeriod(ros::Duration(time));
        return true;   
    }

    /* called before first time to update */
    void start(robot_msgs::JointState& state, const ros::Time& time) {
        set = false;
        timer.start();
    }

    /* called periodically, return true is transition criteria is matched */
    virtual bool fulfilled(robot_msgs::JointState& state, const ros::Time& time, const ros::Duration& dt) {
        return set;
    }

    /* called after last call to update */
    virtual void stop(robot_msgs::JointState& state, const ros::Time& time) {
        timer.stop();
    }

    void callback(const ros::TimerEvent& event) {
        set = true;
    }

private:
    ros::Timer timer;
    double time;
    bool set;
};

/*
 * Transition based on reaching a specified jointstate
 * Transiton criteria is fulfiled is Qdesired = Q
 */
class ReachJointTransition : public TransitionBase
{
public:
    ReachJointTransition(const std::string& name, StateBaseConstPtr src, StateBasePtr dst,
                         config::scalar_t eps = config::scalar_t(8e-2))
        : TransitionBase( name, src, dst ),
          eps(eps)
    {
    }
    virtual ~ReachJointTransition() {}

    /* called before first time to update joint state contains desired position */
    virtual void start(robot_msgs::JointState& state, const ros::Time& time) {
        Qd = state.D.head(6);
    }

     /* called periodically, return true is transition criteria is matched */
    virtual bool fulfilled(robot_msgs::JointState& state, const ros::Time& time, const ros::Duration& dt) {
        return ( (state.Q - Qd).norm() < eps );
    }   

private:
    config::Vector6 Qd;
    config::scalar_t eps;
};

/*
 * Transition based on reaching a specified jointstate
 * Transiton criteria is fulfiled is Xdesired = X
 */
class ReachCartesianTransition : public TransitionBase
{
public:
    ReachCartesianTransition(const std::string& name, model_interface::ModelBase& model, StateBaseConstPtr src, StateBasePtr dst,
                             config::scalar_t eps = config::scalar_t(3e-3))
        : TransitionBase( name, src, dst ),
          eps(eps),
          model(model)
    {
    }
    virtual ~ReachCartesianTransition() {}

private:
    /* called before first time to update joint state contains desired position */
    virtual void start(robot_msgs::JointState& state, const ros::Time& time) {
        config::Quaternion qd;
        qd.coeffs() = state.D.tail(4);
        Rd = qd.toRotationMatrix();
        Xd = state.D.head(3);
    }

     /* called periodically, return true is transition criteria is matched */
    virtual bool fulfilled(robot_msgs::JointState& state, const ros::Time& time, const ros::Duration& dt) {
        model.T_ef(Tef_0, state.Q);
        X = Tef_0.topRightCorner(3, 1);
        R = Tef_0.topLeftCorner(3, 3);

        e << X - Xd, -config::scalar_t(0.5)*( R.col(0).cross(Rd.col(0)) + R.col(1).cross(Rd.col(1)) + R.col(2).cross(Rd.col(2)) );
        return ( e.norm() < eps );
    }   

private:
    model_interface::ModelBase& model;

    config::Vector3 X, Xd;
    config::Matrix3 R, Rd;
    config::Vector6 e;

    config::Matrix4 Tef_0;
    config::scalar_t eps;
};

}

#endif
