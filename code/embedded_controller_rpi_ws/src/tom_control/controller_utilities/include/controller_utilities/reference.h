/**
 * @file reference.h
 *
 * @brief helper class for reference computation
 *
 * @ingroup controller_utilities
 *
 */

#ifndef REFERENCE_H_
#define REFERENCE_H_

#include <ros/ros.h>

#include <controller_msgs/mode.h>

#include "config.h"

namespace reference
{

/**
 * Implementation of desired value to reference value converter
 *
 * takes desired jointstate and current jointstate to compute the reference jointstate
 *
 * Templated class, iniziate with:
 *      Reference<Eigen::Vector3d> for position / orientation
 *      Reference<Eigen::Vector6d> for position + orientation
 */
template <typename Derived>
class Reference
{
public:
    typedef typename Derived::Scalar DerivedScalar;
    typedef typename Eigen::Matrix<DerivedScalar, Derived::RowsAtCompileTime, 1> DerivedVector;
    typedef typename Eigen::Matrix<DerivedScalar, Derived::RowsAtCompileTime, Derived::RowsAtCompileTime> DerivedMatrix;

public:
    /** 
    * @brief constructor
    *
    * @param prop gain
    * @param integ gain
    */
    Reference();
    Reference(const DerivedMatrix& Kp, const DerivedMatrix& Ki );
    virtual ~Reference();

    /** 
    * @brief set prop gain
    *
    * @param prop gain
    */
    void set_Kp(const DerivedMatrix& Kp);
    void set_Kp(const DerivedScalar& kp);

    /** 
    * @brief set integ gain
    *
    * @param integ gain
    */
    void set_Ki(const DerivedMatrix& Ki);
    void set_Ki(const DerivedScalar& ki);

    /** 
    * @brief set limits for integral part
    *
    * @param integ lower limit
    * @param integ upper limit
    */
    void set_limits(const DerivedScalar& i_min, const DerivedScalar& i_max ); 
    void set_limits(const DerivedVector& i_min, const DerivedVector& i_max ); 

    /// @brief getter
    DerivedMatrix get_Kp() const { return Kp; }
    /// @brief getter
    DerivedMatrix get_Ki() const { return Ki; }
    /// @brief getter
    DerivedVector get_i_min() const { return i_min; }
    /// @brief getter
    DerivedVector get_i_max() const { return i_max; }
    /// @brief getter
    DerivedVector get_integral() const { return iE; }

    /** 
    * @brief reset the integ part back to zero
    */
    void reset_integral();

    /** 
    * @brief compute the reference values
    *
    * @param desired velocity
    * @param error
    * @param deltatime
    * @param control mode
    * @return reference velo
    */
    DerivedVector refp(const DerivedVector& Dp, const DerivedVector& E, const ros::Duration& dt, controller_msgs::Mode mode);

    /** 
    * @brief compute the reference derivative values
    *
    * @param desired acceleration
    * @param error 
    * @param error derivative
    * @param deltatime
    * @param control mode
    * @return reference acceleration
    */
    DerivedVector refpp(const DerivedVector& Dpp, const DerivedVector& E, const DerivedVector& Ep, const ros::Duration& dt, controller_msgs::Mode mode);

private:
    DerivedMatrix Kp;                 // derivative gain
    DerivedMatrix Ki;                 // integral gain        
    DerivedVector iE;                 // integated err

    DerivedVector i_min, i_min_n;     // integral minimum limit, normalized integral minimum limit        
    DerivedVector i_max, i_max_n;     // integral maximum limit, normalized integral maximum limit

    bool is_antiwindup;                 // true if integral is limited by i_min, i_max
    bool is_ki_set;                     // true if ki gain set to other value than zero
};

};

#endif
