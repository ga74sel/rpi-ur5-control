#ifndef RT_UTILITES_H_
#define RT_UTILITES_H_

#include <stdlib.h>
#include <string>

#include "rt_utilities/priority_switcher.h"

namespace rt_utilities
{

/*
 * parse input of application arguments:
 * returns:
 * 0 - non rt
 * 1 - FIFO
 * 2 - RR
 */
int parse_arg(int argc, char **argv)
{
    int ret = POLICY_NORMAL;
    for(int i = 1; i < argc; ++i) {
        std::string arg(argv[i]);
        if( arg == "-s") {
            std::string val(argv[i+1]);
            if( val == "FIFO")
                ret = POLICY_FIFO;
            if( val == "RR")
                ret = POLICY_RR;
        }
    }
    return ret;
} 

}

#endif