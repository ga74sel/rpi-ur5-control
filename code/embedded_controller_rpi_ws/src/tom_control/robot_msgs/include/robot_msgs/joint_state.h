/**
 * @file joint_state.h
 *
 * @brief representation of robots jointstate
 *
 * @ingroup robot_msgs
 *
 */

#ifndef JOINT_STATE_H_
#define JOINT_STATE_H_

#include <controller_utilities/config.h>

namespace robot_msgs
{

/**
 * Description of current robot jointstate
 *
 * used to pass robot state around
 *
 */
class JointState 
{
public:
    JointState() {}
    ~JointState() {}

    void setZero() 
    {
        D.setZero();
        Dp.setZero();
        Dpp.setZero();
        Q.setZero();
        Qp.setZero();
        Qpp.setZero(); 
        Qrp.setZero();
        Qrpp.setZero();
        tau.setZero();        
    }

public:
    // Desired
    config::Vector7 D;              // position: [x,y,z], orientation: [w0,w1,w2,w3]
    config::VectorDOF Dp;           // omega
    config::VectorDOF Dpp;          // omega dot

    // robot state
    config::VectorDOF Q;
    config::VectorDOF Qp;
    config::VectorDOF Qpp;

    // reference
    config::VectorDOF Qrp;
    config::VectorDOF Qrpp;

    // torque
    config::VectorDOF tau;
};

}

#endif
