#ifndef MODE_H_
#define MODE_H_

#include "controller_msgs/setmode.h"

namespace controller_msgs
{

/**
 * Enum of possible controller modes
 *
 * used to switch between BREAK, PD, PID controllers
 * 
 */
enum Mode {
    BREAK = controller_msgs::setmodeRequest::MODE_BREAK,
    PD = controller_msgs::setmodeRequest::MODE_PD,
    PID = controller_msgs::setmodeRequest::MODE_PID
};

}

#endif