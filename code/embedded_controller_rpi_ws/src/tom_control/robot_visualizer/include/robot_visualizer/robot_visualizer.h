/**
 * @file robot_visualizer.h
 *
 * @brief publish joint position in tf tree
 *
 * @ingroup controller_interface
 *
 */

#ifndef ROBOT_VISUALIZER_H_
#define ROBOT_VISUALIZER_H_

#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <tf/transform_broadcaster.h>

#include <vector>
#include <string>
#include <Eigen/Dense>

#include <robot_msgs/controldata.h>
#include <controller_utilities/config.h>

#include <model_interface/model_base.h>

namespace robot_visualizer
{

/**
 * Class to visualize robot model by publishing the jointstates and tf
 *
 */
class RobotVisualizer
{
public:
    /** 
    * @brief contructor
    *
    * @param kinematic and dynamic model
    * @param freq
    * @param nh
    */
    RobotVisualizer(model_interface::ModelBase& model, double rate = 30.0, const ros::NodeHandle& nh = ros::NodeHandle());
    virtual ~RobotVisualizer();

    void set_T0_W(const config::Matrix4& T0_W);

    bool start();

private:
    /* initalize all parameters */
    bool init();

    /* main loop running here */
    void run();

    /* called periodically */
    void update();

    void publish_jointstate();

    void broadcast_tftree();

    void forward_kinematic();

    /* callbacks */
    void controldata_callback(const robot_msgs::controldataConstPtr& msg);

private:
    ros::NodeHandle nh;
    ros::Publisher jointstate_pub;                              // send to rviz
    ros::Subscriber jointdata_intern_sub;                       // get from dynamic model

    model_interface::ModelBase& model;                          // kinematic

    config::Vector6 Q;                                          // joint positions

    sensor_msgs::JointState joint_state;                        // joint states msg

    tf::TransformBroadcaster br;                                // broadcaster
    std::vector<tf::StampedTransform> tf_stamped_transform;     // stack of tf transformations

    std::string base_name;                                      // base frame name
    std::vector<std::string> joint_names_dh;                    // tf dh frames names
    std::vector<config::Matrix4> joint_transform;               // stack of eigen transformation matrices
    std::vector<config::Matrix4> joint_transform_W;             // stack of eigen transformation matrices wrt world
    config::Matrix4 T0_W;                                       // transformation base wrt world

    double rate;
};

}

#endif
