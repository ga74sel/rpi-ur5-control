#include <stdlib.h>
#include <string>

#include <models/model_ur5.h>

#include "robot_simulator/rpi_communication.h"
#include "robot_simulator/robot.h"

int main(int argc, char **argv)
{
    ros::init(argc, argv, "robot_simulator");
    ROS_INFO_STREAM("Starting robot_simulator... \n");

    // load frequency settings from parameter server
    double freq = 1000.0;
    if( ros::param::get("simulator/freq", freq) )
        std::cout << "robot_simulator: freq=" << freq << std::endl;
    else
        ROS_ERROR_STREAM("robot_simulator: error setting freq");


    // create new model instance (UR5) that loads robot parameters
    models::UR5Model ur5_model;

    // instanciate communication to rpi controller
    rpi::RPICommunication comm("host", "rpi", 1.0);

    // instanciate robot class 
    robot_simulator::Robot robot(ur5_model, comm, freq);

    // start
    if(!robot.start_request())
        return -1;

    ros::Rate loop_rate(50.0);
    while( ros::ok() ) {
        ros::spinOnce();
        loop_rate.sleep();
    }

    robot.stop_request();

    return 0;
}