#include "robot_simulator/robot.h"

#include <controller_utilities/load_param.h>

#include <QElapsedTimer>

using namespace config;
using namespace robot_simulator;

Robot::Robot(model_interface::ModelBase& model, hardware_interface::CommunicationBase& hw_comm, double rate, const ros::NodeHandle& nh_)
    : RtThreads::Thread(RtThreads::Thread::RtThreadMode, "robot"),
      model(model),
      hw_comm(hw_comm),
      rate(rate),
      state(INITIALIZED),
      active(true)
{   
    if( RtThreads::Thread::isRtThread() ) {
        RtThreads::Thread::rtThread()->setPriority(95);
        RtThreads::Thread::rtThread()->setStackSize(16*1024*1024);
        RtThreads::Thread::rtThread()->setStackPreFaultSize(64*1024*1024);
        ROS_INFO_STREAM("Robot dynamics: using rtthread");
    }
    else {
        ROS_INFO_STREAM("Robot dynamics: using qthread");
    }

    // ros connections
    joint_state_pub = nh.advertise<robot_msgs::controldata>("jointstate_intern", 1);
    joint_state_srv = nh.advertiseService("/tom_arm_controller/read_jointstate_intern", &Robot::read_joint_state_handler, this);
    start_srv = nh.advertiseService("/robot_simulator/start", &Robot::start_srv_handler, this);
    stop_srv = nh.advertiseService("/robot_simulator/stop", &Robot::stop_srv_handler, this);

    // default inital position to equilibrium
    Q0.setZero();
    Q0(1) = -M_PI / 2.0f;
    Q0(3) = -M_PI / 2.0f;
    jointstate.setZero();
}

Robot::~Robot()
{
    stop_request();
}

bool Robot::start_request()
{
    // initalize everything
    if( !init() ) {
        ROS_ERROR_STREAM("Robot Simulator: error init");
        return false;
    }

    prev_t = ros::Time::now();
    state = INITIALIZED;

    // go
    RtThreads::Thread::start();
    return true;
}

void Robot::stop_request()
{
    if( state != RUNNING )
        return;
    
    state = STOPPING;
    QElapsedTimer timer;

    while(state != CONSTRUCTED) {
        RtThreads::Thread::usleep(10*1000);
        if( timer.elapsed() > 500 ) {
            RtThreads::Thread::terminate();
            RtThreads::Thread::wait(100);
            ROS_WARN_STREAM("Robot Simulator: Thread not responding. Trigger unclean termination");
            state = CONSTRUCTED;
            return;
        }
    }
}

void Robot::run()
{
    // start the loop
    state = RUNNING;

    ros::Rate loop_rate(rate);
    while(ros::ok() && state != STOPPING) {
        // only update if start_srv called
        if( active )
            update();
        ros::spinOnce();
        loop_rate.sleep();
    }

    state = CONSTRUCTED;
}

bool Robot::load_parameter(const std::string& ns) 
{
    bool ret = true;

    // load inital Q value from ros parameter service
    std::vector<double> vev_q;
    std::string err_str;
    if( load_param::load_param_vec(ns, "Q_INIT", vev_q, config::NJOINT, err_str) ) {
        for( int i = 0; i < Q0.size(); ++i)
            *(Q0.data() + i) = vev_q[i];
    } else {
        ret = false; ROS_ERROR_STREAM("Robot Simulator:" << " error setting Q0: " << err_str);
    }

    // set inital values
    M.setZero();
    C.setZero();
    g.setZero();

    Vector6 tmp = jointstate.tau;
    jointstate.setZero();
    jointstate.tau = tmp;
    jointstate.Q = Q0;
    Beta = 10.0f * Matrix6::Identity();

    // compute inital state and write it in the controller buffer
    ros::Time time = ros::Time::now();
    prev_t = time;
    dynamic_equations(ros::Duration(0.0));
    return ret;
}

bool Robot::init()
{
    bool ret = true;

    // load parameter form ros parameter server
    if( load_parameter("UR5Model") ) {
        std::cout << "Robot Simulator: Q0=" << std::endl << Q0 << std::endl;
    } else ret = false;

    // init robot model
    if( model.init() ) {
        std::cout << "Robot Simulator: robot model init" << std::endl;
    } else ret = false;

    // initalize the communication
    if( !hw_comm.init( nh ) ) {
        ret = false;
        ROS_ERROR_STREAM("Robot Simulator: error init controller communication");
    }
    compute_control_client = nh.serviceClient<robot_msgs::computecontroldata>("controller_manager/compute_control", true);

    return ret;
}

void Robot::set_Q0(const config::Vector6& Q0)
{
    this->Q0 = Q0;
}

void Robot::update()
{
    // update time ( robot dynamic is time keeper )
    ros::Time cur_t = ros::Time::now();
    ros::Duration dt = (cur_t - prev_t);
    prev_t = cur_t;

    // assemble rpi controller msg
    robot_msgs::computecontroldata srv;
    srv.request.data.header.stamp = cur_t;
    srv.request.data.header.dt = dt.toSec();
    srv.request.data.Q = std::vector<config::scalar_t>(jointstate.Q.data(), jointstate.Q.data() + jointstate.Q.size());
    srv.request.data.Qp = std::vector<config::scalar_t>(jointstate.Qp.data(), jointstate.Qp.data() + jointstate.Qp.size());
    srv.request.data.Qpp = std::vector<config::scalar_t>(jointstate.Qpp.data(), jointstate.Qpp.data() + jointstate.Qpp.size());

    // call the rpi service
    if( compute_control_client.call( srv ) ) {
        jointstate.tau = config::VectorDOF( srv.response.data.tau.data() );
    } else {
        // error case
        ROS_ERROR_THROTTLE(0.5, "Robot Simulator: communication error stopping!!!");
    }

    // send
    publish_jointstate_intern(cur_t);
}

void Robot::dynamic_equations(const ros::Duration& dt)
{
    // evaluate model equation
    model.M( M, jointstate.Q );
    model.C( C, jointstate.Q, jointstate.Qp );
    model.G( g, jointstate.Q );
    
    // compute joint accelerations
    jointstate.Qpp = M.lu().solve( jointstate.tau - C*jointstate.Qp - g - Beta*jointstate.Qp);

    // integrate results
    jointstate.Qp += jointstate.Qpp*dt.toSec();
    jointstate.Q += jointstate.Qp*dt.toSec();
}

void Robot::publish_jointstate_intern(const ros::Time& time)
{
    robot_msgs::controldata msg;
    msg.header.stamp = time;
    msg.Q = std::vector<float>(jointstate.Q.data(), jointstate.Q.data() + jointstate.Q.size());
    msg.Qp = std::vector<float>(jointstate.Qp.data(), jointstate.Qp.data() + jointstate.Qp.size());
    msg.Qpp = std::vector<float>(jointstate.Qpp.data(), jointstate.Qpp.data() + jointstate.Qpp.size());
    joint_state_pub.publish(msg);
}

bool Robot::read_joint_state_handler(robot_msgs::readcontroldata::Request& req, robot_msgs::readcontroldata::Response& res)
{
    // look srv
    std::lock_guard<std::mutex> lock(srv_mutex);

    if( active ) {
        res.data.header.stamp = ros::Time::now();
        res.data.Q = std::vector<float>(jointstate.Q.data(), jointstate.Q.data() + jointstate.Q.size());
        res.data.Qp = std::vector<float>(jointstate.Qp.data(), jointstate.Qp.data() + jointstate.Qp.size());
        res.data.Qpp = std::vector<float>(jointstate.Qpp.data(), jointstate.Qpp.data() + jointstate.Qpp.size());
        return true;
    }
    return false;
}

bool Robot::start_srv_handler(robot_simulator::start::Request& req, robot_simulator::start::Response& res)
{
    // look srv
    std::lock_guard<std::mutex> lock(srv_mutex);

    if( !active ) {
        load_parameter("simulator");
        active = true;
    }
    return true;
}

bool Robot::stop_srv_handler(robot_simulator::stop::Request& req, robot_simulator::stop::Response& res)
{
    // look srv
    std::lock_guard<std::mutex> lock(srv_mutex);

    active = false;
    return true;
}
