/**
 * @file robot.h
 *
 * @brief simulator of robot
 *
 * @ingroup robot_simulator
 *
 */

#ifndef ROBOT_H_
#define ROBOT_H_

#include <ros/ros.h>

#include <RtThreads/Thread.h>

#include <mutex>

#include <robot_msgs/joint_state.h>
#include <robot_msgs/controldata.h>
#include <robot_msgs/readcontroldata.h>
#include <robot_msgs/computecontroldata.h>

#include <hardware_interface/communication_base.h>

#include <model_interface/model_base.h>

#include "robot_simulator/start.h"
#include "robot_simulator/stop.h"

namespace robot_simulator
{

/**
 * Evaluation of dynamic robot model for robot simulation
 *
 */
class Robot : public RtThreads::Thread
{
public:
    enum State {CONSTRUCTED, INITIALIZED, RUNNING, STOPPING};

    /** 
    * @brief contructor
    *
    * @param kinematic and dynamic model
    * @param communication interface write and read jointstate
    * @param freq
    * @param nh
    */
    Robot(model_interface::ModelBase& model, hardware_interface::CommunicationBase& hw_comm, double rate = 1000.0, const ros::NodeHandle& nh = ros::NodeHandle());
    virtual ~Robot();

    /** 
    * @brief start the simulator
    */
    bool start_request();

    /** 
    * @brief stop the simulator
    */
    void stop_request();

    /** 
    * @brief set inital joint position
    * 
    * @param joint position
    */
    void set_Q0(const config::Vector6& Q0);

    /** 
    * @brief laod internal parameter form parameter server
    * 
    * @param namespace name
    */
    bool load_parameter(const std::string& ns);

private:
    /** 
    * @brief init interal parameter
    */
    bool init();

    /** 
    * @brief main thread is running here
    */
    void run();

    /** 
    * @brief called periodically
    */
    void update();

    /** 
    * @brief compute the model
    */
    void dynamic_equations(const ros::Duration& dt);

    /** 
    * @brief publish joint state to other nodes
    */
    void publish_jointstate_intern(const ros::Time& time);

    // callbacks
    void torque_callback(const robot_msgs::controldataConstPtr& msg);

    // service handler
    bool read_joint_state_handler(robot_msgs::readcontroldata::Request& req, robot_msgs::readcontroldata::Response& res);
    bool start_srv_handler(robot_simulator::start::Request& req, robot_simulator::start::Response& res);
    bool stop_srv_handler(robot_simulator::stop::Request& req, robot_simulator::stop::Response& res);

private:
    State state;

    ros::NodeHandle nh;
    ros::Publisher joint_state_pub;                 // publish current jointstate Q, Qp, Qpp
    ros::ServiceClient compute_control_client;

    ros::ServiceServer joint_state_srv;             // srv to request current joint state Q, Qp, Qpp
    ros::ServiceServer start_srv;                   // start simularion              
    ros::ServiceServer stop_srv;                    // stop simularion

    bool active;                                    // true if start_srv was called

    model_interface::ModelBase& model;              // roboter model

    hardware_interface::CommunicationBase& hw_comm; // communication with the controller

    robot_msgs::JointState jointstate;              // state variables

    config::Vector6 Q0;                             // inital state for integrator
    config::Matrix6 M, C;                           // Inertia, Coriolis force
    config::Vector6 g;                              // gravity
    config::Matrix6 Beta;                           // linear friction coeff

    ros::Time prev_t;
    double rate;
    
    std::mutex srv_mutex;
};

}

#endif
