#include "demo_manual_control/gamepad_controller.h"

#include <controller_utilities/math.h>

#include <robot_msgs/conv_joint_state.h>

#define THRESHOLD 0.95f

using namespace config;
using namespace gamepad_controller;
using namespace robot_msgs;

CartesianGamepadState::CartesianGamepadState(const std::string& name, model_interface::ModelBase& model, const std::string& controller_name, const controller_msgs::Mode& mode)
    : StateBase(name, controller_name),
      model( model ),
      mode( mode ),
      manual_mode(POSITION),
      is_pressed(false),
      Xp( Vector6::Zero() ),
      Xpd( Vector6::Zero() ),
      Kp( 0.8 * Matrix6::Identity() ),
      Xp_step( Vector6::Ones() )
{
    Xp_step << 0.1 * Vector3::Ones(), 0.3 * Vector3::Ones();
}

CartesianGamepadState::~CartesianGamepadState()
{
}

/* init state parameters */
bool CartesianGamepadState::init(ros::NodeHandle& nh) {
    set_command_client = nh.serviceClient<robot_msgs::writecontroldata>(StateBase::get_controller_name() + "/set_command", true);
    set_mode_client = nh.serviceClient<controller_msgs::setmode>(StateBase::get_controller_name() + "/set_mode", true);

    joy_sub = nh.subscribe<sensor_msgs::Joy>("/joy", 1, &CartesianGamepadState::joy_callback, this);

    return true;
}
    
/* called before first time to update */
void CartesianGamepadState::start(robot_msgs::JointState& state, const ros::Time& time) {

    // swtich to desired controller mode
    controller_msgs::setmode msg;
    msg.request.mode = mode;
    set_mode_client.call( msg );

    Matrix4 Tef_0;
    Matrix3 R;
    start_time = ros::Time::now();

    // read inital position
    model.T_ef(Tef_0, state.Q);
    Xpos = Tef_0.topRightCorner(3, 1);
    R = Tef_0.topLeftCorner(3, 3);
    q = Quaternion( R );

    /* TEST. TODO remove
    model.T_ef(Tef_0, state.Q);
    R = Tef_0.topLeftCorner(3, 3);
    model.T_j(Tef_0, state.Q, 2);
    Xpos = Tef_0.topRightCorner(3, 1);
    q = Quaternion( R );*/

    // write desired state into jointstate (TODO)
    state.D.setZero();
    Xp.setZero();
    prev_manual_mode = manual_mode;
}

/* called periodically */
void CartesianGamepadState::update(robot_msgs::JointState& state, const ros::Time& time, const ros::Duration& dt) {
    Vector6 DeltaXp;
    config::Matrix4 Tef_0;
    Matrix3 R;
    Vector3 Xpos_cur;
    Quaternion q_cur, qw, qv;

    // forward kinematic
    model.T_ef(Tef_0, state.Q);
    Xpos_cur = Tef_0.topRightCorner(3, 1);
    R = Tef_0.topLeftCorner(3, 3);
    q_cur = Quaternion( R );

    /* TEST, TODO remove
    model.T_ef(Tef_0, state.Q);
    R = Tef_0.topLeftCorner(3, 3);
    model.T_j(Tef_0, state.Q, 2);
    Xpos_cur = Tef_0.topRightCorner(3, 1);
    q_cur = Quaternion( R ); */

    // change manual control mode
    if( prev_manual_mode != manual_mode ) {
        prev_manual_mode = manual_mode;
        ROS_INFO_STREAM("state= " << ManualModeName[manual_mode]);
        switch(manual_mode) {
        case POSITION:
            q = q_cur;          // remember orientation
            break;
        case ORIENTATION:
            Xpos = Xpos_cur;    // remember position
            break;
        }
    }
    switch(manual_mode) {
    case POSITION:
        Xpd.tail(3).setZero();
        break;
    case ORIENTATION:
        Xpd.head(3).setZero();
        break;
    case BOTH:
        Xpos = Xpos_cur;
        q = q_cur;
        break;
    }

    // update command velocity
    DeltaXp = Xp - Xpd;
    Xp += -Kp * DeltaXp * dt.toSec();

    // update orientation
    qw.coeffs() << Xp[3], Xp[4], Xp[5], 0;                                              // omega wrt to ef frame
    state.Dpp.tail(3).setZero();                                                        // set to zero for testing
    state.Dp.tail(3) = (q * qw * q.inverse()).vec();                                    // wrt to base frame
    q = math::integ_quad(q, state.Dp.tail(3), dt.toSec());                              // integrate the quaternion is base frame
    state.D.tail(4) = q.coeffs();

    // update position
    qv.coeffs() << Xp[0], Xp[1], Xp[2], 0;                      // velo wrt to ef frame
    state.Dpp.setZero();
    state.Dp.head(3) = (q * qv * q.inverse()).vec();            // velo wrt to base frame
    Xpos += state.Dp.head(3) * dt.toSec();
    state.D.head(3) = Xpos;

    // write new state
    robot_msgs::writecontroldata msg;
    robot_msgs::write_jointstate(msg, state, time, dt);
    set_command_client.call( msg );
}

void CartesianGamepadState::parse_joy(const std::vector<float>& axes, const std::vector<int>& buttons)
{
    // update the desired values
    
    // keys for position control
    if( buttons[MOVE_UP] > 0 )
        Xpd[2] = Xp_step[2];
    if( axes[MOVE_DOWN] < THRESHOLD && axes[MOVE_DOWN] != 0.0f )
        Xpd[2] = -Xp_step[2];
    if( buttons[MOVE_UP] == 0 && (axes[MOVE_DOWN] > THRESHOLD || axes[MOVE_DOWN] == 0.0f) )
        Xpd[2] = 0.0;
    // joystick left, right, forward, downward
    Xpd[1] = -Xp_step[1] * axes[MOVE_LEFT_RIGHT];
    Xpd[0] = -Xp_step[0] * axes[MOVE_FORWARD_BACKWARD];

    // keys for orientation control
    if( buttons[MOVE_YAW_UP] > 0 )
        Xpd[5] = Xp_step[5];
    if( axes[MOVE_YAW_DOWN] < THRESHOLD && axes[MOVE_YAW_DOWN] != 0.0f )
        Xpd[5] = -Xp_step[5];
    if( buttons[MOVE_YAW_UP] == 0 && (axes[MOVE_YAW_DOWN] > THRESHOLD || axes[MOVE_YAW_DOWN] == 0.0f) )
        Xpd[5] = 0.0;
    // move left right
    Xpd[4] = -Xp_step[4] * axes[MOVE_PITCH_UP_DOWN];
    Xpd[3] = Xp_step[3] * axes[MOVE_ROLL_UP_DOWN];

    // keys for state selection
    if( buttons[STATE_SELECT] == 1 && !is_pressed) {
        is_pressed = true;
        manual_mode = ManualMode((manual_mode + 1) % 3);
    }
    if( buttons[STATE_SELECT] == 0 )
        is_pressed = false;
}

/* callback */
void CartesianGamepadState::joy_callback(const sensor_msgs::JoyConstPtr& msg)
{
    parse_joy(msg->axes, msg->buttons);
}

//------------------------------------------------------------------------------------------------------------------------------------------

JointGamepadState::JointGamepadState(const std::string& name, const std::string& controller_name, const controller_msgs::Mode& mode)
    : StateBase(name, controller_name),
      mode( mode ),
      Qp( Vector6::Zero() ),
      Qpd( Vector6::Zero() ),
      Kp( 0.8 * Matrix6::Identity() ),
      Qp_step( 0.1 * Vector6::Ones() )
{
}

JointGamepadState::~JointGamepadState()
{
}

/* init state parameters */
bool JointGamepadState::init(ros::NodeHandle& nh) {
    set_command_client = nh.serviceClient<robot_msgs::writecontroldata>(StateBase::get_controller_name() + "/set_command", true);
    set_mode_client = nh.serviceClient<controller_msgs::setmode>(StateBase::get_controller_name() + "/set_mode", true);
    joy_sub = nh.subscribe<sensor_msgs::Joy>("/joy", 1, &JointGamepadState::joy_callback, this);
    return true;
}

/* called before first time to update */
void JointGamepadState::start(robot_msgs::JointState& state, const ros::Time& time) {

    // swtich to desired controller mode
    controller_msgs::setmode msg;
    msg.request.mode = mode;
    set_mode_client.call( msg );
    // forward kinematic
    Q = state.Q;
    Qp.setZero();
    // write desired state into jointstate (TODO)
    state.D.setZero();
}

/* called periodically */
void JointGamepadState::update(robot_msgs::JointState& state, const ros::Time& time, const ros::Duration& dt) {
    Vector6 DeltaQp;

    // update command velocity
    DeltaQp = Qp - Qpd;
    Qp += -Kp * DeltaQp * dt.toSec();

    // position
    Q = state.Q;
    state.Dpp.setZero();
    state.Dp = Qp;
    state.D << Q + state.Dp * dt.toSec(), 0;

    // write new state
    robot_msgs::writecontroldata msg;
    robot_msgs::write_jointstate(msg, state, time, dt);
    set_command_client.call( msg );
}

void JointGamepadState::parse_joy(const std::vector<float>& axes, const std::vector<int>& buttons)
{
    // update the desired values

    // joystick 1
    Qpd[0] = Qp_step[0] * axes[JOINT1_UP_DOWN];
    Qpd[1] = Qp_step[1] * axes[JOINT2_UP_DOWN];
    // joystick 2
    Qpd[2] = Qp_step[2] * axes[JOINT3_UP_DOWN];
    Qpd[3] = Qp_step[3] * axes[JOINT4_UP_DOWN];

    // keys right
    if( buttons[JOINT5_UP] > 0 )
        Qpd[4] = Qp_step[4];
    if( axes[JOINT5_DOWN] < THRESHOLD && axes[JOINT5_DOWN] != 0.0f )
        Qpd[4] = -Qp_step[4];
    if( buttons[JOINT5_UP] == 0 && (axes[JOINT5_DOWN] > THRESHOLD || axes[JOINT5_DOWN] == 0.0f) )
        Qpd[4] = 0.0;

    // key up/down
    if( buttons[JOINT6_UP] > 0 )
        Qpd[5] = Qp_step[5];
    if( axes[JOINT6_DOWN] < THRESHOLD && axes[JOINT6_DOWN] != 0.0f )
        Qpd[5] = -Qp_step[5];
    if( buttons[JOINT6_UP] == 0 && (axes[JOINT6_DOWN] > THRESHOLD || axes[JOINT6_DOWN] == 0.0f) )
        Qpd[5] = 0.0;
}

/* callback */
void JointGamepadState::joy_callback(const sensor_msgs::JoyConstPtr& msg)
{
    parse_joy(msg->axes, msg->buttons);
}
