#include <trajectory_generator/trajectory_generator.h>
#include <trajectory_generator/moveto.h>

#include <controller_utilities/load_param.h>

#include <models/model_ur5.h>

#include "demo_manual_control/gamepad_controller.h"

/** 
* @brief fnc wrapper for transition based on gamepad key press
*
* @param source state
* @param destination state
* @param key identifier
*/
void key_transition(StateBasePtr a, StateBasePtr b, int key_id) {
    TransitionBasePtr t = std::make_shared<gamepad_controller::GamepadTransition>("T", a, b, key_id);
    a->add_transition( t );
}

/** 
* @brief demo function to show robot control with gamepad
*
* @param update rate
*/
void demo_gamepad_controller(double rate = 1000.0)
{
    models::UR5Model ur5_model;
    if( !ur5_model.init() ) {
        ROS_INFO_STREAM("demo_manual_control_node: error init model");
    }

    std::string joint_controller, cartesian_controller, jointstate_srv, cartesian_position_controller;
    config::Vector3 X2;
    config::Matrix3 R2;
    config::Vector6 Q2;
    config::Matrix4 T0_W, TW_0;

    joint_controller = "joint_controller";
    cartesian_controller = "cartesian_controller";
    jointstate_srv = "/tom_arm_controller/read_jointstate_intern";

    T0_W = ur5_model.T0_W();
    TW_0 = T0_W.inverse();
    std::cout << "TW_0=" << TW_0 << std::endl;

    Q2 << 5.33, -1.566, 1.55, -3.10, -1.55, 2.34;
    R2 << 0, 0, 1, 0, -1, 0, 1, 0, 0; math::transf_rot(TW_0, R2);
    X2 << 0.6, -0.6, 0.4; math::transf_pt(TW_0, X2);

    // define States
    StateBasePtr s1 = std::make_shared<BreakTrajectoryState>("s1_break", joint_controller);
    StateBasePtr s2 = std::make_shared<MoveToPointJointState>("s2_joint_linear", joint_controller, Mode::PD, 20.0, Q2);
    StateBasePtr s3 = std::make_shared<MoveToPointCartesianState>("s3_cartesian_linear", ur5_model, cartesian_controller, Mode::PD, 2.0, X2, R2);
    StateBasePtr s4 = std::make_shared<BreakTrajectoryState>("s4_break", cartesian_controller);
    StateBasePtr s5 = std::make_shared<gamepad_controller::CartesianGamepadState>("s5_gamepad", ur5_model, cartesian_controller, Mode::PD);
    
    // setup transition
    wait(s1, s2, 2.0);
    reach_joint(s2, s3);
    reach_cartesian(ur5_model, s3, s4);
    key_transition(s5, s4, gamepad_controller::KEY_A);
    key_transition(s4, s5, gamepad_controller::KEY_B);

    // build machine
    TrajectoryGeneratorPtr machine = std::make_shared<TrajectoryGenerator>(jointstate_srv, rate);
    machine->add(s1);
    machine->add(s2);
    machine->add(s3);
    machine->add(s4);
    machine->add(s5);

    // start
    if( !machine->start_request() )
        return;
    ros::spin();
    machine->stop_request();
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "trajectory_generator");
    ROS_INFO_STREAM("Starting trajectory_generator... \n");

    // 1. load fequency
    double freq = 1000.0;
    if( ros::param::get("trajectory_gen/freq", freq) )
        std::cout << "trajectory_gen: freq=" << freq << std::endl;
    else
        ROS_ERROR_STREAM("trajectory_gen: error setting freq");

    demo_gamepad_controller(freq);

    return 0;
}
