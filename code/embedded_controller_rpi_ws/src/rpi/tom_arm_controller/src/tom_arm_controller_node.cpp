#include <tum_ics_ur_robot_lli/Robot/RobotArm.h>

#include <tom_arm_controller/rpi_communication.h>
#include <tom_arm_controller/tom_arm_controller.h>

#include<QApplication>

using namespace Eigen;

int main(int argc, char **argv)
{
    QApplication a(argc,argv);

    ros::init(argc,argv,"tom_arm_controller",ros::init_options::AnonymousName);

    QString configFilePath=argv[1];

    ROS_INFO_STREAM("Config File: " << configFilePath.toStdString().c_str());

    //tum_ics_ur_robot_lli::Robot::RobotArmConstrained robot(configFilePath);
    tum_ics_ur_robot_lli::Robot::RobotArm robot(configFilePath);

    //starts robotArm communication and the thread
    //inits Kinematic Model, Dynamic Model
    if(!robot.init()) {
        return -1;
    }

    ROS_INFO_STREAM("dynamic_model:" << robot.getDynamicModel()->g0());

    // communication host -> rpi controller
    rpi::RPICommunication rpi("host", "rpi", 0.1);

    // controller
    tum_ics_ur_robot_lli::RobotControllers::TOMArmController tom_arm_controller(rpi);

    if(!robot.add(&tom_arm_controller)) {
        return -1;
    }

    tom_arm_controller.setQHome(robot.qHome());
    tom_arm_controller.setQPark(robot.qPark());

    robot.start();

    ROS_INFO_STREAM("tom_arm_controller_node: start main thread");
    ros::spin();
    ROS_INFO_STREAM("tom_arm_controller_node: stoping RobotArm()");
    robot.stop();
    ROS_INFO_STREAM("tom_arm_controller_node: stopped!!!");
}
