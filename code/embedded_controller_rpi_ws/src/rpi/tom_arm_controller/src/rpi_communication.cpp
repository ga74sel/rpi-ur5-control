#include "tom_arm_controller/rpi_communication.h"

using namespace rpi;
using namespace config;
using namespace robot_msgs;

RPICommunication::RPICommunication(const std::string& src_name, const std::string& dst_name, double time_out)
    : state(CONSTRUCTED),
      time_out(time_out),
      src_name(src_name),
      dst_name(dst_name)
{
}

RPICommunication::~RPICommunication()
{
}

bool RPICommunication::init(ros::NodeHandle& nh)
{
    bool ret;

    // connections 
    joint_state_server = nh.advertiseService(src_name + "/read_joint_state", &RPICommunication::joint_state_handler, this);

    // init state
    state = INITIALIZED;
    jointstate.setZero();
    cmd_time = ros::Time::now();

    // wait till destination is ready
    ROS_INFO_STREAM(src_name + " waiting for " + dst_name + " to be ready...");
    ret = ros::service::waitForService(dst_name + "/read_tau", ros::Duration(10.0));
    ROS_INFO_STREAM(src_name + " to " + dst_name + " is ready");

    ret = ros::service::waitForService(dst_name + "/read_tau", ros::Duration(1.0));
    torque_client = nh.serviceClient<robot_msgs::readcontroldata>(dst_name + "/read_tau", true);
    return ret;
}

/* check if communication is still possible */
bool RPICommunication::is_alive()
{
    // check if communication channel is still open
    if( state == CONNECTED ) {

        static bool time_flag = true;
        if(time_flag) {
            time_flag = false;
            ROS_INFO_STREAM("is_alive               : " << (ros::Time::now() - cmd_time).toSec() );
        }

        // connected: check time limit
        if( (ros::Time::now() - cmd_time).toSec() > time_out ) {
            ROS_INFO_STREAM("error time: " << (ros::Time::now() - cmd_time).toSec());
            state = ERROR;
            return false;
        }
        return true;
    }
    else if( state == ERROR ) {
        // stay in error state
        return false;
    }
    // not started yet
    return false;
}

/* read current joint state */
bool RPICommunication::read(robot_msgs::JointState& jointstate, ros::Time& time)
{
    // make service call to read current joint state from robot
    robot_msgs::readcontroldata srv;
    if( torque_client.call( srv ) )
    {
        robot_msgs::controldata& data = srv.response.data;

        // check if connected
        if( state != CONNECTED ) {
            if( state == INITIALIZED )
                state = RECEIVED2;
            else if( state == RECEIVED1 ) {
                state = CONNECTED;
            }
        }

        // lock jointstate
        pthread_mutex_lock(&jointstate_mutex);

        // cpy torque
        time = data.header.stamp;
        jointstate.tau = Vector6( data.tau.data() );

        // unlock jointstate
        pthread_mutex_unlock(&jointstate_mutex);

        // check if msg contains NANs
        if( jointstate.tau.array().isNaN().any() ) {
            ROS_ERROR_STREAM("RPICommunication::torquedata_callback : got NAN tau");
            state = ERROR;
            return false;
        }

        // update time
        cmd_time = data.header.stamp;
        return true;
    }
    return false;
}

/* write new joint state */
bool RPICommunication::write(const robot_msgs::JointState& jointstate_, const ros::Time& time)
{
    // check if connected
    if( state != CONNECTED ) {
        if( state == INITIALIZED)
            state = RECEIVED1;
        else if( state == RECEIVED2)
            state = CONNECTED;
    }

    // lock jointstate
    pthread_mutex_lock(&jointstate_mutex);

    // save internally
    cur_time = time;
    jointstate.Q = jointstate_.Q;
    jointstate.Qp = jointstate_.Qp;
    jointstate.Qpp = jointstate_.Qpp;

    // unlock
    pthread_mutex_unlock(&jointstate_mutex);
    return true;
}

bool RPICommunication::joint_state_handler(robot_msgs::readcontroldata::Request& req, robot_msgs::readcontroldata::Response& res)
{
    if( state == RECEIVED1 || state == CONNECTED ) {

        // lock jointstate
        pthread_mutex_lock(&jointstate_mutex);

        res.data.header.stamp = cur_time;
        res.data.Q = std::vector<scalar_t>(jointstate.Q.data(), jointstate.Q.data() + jointstate.Q.size());
        res.data.Qp = std::vector<scalar_t>(jointstate.Qp.data(), jointstate.Qp.data() + jointstate.Qp.size());
        res.data.Qpp = std::vector<scalar_t>(jointstate.Qpp.data(), jointstate.Qpp.data() + jointstate.Qpp.size());

        // unlook jointstate
        pthread_mutex_unlock(&jointstate_mutex);

        return true;
    }
    return false;
}
