#ifndef TOM_ARM_H
#define TOM_ARM_H

#include<tum_ics_ur_robot_lli/RobotControllers/ControlEffort.h>

#include <pthread.h>

#include <robot_msgs/joint_state.h>
#include <robot_msgs/controldata.h>
#include <robot_msgs/readcontroldata.h>
#include <hardware_interface/communication_base.h>
#include <models/model_ur5.h>

#include <controller_utilities/logger.h>

namespace tum_ics_ur_robot_lli {
namespace RobotControllers {

class TOMArmController: public ControlEffort
{
public:
    TOMArmController(hardware_interface::CommunicationBase& hw_comm, double weight=1.0, const QString& name = "tom_arm_controller");
    virtual ~TOMArmController();

    void setQInit(const JointState& qinit);

    void setQHome(const JointState& qhome);

    void setQPark(const JointState& qpark);

private:
    bool init();

    bool start();

    Vector6d update(const RobotTime& time, const JointState &current);

    bool stop();

    void publish_jointstate(const robot_msgs::JointState& state, const ros::Time& time);

    // service handler
    bool read_joint_state_handler(robot_msgs::readcontroldata::Request& req, robot_msgs::readcontroldata::Response& res);

private:
    ros::NodeHandle nh;
    ros::Publisher joint_state_pub;                         // publish current jointstate Q, Qp, Qpp
    ros::ServiceServer joint_state_srv;                     // srv to request current joint state Q, Qp, Qpp

    ros::ServiceClient compute_control_client;

    bool is_active;                                         // true if start_srv was called
    robot_msgs::JointState jointstate;                      // current jointstate

    hardware_interface::CommunicationBase& hw_comm;         // communication with the rpi hw

    pthread_mutex_t srv_mutex;

    models::UR5Model model;                                 // for safty reasons if rpi connection fails, use this to break all motion
    config::MatrixX Yr;
    config::VectorX th;

    config::Vector6 Q_inital;
    JointState m_qInit;
    JointState m_qHome;
    JointState m_qPark;
    ros::Time prev_t;
};

}
}

#endif // TOM_ARM_H

