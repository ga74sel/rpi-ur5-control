/**
 * @file host_communication.h
 *
 * @brief communication rpi and host (running on rpi)
 *
 * @ingroup rpi_controller
 *
 */

#ifndef RPI_HARDWARE_H_
#define RPI_HARDWARE_H_

#include <robot_msgs/readcontroldata.h>
#include <hardware_interface/communication_base.h>

#include <mutex>

namespace rpi
{

/**
* Defines the communication interface between the rpi controller and the host computer
* 
* read:
*       calls a service on the host computer and returns new Jointstate on success
* write:
*       publishes a topic to be recivied by the host computer and sets the new Jointstate on the robot
*/
class HostCommunication : public hardware_interface::CommunicationBase 
{
public:
    enum State {CONSTRUCTED, INITIALIZED, RECEIVED1, RECEIVED2, CONNECTED, ERROR };

    /** 
    * @brief contructor
    *
    * @param name of source ( used as prefix of services pub by this class )
    * @param name of destination ( used as prefix of services pub by this class )
    */
    HostCommunication(const std::string& src_name, const std::string& dst_name);
    virtual ~HostCommunication();

    /** 
    * @brief init interal parameter
    *
    * @param ros nh for namespace
    */
    bool init(ros::NodeHandle& nh);

    /**
    * @brief check if communication is still possible
    */
    bool is_alive();

    /*
    * @brief read current joint state
    * @param jointstate
    * @param current time
    */
    bool read(robot_msgs::JointState& jointstate, ros::Time& time);

    /*
    * @brief write current joint state
    * @param jointstate
    * @param current time
    */
    bool write(const robot_msgs::JointState& jointstate, const ros::Time& time);

private:
    bool torque_handler(robot_msgs::readcontroldata::Request& req, robot_msgs::readcontroldata::Response& res);

private:
    State state;

    ros::ServiceServer torque_server;          // make current tau available
    ros::ServiceClient joint_state_client;     // current read joint state

    robot_msgs::JointState jointstate;

    ros::Time tau_time;                         // time at which a new jointstate was set

    std::string src_name, dst_name;

    std::mutex jointstate_mutex;
};

}

#endif
