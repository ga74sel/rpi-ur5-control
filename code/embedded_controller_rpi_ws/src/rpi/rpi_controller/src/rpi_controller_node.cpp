#include <controller_manager/controller_manager.h>
#include <controllers/joint_controller.h>
#include <controllers/cartesian_controller.h>
#include <controllers/cartesian_position_controller.h>
#include <models/model_ur5.h>

#include "rpi_controller/host_communication.h"

/**
 * main node for rpi controllers
 *
 * combines model, communication, controller manager and controllers
 *
 */
int main(int argc, char **argv)
{
    ros::init(argc, argv, "rpi_controller");
    ROS_INFO_STREAM("starting rpi_controller...");

    // load specified control fequency
    double freq = 1000.0;
    if( ros::param::get("controller/freq", freq) )
        std::cout << "error_space_controller: freq=" << freq << std::endl;
    else
        ROS_ERROR_STREAM("error_space_controller: error setting freq");

    // create new model instance (UR5) that loads robot parameters
    models::UR5Model ur5_model;

    // create new hardware instance for rpi to host communication
    rpi::HostCommunication hw("rpi", "host");

    // create contoller manager instance that offers interface to switch space and reference
    controller_manager::ControllerManager controller_manager(hw, freq);

    // create new controller instances
    bool pub_interal_data = true;
    controller_manager.add( std::make_shared<controller::JointController>(ur5_model, "joint_controller", pub_interal_data), 1.0 );
    controller_manager.add( std::make_shared<controller::CartesianController>(ur5_model, "cartesian_controller", pub_interal_data), 0.0 );
    controller_manager.add( std::make_shared<controller::CartesianPositionController>(ur5_model, "cartesian_position_cntr", pub_interal_data), 0.0 );

    // start 
    if( !controller_manager.start_request() )
        return -1;

    // keep thread alive
    ros::spin();
    
    // stop
    controller_manager.stop_request();

    return 0;
}
