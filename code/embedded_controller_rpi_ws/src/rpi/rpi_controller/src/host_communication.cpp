#include "rpi_controller/host_communication.h"

#include <robot_msgs/controldata.h>
#include <robot_msgs/readcontroldata.h>

using namespace rpi;
using namespace config;
using namespace robot_msgs;

HostCommunication::HostCommunication(const std::string& src_name, const std::string& dst_name)
    : state(CONSTRUCTED),
      src_name(src_name),
      dst_name(dst_name)
{
}

HostCommunication::~HostCommunication()
{
}

bool HostCommunication::init(ros::NodeHandle& nh)
{
    bool ret;

    // wait till destination has setup
    ROS_INFO_STREAM(src_name + " waiting for " + dst_name + " to be ready...");
    ret = ros::service::waitForService(dst_name + "/read_joint_state", ros::Duration(10.0));
    ROS_INFO_STREAM(src_name + " to " + dst_name + " is ready");

    // init states
    state = INITIALIZED;
    jointstate.setZero();
    tau_time = ros::Time::now();

    // comunication
    torque_server = nh.advertiseService(src_name + "/read_tau", &HostCommunication::torque_handler, this);
    joint_state_client = nh.serviceClient<robot_msgs::readcontroldata>(dst_name + "/read_joint_state", true);

    return ret;
}

bool HostCommunication::is_alive()
{
    return true;
}

/* read current joint state Q, Qp, Qpp */
bool HostCommunication::read(robot_msgs::JointState& jointstate, ros::Time& time)
{
    // make service call to read current joint state from robot
    robot_msgs::readcontroldata srv;
    if( joint_state_client.call( srv ) ) {
        robot_msgs::controldata& data = srv.response.data;

        // look jointstate
        std::lock_guard<std::mutex> lock(jointstate_mutex);

        time = data.header.stamp;
        jointstate.Q = Vector6( data.Q.data() );
        jointstate.Qp = Vector6( data.Qp.data() );
        jointstate.Qpp = Vector6( data.Qpp.data() );
        return true;
    }
    return false;
}

/* write new joint state */
bool HostCommunication::write(const robot_msgs::JointState& jointstate_, const ros::Time& time)
{
    // look jointstate
    std::lock_guard<std::mutex> lock(jointstate_mutex);

    // check if connected
    if( state != CONNECTED ) {
        if( state == INITIALIZED)
            state = RECEIVED1;
        else if( state == RECEIVED2)
            state = CONNECTED;
    }

    // save internally
    tau_time = time;
    jointstate.tau = jointstate_.tau;
    return true;
}

bool HostCommunication::torque_handler(robot_msgs::readcontroldata::Request& req, robot_msgs::readcontroldata::Response& res)
{
    // look jointstate
    std::lock_guard<std::mutex> lock(jointstate_mutex);

    if( state == RECEIVED1 || state == CONNECTED ) {
        res.data.header.stamp = tau_time;
        res.data.tau = std::vector<scalar_t>(jointstate.tau.data(), jointstate.tau.data() + jointstate.tau.size());
        return true;
    }
    return false;
}
