#include "demo_trajectory/motion_proxy.h"

using namespace demo_trajectory;
using namespace config;
using namespace robot_msgs;

MotionProxyTransition::MotionProxyTransition(const std::string& name, StateBaseConstPtr src, StateBasePtr dst, const std::string srv_name)
    : TransitionBase(name, src, dst),
      srv_name(srv_name),
      set(false)
{
}

MotionProxyTransition::~MotionProxyTransition()
{
}

/* init controller parameters */
bool MotionProxyTransition::init(ros::NodeHandle& nh)
{
    set = false;
    motion_proxy_srv = nh.advertiseService("motion_proxy/moveto", &MotionProxyTransition::motion_proxy_handler, this);
}

/* called before first time to update */
void MotionProxyTransition::start(robot_msgs::JointState& state, const ros::Time& time) {
    set = false;
}

    /* called periodically, return true is transition criteria is matched */
bool MotionProxyTransition::fulfilled(robot_msgs::JointState& state, const ros::Time& time, const ros::Duration& dt)
{
    return set;
}

/* srv handler */
bool MotionProxyTransition::motion_proxy_handler(demo_trajectory::movetoRequest& req, demo_trajectory::movetoResponse& res)
{
    auto cartesian_state = std::dynamic_pointer_cast<CartesianTrajectoryState>(TransitionBase::dst);
    if( cartesian_state != nullptr ) {
        if( req.time > 0.0 ) {
            Quaternion q = Quaternion(req.orientation.w, req.orientation.x, req.orientation.y, req.orientation.z);
            Matrix3 R = q.toRotationMatrix();

            Vector3 X;
            X << req.position.x, req.position.y, req.position.z;
        
            cartesian_state->set(req.time, X, R, true);
            set = true;
        }
    }
    return set;
}