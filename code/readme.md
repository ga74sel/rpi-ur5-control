# C++ Control framework

## Workspaces
* embedded controller_ws contains code for user-pc and tommm-mind-pc
* embedded controller_rpi_ws contains code for raspbery pi

## Raspberry Pi
* user: pi
* pw: root
* static ip: 20.0.0.190

## Build the project

on user-pc and tomm-mind-pc
```ruby
cd controller_ws
catkin_make
```
on raspbery pi
```ruby
cd controller_rpi_ws
catkin_make
```

## Run the project
on tomm-mind-pc
```ruby
roscore
roslaunch bringup_rpi_controller robot_script_manager_right.launch
roslaunch rpi_bringup_controller host_pc.launch
```

on rpi
```ruby
sudo -E bash
roslaunch rpi_bringup_controller rpi.launch
```

on user-pc
```ruby
roslaunch tom_bringup bringTomGroupsLacquey.launch
roslaunch rpi_bringup_controller demo_manual_control.launch
```

## Run the project on a single machine
```ruby
roscore
roslaunch tom_bringup bringTomGroupsLacquey.launch
roslaunch rpi_bringup_controller full.launch
roslaunch rpi_bringup_controller demo_manual_control.launch
```
