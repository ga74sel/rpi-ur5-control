#include <timing_tests_util/logger.h>

const int MAX_HIST_SIZE = 100000;
const int VAL_BUF_SIZE = 16384; 

Logger::Logger(uint64_t hist_size)
{
    size = hist_size > MAX_HIST_SIZE ? MAX_HIST_SIZE : hist_size;
    hist.resize(size);
    outliers.resize(VAL_BUF_SIZE);

    mask = VAL_BUF_SIZE - 1;

    max = 0;
    min = (uint64_t)MAX_HIST_SIZE;
    avg = 0.0;
    cnt = 1;
    outofsync_cnt = 0;
    hist_overflow_cnt = 0;
}

Logger::~Logger()
{
}

void Logger::set_outofsync(int outofsync_cnt)
{
    this->outofsync_cnt = outofsync_cnt;
}

void Logger::update(uint64_t dt)
{
    if(dt > size) {
        hist_overflow_cnt++;
    }
    else {
        hist[dt]++;
    }

    if(dt < min)
        min = dt;
    else if(dt > max)
        max = dt;

    avg = avg + ((double)dt - avg) / cnt;
    cnt++;
}

void Logger::print_hist(const std::string& file)
{
    FILE * fp;
    fp = fopen(file.c_str(), "w");
    if(!fp)
        return;

    for(int i = 0; i < size; ++i) {
        fprintf(fp, "%06d %06d\n", i, hist[i]);
    }
    fclose(fp);
}

void Logger::print_stat()
{
    printf("n=%d\n", cnt);
    printf("n_overflow=%d\n", hist_overflow_cnt);
    printf("n_out_sync=%d\n", outofsync_cnt);
    printf("min=%d us\n", (int)min);
    printf("max=%d us\n", (int)max);
    printf("avg=%f us\n", avg);
    printf("avg_f=%f Hz\n", 1.0 / avg * 1e6);
}