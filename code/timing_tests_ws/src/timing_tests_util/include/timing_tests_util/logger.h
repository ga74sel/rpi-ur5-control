#ifndef LOGGER_H_
#define LOGGER_H_

#include <cstdint>
#include <vector>
#include <string>

/*
 * Log Statistic data
 */
class Logger
{
public:
    Logger(uint64_t hist_size = 100000);
    virtual ~Logger();

    void set_outofsync(int outofsync_cnt);

    /* update statistics */
    void update(uint64_t dt);

    /* print */
    void print_hist(const std::string& file);
    void print_stat();

private:
    uint64_t size;
    std::vector<int> hist;
    std::vector<uint64_t> outliers;

    uint64_t max;
    uint64_t min;
    double avg;
    int cnt;
    int outofsync_cnt;
    int hist_overflow_cnt;

    int mask;
};

#endif