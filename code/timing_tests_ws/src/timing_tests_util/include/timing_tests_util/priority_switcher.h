
#ifndef PRIORITYSWITCHER_H_
#define PRIORITYSWITCHER_H_

#include <unistd.h>
#include <sched.h>
#include <sys/resource.h>

/*
 * Change the priority and scheduler of the current thread
 */
class PrioritySwitcher 
{
public:
	PrioritySwitcher(int policy = 0);
	virtual ~PrioritySwitcher();

    int swtich_realtime_prio();
    int switch_normal_prio();

private:
	pid_t pid;
	pid_t default_prio;
	int default_scheduler_policy;
	int policy;

	void save_default();
};

#endif //PRIORITYSWITCHER_H_