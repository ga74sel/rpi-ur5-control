#ifndef RTT_TOPIC_H_
#define RTT_TOPIC_H_

#include <ros/ros.h>
#include <communication_test/payload_srv.h>
#include <timing_tests_util/logger.h>

/* 
 * Measures the round trip time (rtt) between two instances of class
 * Comunication is done via ros service calls
 */
class RTTService
{
public:
    RTTService();
    virtual ~RTTService();

    /* spin */
    void run();

private:
    ros::NodeHandle nh;
    ros::ServiceServer server;
    ros::ServiceClient client;

    Logger logger;

    ros::Time prev_time;
    bool is_first;

    /* test settings */
    int is_client;
    std::string id, srv_name, client_name;
    int n;
    int freq;
    int payload_len;
    int do_start;

    /* message state */
    std::vector<unsigned char> payload;

    void run_service();
    void run_client();

    /* services */
    bool run_srv_payload(communication_test::payload_srv::Request& req, communication_test::payload_srv::Response &res);
};

#endif
