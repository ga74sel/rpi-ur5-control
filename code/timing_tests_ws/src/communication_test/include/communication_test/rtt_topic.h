#ifndef RTT_TOPIC_H_
#define RTT_TOPIC_H_

#include <ros/ros.h>
#include <communication_test/timestemp.h>
#include <timing_tests_util/logger.h>

/* 
 * Measures the round trip time (rtt) between two instances of class
 * Communication is done via two ros topics
 */
class RTTTopic
{
public:
    RTTTopic();
    virtual ~RTTTopic();

    /* spin */
    void run();

private:
    ros::NodeHandle nh;
    ros::Publisher pub;
    ros::Subscriber sub;
    ros::Timer offset_timer;

    Logger logger;

    /* test settings */
    std::string id, sub_topic_name, pub_topic_name;
    int payload_len;
    float frequency;
    double mean_loop_dt;
    int offset_sec;
    int do_start;

    /* message state */
    std::vector<char> payload;
    ros::Time prev_time;
    int prev_seq;

    /* statistics */
    int outofsync_cnt;

    /* callbacks */
    void msg_callback(const communication_test::timestemp::ConstPtr& msg);
    void timer_callback(const ros::TimerEvent& timerevent);
};

#endif
