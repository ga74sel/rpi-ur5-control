#include <communication_test/rtt_topic.h>
#include <stdio.h>

RTTTopic::RTTTopic()
    : id(""), sub_topic_name(""), pub_topic_name(""), offset_sec(0.0), payload_len(256), do_start(false), mean_loop_dt(0.0)
{
    ros::param::get("~id", id);
    ros::param::get("~sub_topic_name", sub_topic_name);
    ros::param::get("~pub_topic_name", pub_topic_name);
    ros::param::get("~frequency", frequency);
    ros::param::get("~offset_sec", offset_sec);
    ros::param::get("~payload_len", payload_len);
    ros::param::get("~do_start", do_start);

    pub = nh.advertise<communication_test::timestemp>("/" + pub_topic_name, 10);
    sub = nh.subscribe<communication_test::timestemp>("/" + sub_topic_name, 10, &RTTTopic::msg_callback, this, ros::TransportHints().tcpNoDelay());
    if( do_start )
        offset_timer = nh.createTimer(ros::Duration(offset_sec), &RTTTopic::timer_callback, this, true);

    prev_seq = -1;
    prev_time = ros::Time::now();

    outofsync_cnt = 0;
}

RTTTopic::~RTTTopic()
{
    logger.set_outofsync(outofsync_cnt);
    logger.print_stat();
    std::cout << "node_loop_freq=" << 1.0 / mean_loop_dt * 1e6 << " kHz" << std::endl;
    logger.print_hist("node_" + id + "_rtt_topic_hist.txt");
}

void RTTTopic::run()
{
    // spin at maximum speed
    while(ros::ok()) {
        ros::Time t = ros::Time::now();

        ros::spinOnce();

        uint64_t loop_dt = (ros::Time::now() - t).toNSec();
        mean_loop_dt = (0.2 * (double)loop_dt) + 0.8 * mean_loop_dt;
    }
}

void RTTTopic::msg_callback(const communication_test::timestemp::ConstPtr& msg)
{
    // deep cpy, inc sequence counter, send
    communication_test::timestemp _msg = *msg;
    _msg.seq++;
    pub.publish(_msg);

    // get local time
    ros::Time now = ros::Time::now();
    uint64_t dt = (now - prev_time).toNSec() / 1000; // in µs

    // check if packages get out of synch
    if(msg->seq < prev_seq) {
        outofsync_cnt++;
    }
    else {
        logger.update(dt);
    }

    prev_time = now;
    prev_seq = _msg.seq;
}

void RTTTopic::timer_callback(const ros::TimerEvent& timerevent)
{
    // send first message
    communication_test::timestemp msg;
    msg.seq = 0;
    msg.payload.resize(payload_len, 0xFF);

    prev_seq = 0;
    prev_time = ros::Time::now();
    pub.publish(msg);

    ROS_INFO_STREAM("topic publishing started from node_" << id << " ...");
}