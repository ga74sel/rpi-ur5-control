#include <communication_test/rtt_service.h>
#include <stdio.h>

RTTService::RTTService()
    : id(""), is_client(false), srv_name(""), client_name(""), n(1000), freq(1000), payload_len(256), is_first(true)
{
    ros::param::get("~id", id);
    ros::param::get("~is_client", is_client);
    ros::param::get("~srv_name", srv_name);
    ros::param::get("~n", n);   
    ros::param::get("~freq", freq);
    ros::param::get("~payload_len", payload_len);

    if(!is_client)
        server = nh.advertiseService("/" + srv_name, &RTTService::run_srv_payload, this);
    else
        client = nh.serviceClient<communication_test::payload_srv>("/" + srv_name, true); // set persistant mode ... error if connection fails...

    ROS_INFO_STREAM("~id " << id);
    ROS_INFO_STREAM("~is_client " << is_client);
    ROS_INFO_STREAM("~srv_name " << srv_name);
    ROS_INFO_STREAM("~n " << n);   
    ROS_INFO_STREAM("~freq " << freq);
    ROS_INFO_STREAM("~payload_len " << payload_len);
}

RTTService::~RTTService()
{
    logger.print_stat();
    if(!is_client)
        logger.print_hist("node_" + id + "_rtt_service_server_hist.txt");
    else
        logger.print_hist("node_" + id + "_rtt_service_client_hist.txt");
}

void RTTService::run()
{
    if(!is_client)
        run_service();
    else
        run_client();
}

void RTTService::run_service()
{
    ROS_INFO_STREAM("service ready...");
    while(ros::ok()) {
        ros::spinOnce();
    }
}

void RTTService::run_client()
{
    ros::Rate rate(freq);
    ros::Time befor_time;

    // build message
    payload.resize(payload_len, 0xFF);
    communication_test::payload_srv payload_srv;
    payload_srv.request.payload = payload;

    // wait till service becomes avialable
    ROS_INFO_STREAM("client waiting for service...");
    ros::service::waitForService("/" + srv_name, -1);
    
    // do service calls
    ROS_INFO_STREAM("client doing service calls...");
    int i = 0;
    while( i < n && ros::ok()) {
        befor_time = ros::Time::now();
        if( client.call(payload_srv) ) {
            uint64_t dt = (ros::Time::now() - befor_time).toNSec() / 1000; // in µs
            logger.update(dt);
            i++;
        }
        rate.sleep();   
    }
}

bool RTTService::run_srv_payload(communication_test::payload_srv::Request& req, communication_test::payload_srv::Response &res)
{
    // copy the payload
    res.payload = req.payload;

    // measure frequency of client requests
    ros::Time now = ros::Time::now();
    uint64_t dt = (now - prev_time).toNSec() / 1000; // in µs
    if( !is_first )
        logger.update(dt);
    else
        is_first = false;
    prev_time = now;

    return true;
}