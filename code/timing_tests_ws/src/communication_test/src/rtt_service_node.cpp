#include <stdlib.h>
#include <timing_tests_util/priority_switcher.h>
#include <communication_test/rtt_service.h>
#include <string>

// parse arguments
int parse_arg(int argc, char **argv)
{
    int ret = 0;
    for(int i = 1; i < argc; ++i) {
        std::string arg(argv[i]);
        if( arg == "-s") {
            std::string val(argv[i+1]);
            if( val == "FIFO")
                ret = 1;
            if( val == "RR")
                ret = 2;
        }
    }
    return ret;
}

int main(int argc, char **argv)
{
    // 1. read args
    int policy = parse_arg(argc, argv);

    // 1. switch to realtime
    if( policy != 0 ) {
        PrioritySwitcher switcher( policy );
        if( switcher.swtich_realtime_prio() != 0 ) {
            fprintf(stderr, "ERROR: Failed to switch to realtime priority, run as root, exiting programm\n");
            return -1;
        }
    }

    // 2. run round trip time measurement
    ros::init(argc, argv, "service_node");
    ROS_INFO_STREAM("Starting rrt_service_node... \n");

    RTTService rrt;
    rrt.run();

    return 0;
}