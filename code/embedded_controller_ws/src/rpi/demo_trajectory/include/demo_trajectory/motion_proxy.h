/**
 * @file motion_proxy.h
 *
 * @brief advertise service to control robot
 *
 * @ingroup demo_trajectory
 *
 */

#ifndef MOTION_PROXY_H_
#define MOTION_PROXY_H_

#include <trajectory_generator/states.h>
#include <trajectory_generator/transitions.h>
#include <trajectory_generator/trajectories.h>

#include <demo_trajectory/moveto.h>

namespace demo_trajectory
{

using namespace trajectory_generator;

/**
 * Special Transition, that reacts to advertised service call
 *
 */
class MotionProxyTransition : public TransitionBase
{
public:
    /** 
    * @brief contructor
    *
    * @param source state ptr
    * @param destination state ptr
    * @param advertised service
    */
    MotionProxyTransition(const std::string& name, StateBaseConstPtr src, StateBasePtr dst, const std::string srv_name);
    virtual ~MotionProxyTransition();

    /** 
    * @brief init interal parameter
    *
    * @param ros nh for namespace
    */
    virtual bool init(ros::NodeHandle& nh);

    /** 
    * @brief start the state, called befor update
    *
    * @param jointstate
    * @param starting time
    */
    virtual void start(robot_msgs::JointState& state, const ros::Time& time);

    /**
    * @brief called periodically, return true is transition criteria is matched
    * 
    * @param jointstate
    * @param starting time
    * @param deltatime
    */
    virtual bool fulfilled(robot_msgs::JointState& state, const ros::Time& time, const ros::Duration& dt);

    /* srv handler */
    bool motion_proxy_handler(demo_trajectory::movetoRequest& req, demo_trajectory::movetoResponse& res);

private:
    ros::ServiceServer motion_proxy_srv;
    std::string srv_name;
    bool set;
};

}

#endif