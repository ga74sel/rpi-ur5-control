#include <trajectory_generator/trajectory_generator.h>
#include <trajectory_generator/moveto.h>

#include <controller_utilities/load_param.h>

#include <models/model_ur5.h>


/** 
* @brief demo function to test joint space motion
*
* @param update rate
*/
void demo_joint_controller(double rate = 1000.0)
{
    std::string joint_controller, jointstate_srv;
    config::Vector6 Q2;

    joint_controller = "joint_controller";
    jointstate_srv = "/tom_arm_controller/read_jointstate_intern";

    Q2 << 0.0, 0.0, 1.57, 0.0, 0.0, 0.0;

    // define states
    StateBasePtr s1 = std::make_shared<BreakTrajectoryState>("s1_break", joint_controller);

    StateBasePtr s2 = std::make_shared<MoveToPointJointState>("s2_joint_linear", joint_controller, Mode::PD, 10.0, Q2);

    StateBasePtr s3 = std::make_shared<JointTrajectoryState>("s3_joint_traj_sinus", joint_controller, Mode::PID,
                                        make_unique<LinearPhase>(1.0f/6.0),
                                        make_unique<SinusoidalJointPath>(0.5*M_PI));
    
    StateBasePtr s4 = std::make_shared<BreakTrajectoryState>("s4_final", joint_controller);

    // setup transition
    wait(s1, s2, 1.0);
    reach_joint(s2, s3);
    wait(s3, s4, 10000);

    // execute
    TrajectoryGeneratorPtr machine = std::make_shared<TrajectoryGenerator>(jointstate_srv, rate);
    machine->add(s1);
    machine->add(s2);
    machine->add(s3);

    // start
    if( !machine->start_request() )
        return;
    ros::spin();
    machine->stop_request();
}

/** 
* @brief demo function to test cartesian space motion
*
* @param update rate
*/
void demo_cartesian_controller(double rate = 1000.0)
{
    models::UR5Model ur5_model;
    if( !ur5_model.init() ) {
        ROS_INFO_STREAM("demo_manual_control_node: error init model");
    }

    std::string joint_controller, cartesian_controller, jointstate_srv, cartesian_position_controller;
    config::Vector3 X2;
    config::Matrix3 R2, R3, R4;
    config::Vector6 Q2;
    config::Matrix4 T0_W, TW_0;

    joint_controller = "joint_controller";
    cartesian_controller = "cartesian_controller";
    jointstate_srv = "/tom_arm_controller/read_jointstate_intern";

    T0_W = ur5_model.T0_W();
    TW_0 = T0_W.inverse();
    std::cout << "TW_0=" << TW_0 << std::endl;

    // intial joint trajectory
    Q2 << 5.33, -1.566, 1.55, -3.10, -1.55, 2.34;

    // intial cartesian position
    R2 << 0, 0, 1, 0, -1, 0, 1, 0, 0; math::transf_rot(TW_0, R2);
    R3 << -1, 0, 0, 0, -1, 0, 0, 0, 1; math::transf_rot(TW_0, R3);
    R4 << 0, 1, 0, 0, 0, 1, 1, 0, 0; math::transf_rot(TW_0, R4);

    X2 << 0.6, -0.4, 0.4; math::transf_pt(TW_0, X2);

    // circle trajectory
    config::Vector3 n, d;
    scalar_t phi = 0.0, theta = 0.0; // phi: angle z-axis, theta: angle x-axis
    n << sin(phi)*cos(theta), sin(phi)*sin(theta), cos(phi); math::transf_vec(TW_0, n);
    d << 0.5, -0.5, 0.4; math::transf_pt(TW_0, d);

    // 
    config::Vector3 n1, d1;
    phi = M_PI_2; theta = 0.0; // phi: angle z-axis, theta: angle x-axis
    n1 << sin(phi)*cos(theta), sin(phi)*sin(theta), cos(phi); math::transf_vec(TW_0, n1);
    d1 << 0.6, -0.6, 0.4; math::transf_pt(TW_0, d1);

    // rectancle trajectory
    config::Vector3 ra, rb, rc, rd;
    ra <<  0.6, -0.4, 0.6; math::transf_pt(TW_0, ra);
    rb <<  0.6, -0.8, 0.6; math::transf_pt(TW_0, rb);
    rc <<  0.6, -0.8, 0.2; math::transf_pt(TW_0, rc);
    rd <<  0.6, -0.4, 0.2; math::transf_pt(TW_0, rd);

    float lag = 2.0;

    // define States
    StateBasePtr s1 = std::make_shared<BreakTrajectoryState>("s1_break", joint_controller);
    StateBasePtr s2 = std::make_shared<MoveToPointJointState>("s2_joint_linear", joint_controller, Mode::PD, 10.0*lag, Q2);
    StateBasePtr s3 = std::make_shared<MoveToPointCartesianState>("s3_cartesian_linear", ur5_model, cartesian_controller, Mode::PD, 2.0*lag, X2, R2);

    // move to start
    StateBasePtr s5 = std::make_shared<MoveToPointCartesianState>("s5_cartesian_linear", ur5_model, cartesian_controller, Mode::PD, 4.0*lag, X2, R2);
    StateBasePtr s6 = std::make_shared<BreakTrajectoryState>("s6_break", cartesian_controller);

    // rectangle 1
    StateBasePtr s7 = std::make_shared<MoveToPointCartesianState>("s7_cartesian_linear", ur5_model, cartesian_controller, Mode::PD, 4.0*lag, ra, R2);
    StateBasePtr s8 = std::make_shared<MoveToPointCartesianState>("s8_cartesian_linear", ur5_model, cartesian_controller, Mode::PD, 4.0*lag, rb, R4);
    StateBasePtr s9 = std::make_shared<MoveToPointCartesianState>("s9_cartesian_linear", ur5_model, cartesian_controller, Mode::PD, 4.0*lag, rc, R2);
    StateBasePtr s10 = std::make_shared<MoveToPointCartesianState>("s10_cartesian_linear", ur5_model, cartesian_controller, Mode::PD, 4.0*lag, rd, R4);

    // rectangle 3
    StateBasePtr s15 = std::make_shared<MoveToPointCartesianState>("s15_cartesian_linear", ur5_model, cartesian_controller, Mode::PD, 4.0*lag, ra, R2);
    StateBasePtr s16 = std::make_shared<MoveToPointCartesianState>("s16_cartesian_linear", ur5_model, cartesian_controller, Mode::PD, 4.0*lag, rb, R4);
    StateBasePtr s17 = std::make_shared<MoveToPointCartesianState>("s17_cartesian_linear", ur5_model, cartesian_controller, Mode::PD, 4.0*lag, rc, R2);
    StateBasePtr s18 = std::make_shared<MoveToPointCartesianState>("s18_cartesian_linear", ur5_model, cartesian_controller, Mode::PD, 4.0*lag, rd, R4);

    // rotate
    StateBasePtr s19 = std::make_shared<MoveToPointCartesianState>("s19_cartesian_linear", ur5_model, cartesian_controller, Mode::PD, 4.0*lag, X2, R3);

    // circle
    StateBasePtr s20 = std::make_shared<CartesianTrajectoryState>("20_cartesian_circle", ur5_model, cartesian_controller, Mode::PID,
                                        make_unique<LinearPhase>(0.2/lag),
                                        make_unique<CircleCartesianPath>(n, d),
                                        make_unique<SLERPRotationPath>(R3));

    StateBasePtr s21 = std::make_shared<MoveToPointCartesianState>("s21_cartesian_linear", ur5_model, cartesian_controller, Mode::PD, 4.0*lag, X2, R2);


    // Lemniscate
    StateBasePtr s22 = std::make_shared<CartesianTrajectoryState>("20_cartesian_circle", ur5_model, cartesian_controller, Mode::PD,
                                        make_unique<LinearPhase>(0.1/lag),
                                        make_unique<LemniscateCartesianPath>(n1, d1),
                                        make_unique<SLERPRotationPath>(R2));

    StateBasePtr s23 = std::make_shared<BreakTrajectoryState>("s21_break", cartesian_controller);


    // setup transition
    wait(s1, s2, 2.0*lag);
    reach_joint(s2, s3);
    reach_cartesian(ur5_model, s3, s7);

    // rect 1
    reach_cartesian(ur5_model, s7, s8);
    reach_cartesian(ur5_model, s8, s9);
    reach_cartesian(ur5_model, s9, s10);
    reach_cartesian(ur5_model, s10, s15);

    // rect 3
    reach_cartesian(ur5_model, s15, s16);
    reach_cartesian(ur5_model, s16, s17);
    reach_cartesian(ur5_model, s17, s18);
    reach_cartesian(ur5_model, s18, s19);

    // circle
    reach_cartesian(ur5_model, s19, s20);
    wait(s20, s21, 20*lag);

    // curve
    reach_cartesian(ur5_model, s21, s22);
    wait(s22, s23, 20*lag);

    wait(s23, s3, 10000);

    // build machine
    TrajectoryGeneratorPtr machine = std::make_shared<TrajectoryGenerator>(jointstate_srv, rate);
    machine->add(s1);
    machine->add(s2);
    machine->add(s3);

    machine->add(s5);
    machine->add(s6);

    machine->add(s7);
    machine->add(s8);
    machine->add(s9);
    machine->add(s10);

    machine->add(s15);
    machine->add(s16);
    machine->add(s17);
    machine->add(s18);

    machine->add(s19);
    machine->add(s20);
    machine->add(s21);
    machine->add(s22);
    machine->add(s23);

    // start
    if( !machine->start_request() )
        return;
    ros::spin();
    machine->stop_request();
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "trajectory_generator");
    ROS_INFO_STREAM("Starting trajectory_generator... \n");

    // 1. load fequency
    double freq = 1000.0;
    if( ros::param::get("trajectory_gen/freq", freq) )
        std::cout << "trajectory_gen: freq=" << freq << std::endl;
    else
        ROS_ERROR_STREAM("trajectory_gen: error setting freq");

    //demo_joint_controller(freq);
    demo_cartesian_controller(freq);
    return 0;
}
