#include <trajectory_generator/trajectory_generator.h>
#include <trajectory_generator/moveto.h>

#include <controller_utilities/load_param.h>

#include <models/model_ur5.h>

#include "demo_trajectory/motion_proxy.h"


/** 
* @brief wait transition between two states
*
* @param source state
* @param destination state
* @param service anme to wait for
*/
void wait_command(StateBasePtr a, StateBasePtr b, std::string srv_name) {
    TransitionBasePtr t = std::make_shared<MotionProxyTransition>("T", a, b, srv_name);
    a->add_transition( t );
}

/** 
* @brief demo function to test motion proxy
*
* @param update rate
*/
void demo_motion_proxy(double rate = 1000.0)
{
    models::UR5Model ur5_model;

    std::string joint_controller, cartesian_controller, jointstate_srv;
    config::Vector6 Q1, Q2;
    config::Vector3 X2, n, d;
    config::Matrix3 R2;
    config::Matrix4 T0_W, TW_0;

    joint_controller = "joint_controller";
    cartesian_controller = "cartesian_controller";
    jointstate_srv = "/tom_arm_controller/read_jointstate_intern";

    T0_W = ur5_model.T0_W();
    TW_0 = T0_W.inverse();
    std::cout << "TW_0=" << TW_0 << std::endl;

    Q2 << 5.33, -1.566, 1.55, -3.10, -1.55, 2.34;
    R2 << 0, 0, 1, 0, -1, 0, 1, 0, 0; math::transf_rot(TW_0, R2);
    X2 << 0.6, -0.6, 0.4; math::transf_pt(TW_0, X2);

    // define States
    StateBasePtr s1 = std::make_shared<BreakTrajectoryState>("s1_break", joint_controller);

    StateBasePtr s2 = std::make_shared<MoveToPointJointState>("s2_joint_linear", joint_controller, Mode::PD, 10.0, Q1, Q2);

    StateBasePtr s3 = std::make_shared<BreakTrajectoryState>("s3_wait", cartesian_controller);

    StateBasePtr s4 = std::make_shared<MoveToPointCartesianState>("s4_cartesian_linear", ur5_model, cartesian_controller, Mode::PD, 10.0, X2, R2);

    // setup transition
    wait(s1, s2, 1.0);
    reach_joint(s2, s3);
    wait_command(s3, s4, "/moveto");
    reach_cartesian(ur5_model, s4, s3);

    // execute
    TrajectoryGeneratorPtr machine = std::make_shared<TrajectoryGenerator>(jointstate_srv, rate);
    machine->add(s1);
    machine->add(s2);
    machine->add(s3);
    machine->add(s4);

    // start
    if( !machine->start_request() )
        return;
    ros::spin();
    machine->stop_request();
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "trajectory_generator");
    ROS_INFO_STREAM("Starting trajectory_generator... \n");

    // 1. load fequency
    double freq = 1000.0;
    if( ros::param::get("trajectory_gen/freq", freq) )
        std::cout << "trajectory_gen: freq=" << freq << std::endl;
    else
        ROS_ERROR_STREAM("trajectory_gen: error setting freq");

    //demo_joint_controller(freq);
    demo_cartesian_controller(freq);

    return 0;
}
