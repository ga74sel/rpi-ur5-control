/**
 * @file gamepad_controller.h
 *
 * @brief Special state for gamepad ( ros joy package )
 *
 * @ingroup demo_manual_control
 *
 */

#include <trajectory_generator/state_base.h>

#include <robot_msgs/writecontroldata.h>

#include <controller_utilities/math.h>

#include <controller_msgs/mode.h>
#include <controller_msgs/setmode.h>

#include <model_interface/model_base.h>

#include <sensor_msgs/Joy.h>

namespace gamepad_controller
{

enum JOYKEY
{
    // key digital
    KEY_A = 0,
    KEY_B = 1,
    KEY_Y = 2,
    KEY_X = 3,
    KEY_LB = 4,
    KEY_RB = 5,
    // joystick analog
    JOY1_X = 0,
    JOY1_Y = 1,
    JOY2_X = 3,
    JOY2_Y = 4,
    ARROW_UP_DOWN = 6,
    ARROW_LEFT_RIGHT = 7,
    // key analog
    KEY_LT = 2,
    KEY_RT = 5
};

/**
 * Cartesian control of end-effector pose based on gamepad
 *
 */
class CartesianGamepadState : public trajectory_generator::StateBase
{
public:

    /*
     * key mapping
     */
    enum JOYMAP
    {
        // position x,y,z motion
        MOVE_UP = KEY_LB,
        MOVE_DOWN = KEY_LT,
        MOVE_LEFT_RIGHT = JOY1_X,
        MOVE_FORWARD_BACKWARD = JOY1_Y,

        // orientation yaw, pitch roll
        MOVE_YAW_UP = KEY_RB,
        MOVE_YAW_DOWN = KEY_RT,
        MOVE_PITCH_UP_DOWN = JOY2_X,
        MOVE_ROLL_UP_DOWN = JOY2_Y,

        // state selection
        STATE_SELECT = KEY_Y
    };

    /*
     * different gamapad states
     */
    enum ManualMode
    {
        POSITION = 0,       // position, orientation fixed
        ORIENTATION = 1,    // orientation, position fixed
        BOTH = 2            // position and orientation free
    };
    std::vector<std::string> ManualModeName
    {
        "position",
        "orientation",
        "position+orientation"
    };

    /** 
    * @brief contructor
    *
    * @param name of state
    * @param kinematic and dynamic model
    * @param controller name
    * @param control mode
    */
    CartesianGamepadState(const std::string& name, model_interface::ModelBase& model, const std::string& controller_name, const controller_msgs::Mode& mode);
    virtual ~CartesianGamepadState();

private:
    /** 
    * @brief init interal parameter
    *
    * @param ros nh for namespace
    */
    bool init(ros::NodeHandle& nh);
    
    /** 
    * @brief start the state, called befor update
    *
    * @param jointstate
    * @param starting time
    */
    virtual void start(robot_msgs::JointState& state, const ros::Time& time);

    /** 
    * @brief performs update step of the state, called periodically
    *
    * @param jointstate
    * @param starting time
    * @param deltatime
    */
    virtual void update(robot_msgs::JointState& state, const ros::Time& time, const ros::Duration& dt);

    /**
    * @brief parse new key command
    */
    void parse_joy(const std::vector<float>& axes, const std::vector<int>& buttons);

    /* callbacks */
    void joy_callback(const sensor_msgs::JoyConstPtr& msg);

private:
    ros::ServiceClient set_command_client;
    ros::ServiceClient set_mode_client;
    
    ManualMode manual_mode, prev_manual_mode;
    ros::Subscriber joy_sub;

    controller_msgs::Mode mode;
    ros::Time start_time;

    model_interface::ModelBase& model;

    config::Vector3 Xpos;
    config::Quaternion q;

    config::Vector6 Xp, Xpd, Xp_step;   // velo skew, desired velo skew, step size
    config::Matrix6 Kp;                 // gain

    bool is_pressed;
};


/**
 * Joint control of robot joints based on gamepad
 *
 */
class JointGamepadState : public trajectory_generator::StateBase
{
public:

    /*
     * key mapping
     */
    enum JOYMAP
    {
        // position x,y,z motion
        JOINT1_UP_DOWN = JOY1_X,
        JOINT2_UP_DOWN = JOY1_Y,
        JOINT3_UP_DOWN = JOY2_X,
        JOINT4_UP_DOWN = JOY2_Y,

        JOINT5_UP = KEY_LB,
        JOINT5_DOWN = KEY_LT,
        JOINT6_UP = KEY_RB,
        JOINT6_DOWN = KEY_RT
    };

    /** 
    * @brief contructor
    *
    * @param name of state
    * @param controller name
    * @param control mode
    */
    JointGamepadState(const std::string& name, const std::string& controller_name, const controller_msgs::Mode& mode);
    virtual ~JointGamepadState();

private:
    /** 
    * @brief init interal parameter
    *
    * @param ros nh for namespace
    */
    bool init(ros::NodeHandle& nh);

    /** 
    * @brief start the state, called befor update
    *
    * @param jointstate
    * @param starting time
    */
    virtual void start(robot_msgs::JointState& state, const ros::Time& time);

    /** 
    * @brief performs update step of the state, called periodically
    *
    * @param jointstate
    * @param starting time
    * @param deltatime
    */
    virtual void update(robot_msgs::JointState& state, const ros::Time& time, const ros::Duration& dt);

    /**
    * @brief parse new key command
    */
    void parse_joy(const std::vector<float>& axes, const std::vector<int>& buttons);

    /* callbacks */
    void joy_callback(const sensor_msgs::JoyConstPtr& msg);

private:
    ros::ServiceClient set_command_client;
    ros::ServiceClient set_mode_client;

    ros::Subscriber joy_sub;

    controller_msgs::Mode mode;
    ros::Time start_time;

    config::Vector6 Q;

    config::Vector6 Qp, Qpd, Qp_step;   // velo, desired velo, step size
    config::Matrix6 Kp;                 // gain
};

/**
* A transition based on pressing keys on the gamepad
*/
class GamepadTransition : public trajectory_generator::TransitionBase
{
public:
    /** 
    * @brief contructor
    *
    * @param source state ptr
    * @param destination state ptr
    * @param key identifier
    */
    GamepadTransition(const std::string& name, trajectory_generator::StateBaseConstPtr src, trajectory_generator::StateBasePtr dst, int key_id)
        : TransitionBase( name, src, dst ),
          key_id(key_id),
          set(false)
    {
    }
    virtual ~GamepadTransition() {}

private:
    /** 
    * @brief init interal parameter
    *
    * @param ros nh for namespace
    */
    bool init(ros::NodeHandle& nh) {
        set = false;
        joy_sub = nh.subscribe<sensor_msgs::Joy>("/joy", 1, &GamepadTransition::joy_callback, this);
        return true;   
    }

    /** 
    * @brief start the state, called befor update
    *
    * @param jointstate
    * @param starting time
    */
    void start(robot_msgs::JointState& state, const ros::Time& time) {
        set = false;
    }

    /**
    * @brief called periodically, return true is transition criteria is matched
    * 
    * @param jointstate
    * @param starting time
    * @param deltatime
    */
    virtual bool fulfilled(robot_msgs::JointState& state, const ros::Time& time, const ros::Duration& dt) {
        return set;
    }

    void joy_callback(const sensor_msgs::JoyConstPtr& msg) {
        if(set)
            return;

        if( key_id <= msg->buttons.size() ) {
            if( msg->buttons[key_id] == 1 )
                set = true;
        }
    }

private:
    ros::Subscriber joy_sub;
    int key_id;
    bool set;
};
}
