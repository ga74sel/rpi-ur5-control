#include <tom_arm_controller/tom_arm_controller.h>

#include <controller_utilities/load_param.h>

#include <robot_msgs/computecontroldata.h>

namespace ics = tum_ics_ur_robot_lli;
namespace ics_cntr = tum_ics_ur_robot_lli::RobotControllers;

ics_cntr::TOMArmController::TOMArmController(hardware_interface::CommunicationBase& hw_comm, double weight, const QString &name)
    : ControlEffort(name, SPLINE_TYPE, JOINT_SPACE, weight),
      is_active(false),
      hw_comm(hw_comm)
{
    // ros connections
    joint_state_pub = nh.advertise<robot_msgs::controldata>("jointstate_intern", 1);
    joint_state_srv = nh.advertiseService("/tom_arm_controller/read_jointstate_intern", &ics_cntr::TOMArmController::read_joint_state_handler, this);
}

ics_cntr::TOMArmController::~TOMArmController()
{
}

void ics_cntr::TOMArmController::setQInit(const JointState& qinit)
{
    m_qInit=qinit;
}
void ics_cntr::TOMArmController::setQHome(const JointState& qhome)
{
    m_qHome=qhome;
}
void ics_cntr::TOMArmController::setQPark(const JointState& qpark)
{
    m_qPark=qpark;
}


bool ics_cntr::TOMArmController::init()
{
    bool ret = true;

    // initalize model
    if( model.init() ) {
        Yr = model.init_Yr();
        th = model.init_th();
    }

    // initalize the communication
    if( !hw_comm.init( nh ) ) {
        ret = false;
        ROS_ERROR_STREAM("TOMArmController: error init controller communication");
    }
    compute_control_client = nh.serviceClient<robot_msgs::computecontroldata>("controller_manager/compute_control", true);

    return ret;
}

bool ics_cntr::TOMArmController::start()
{
}

ics::Vector6d ics_cntr::TOMArmController::update(const RobotTime &time, const JointState &current)
{
    ics::Vector6d tau;
    tau.setZero();

    // save inital joint position
    if( !is_active ) {
       Q_inital = current.q.cast<config::scalar_t>();
       is_active=true;
       prev_t = time.tRc();
    }

    // copy ics joinstate to rpi jointstate
    jointstate.Q = current.q.cast<config::scalar_t>();
    jointstate.Qp = current.qp.cast<config::scalar_t>();
    jointstate.Qpp = current.qpp.cast<config::scalar_t>();

    // assemble rpi controller msg
    ros::Duration dt = time.tRc() - prev_t;
    prev_t = time.tRc();
    robot_msgs::computecontroldata srv;
    srv.request.data.header.stamp = time.tRc();
    srv.request.data.header.dt = dt.toSec();
    srv.request.data.Q = std::vector<config::scalar_t>(jointstate.Q.data(), jointstate.Q.data() + jointstate.Q.size());
    srv.request.data.Qp = std::vector<config::scalar_t>(jointstate.Qp.data(), jointstate.Qp.data() + jointstate.Qp.size());
    srv.request.data.Qpp = std::vector<config::scalar_t>(jointstate.Qpp.data(), jointstate.Qpp.data() + jointstate.Qpp.size());

    // call the rpi service
    if( compute_control_client.call( srv ) ) {
        jointstate.tau = config::VectorDOF( srv.response.data.tau.data() );
    } else {
        // error case
        ROS_ERROR_THROTTLE(0.5, "communication error stopping!!!");
        config::Vector6 Qrp, Qrpp;
        Qrp.setZero(); Qrpp.setZero();
        model.Yr(Yr, jointstate.Q, jointstate.Qp, Qrp, Qrpp );
        jointstate.tau = Yr * th; // compensate g
    }

    // Timing measurement
    static int i = 0;
    static double dt_mean = 0.0;
    dt_mean += dt.toSec();
    if( i == 1000 ) {
        ROS_INFO_STREAM("Time mean=" << dt_mean << " ms");
        dt_mean = 0.0;
        i = 0;
    }
    i++;

    // publish jointstate
    publish_jointstate(jointstate, time.tRc());
    tau = jointstate.tau.cast<double>();
    return tau;
}

bool ics_cntr::TOMArmController::stop()
{
    ROS_INFO_STREAM("stop()");
}

void ics_cntr::TOMArmController::publish_jointstate(const robot_msgs::JointState& state, const ros::Time &time)
{
    robot_msgs::controldata msg;
    msg.header.stamp = time;
    msg.Q = std::vector<float>(state.Q.data(), state.Q.data() + state.Q.size());
    msg.Qp = std::vector<float>(state.Qp.data(), state.Qp.data() + state.Qp.size());
    msg.Qpp = std::vector<float>(state.Qpp.data(), state.Qpp.data() + state.Qpp.size());
    joint_state_pub.publish(msg);
}

bool ics_cntr::TOMArmController::read_joint_state_handler(robot_msgs::readcontroldata::Request& req, robot_msgs::readcontroldata::Response& res)
{
    if( is_active ) {
        // lock srv
        pthread_mutex_lock(&srv_mutex);

        res.data.header.stamp = ros::Time::now();
        res.data.Q = std::vector<float>(jointstate.Q.data(), jointstate.Q.data() + jointstate.Q.size());
        res.data.Qp = std::vector<float>(jointstate.Qp.data(), jointstate.Qp.data() + jointstate.Qp.size());
        res.data.Qpp = std::vector<float>(jointstate.Qpp.data(), jointstate.Qpp.data() + jointstate.Qpp.size());

        // unlock srv
        pthread_mutex_unlock(&srv_mutex);

        return true;
    }
    return false;
}

/*
ics::Vector6d ics_cntr::TOMArmController::update(const RobotTime &time, const JointState &current)
{
    ics::Vector6d tau;
    tau.setZero();

    // copy ics joinstate to rpi jointstate
    jointstate.Q = current.q.cast<config::scalar_t>();
    jointstate.Qp = current.qp.cast<config::scalar_t>();
    jointstate.Qpp = current.qpp.cast<config::scalar_t>();

    // save inital joint position
    if( !is_active ) {
        Q_inital = current.q.cast<config::scalar_t>();
        is_active=true;
    }

    // get latest control command
    ros::Time t;
    if( hw_comm.read(jointstate, t) ) {
        tau = jointstate.tau.cast<ics::Vector6d::Scalar>();
    }
    else {
        // error: connection broken
        tau.setZero();
    }

    // make joinstate available for hw
    hw_comm.write(jointstate, time.tRc());

    // publish jointstate
    publish_jointstate(jointstate, time.tRc());
    return tau;
}
*/


/*
{
     ics::Vector6d tau;
    tau.setZero();

    // save inital joint position
    if( !is_active ) {
       Q_inital = current.q.cast<config::scalar_t>();
       is_active=true;
       prev_t = time.tRc();
    }

    // copy ics joinstate to rpi jointstate
    jointstate.Q = current.q.cast<config::scalar_t>();
    jointstate.Qp = current.qp.cast<config::scalar_t>();
    jointstate.Qpp = current.qpp.cast<config::scalar_t>();

    // assemble rpi controller msg
    ros::Duration dt = time.tRc() - prev_t;
    prev_t = time.tRc();
    robot_msgs::computecontroldata srv;
    srv.request.data.header.stamp = time.tRc();
    srv.request.data.header.dt = dt.toSec();
    srv.request.data.Q = std::vector<config::scalar_t>(jointstate.Q.data(), jointstate.Q.data() + jointstate.Q.size());
    srv.request.data.Qp = std::vector<config::scalar_t>(jointstate.Qp.data(), jointstate.Qp.data() + jointstate.Qp.size());
    srv.request.data.Qpp = std::vector<config::scalar_t>(jointstate.Qpp.data(), jointstate.Qpp.data() + jointstate.Qpp.size());

    // call the rpi service
    logger_clock = ros::Time::now();
    if( compute_control_client.call( srv ) ) {
        jointstate.tau = config::VectorDOF( srv.response.data.tau.data() );
    } else {
        // error case
        ROS_ERROR_THROTTLE(0.5, "communication error stopping!!!");
        config::Vector6 Qrp, Qrpp;
        Qrp.setZero(); Qrpp.setZero();
        model.Yr(Yr, jointstate.Q, jointstate.Qp, Qrp, Qrpp );
        jointstate.tau = Yr * th; // compensate g
    }

    uint64_t clock_dt = (ros::Time::now() - logger_clock).toNSec() / 1000; // in µs
    logger.update( clock_dt );

    // Timing measurement
    static int i = 0;
    static double dt_mean = 0.0;
    dt_mean += dt.toSec();
    if( i == 1000 ) {
        ROS_INFO_STREAM("Time mean=" << dt_mean << " ms");
        dt_mean = 0.0;
        i = 0;
    }
    i++;

    // publish jointstate
    publish_jointstate(jointstate, time.tRc());
    tau = jointstate.tau.cast<double>();
    return tau;
}
*/
