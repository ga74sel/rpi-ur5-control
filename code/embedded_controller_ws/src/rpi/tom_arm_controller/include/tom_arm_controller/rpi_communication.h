#ifndef RPI_COMMUNICATION_H_
#define RPI_COMMUNICATION_H_

#include <pthread.h>

#include <robot_msgs/controldata.h>
#include <robot_msgs/joint_state.h>
#include <robot_msgs/readcontroldata.h>
#include <hardware_interface/communication_base.h>

namespace rpi
{

/*
 * Defines the communication interface between the robot connected to the host commputer and rpi controller
 * read()
 *  - returns last commmand recived from controller
 * write()
 *  - stores the joint state
 */
class RPICommunication : public hardware_interface::CommunicationBase 
{
public:
    enum State {CONSTRUCTED, INITIALIZED, RECEIVED1, RECEIVED2, CONNECTED, ERROR };

    RPICommunication(const std::string& src_name, const std::string& dst_name, double time_out);
    virtual ~RPICommunication();

    bool init(ros::NodeHandle& nh);

    /* check if communication is still possible */
    bool is_alive();

    /* read current joint state */
    bool read(robot_msgs::JointState& jointstate, ros::Time& time);

    /* write new joint state */
    bool write(const robot_msgs::JointState& jointstate, const ros::Time& time);

private:
    bool joint_state_handler(robot_msgs::readcontroldata::Request& req, robot_msgs::readcontroldata::Response& res);

private:
    State state;

    ros::ServiceServer joint_state_server;      // return current robot state
    ros::ServiceClient torque_client;           // read current torque data

    robot_msgs::JointState jointstate;          

    ros::Time cur_time;                         // time at which a new jointstate was set
    ros::Time cmd_time;                         // last time a valid controller command was received
    double time_out;                            // time at which connection is considered to be lost

    std::string src_name, dst_name;

     pthread_mutex_t jointstate_mutex;
};

}

#endif
