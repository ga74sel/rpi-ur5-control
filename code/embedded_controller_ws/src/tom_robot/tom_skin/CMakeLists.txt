cmake_minimum_required(VERSION 2.8.3)
project(tom_skin)

find_package(catkin REQUIRED)

catkin_package(
    CATKIN_DEPENDS tum_ics_skin_driver_events tum_ics_skin_full_config
)

######################################################################################
# TUM_ICS_NOTE: We need to modify the installation variables to be compliant with the
#   debtools. This function is controlled with the global variable -DDEB_BUILD
#   (true/false)
include(/usr/share/cmake/CMakeTools/ics_tools.cmake)
ics_ros_debgen()

foreach(dir launch)
    install(DIRECTORY ${dir}/
        DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}/${dir})
endforeach(dir)
