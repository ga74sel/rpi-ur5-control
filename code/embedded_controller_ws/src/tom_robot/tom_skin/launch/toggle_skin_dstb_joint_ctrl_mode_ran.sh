#!/bin/bash

# get location of this script
sourceDir=$BASH_SOURCE
runDir=$0
thisDir=$(cd $(dirname $sourceDir); pwd)


if [[ $sourceDir != $runDir ]]; then
    echoc BRED "ERROR: You must run this script."
    exit 1
fi

printHelp()
{
    echoc BWHITE "NAME"
    echoc NOCOLOR "  toggle_skin_dstb_joint_ctrl_mode_ran"
    echoc BWHITE "SYNOPSIS"
    echoc NOCOLOR "  toggle_skin_dstb_joint_ctrl_mode"
    echoc BWHITE "DESCRIPTION"
    echoc NOCOLOR "  Toggels the mode of the skin distributed joint controller"
    echoc NOCOLOR "  in a random mannner."
    echoc BWHITE "AUTHORS"
    echoc NOCOLOR " Florian Bergner <florian.bergner@tum.de>"
}


main()
{
    if [ "$1" == "-h" ] || [ "$1" == "--help" ]; then
        printHelp
        exit 1
    fi
    
    toggleDelay=$1
    
    flag=true
    
    while [ true ]
    do


        if [ $flag = true ]; then
            cmd="rosservice call /tom/arm_left/setSkinDstbJointControlCmd \"cmd: 'trq ref on'\""
            flag=false
        else
            cmd="rosservice call /tom/arm_left/setSkinDstbJointControlCmd \"cmd: 'trq ref off'\""
            flag=true
        fi
    
        echo $cmd
        eval $cmd
    
        ran=$(shuf -i0-5 -n1)
        delay=$(echo "scale=1; 0.5+$ran*0.5" | bc)
    
        echoc LGREEN "delay: " LBLUE "$delay s"
        sleep $delay
    done
}


cleanup()
{
    echoc BYELLOW "\nExit (OK)"
    exit 0
}

# catch Ctrl+C
trap "cleanup" 2
#trap "cleanup" EXIT

main "$@"