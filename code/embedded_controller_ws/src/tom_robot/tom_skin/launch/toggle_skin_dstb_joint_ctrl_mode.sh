#!/bin/bash

# get location of this script
sourceDir=$BASH_SOURCE
runDir=$0
thisDir=$(cd $(dirname $sourceDir); pwd)


if [[ $sourceDir != $runDir ]]; then
    echoc BRED "ERROR: You must run this script."
    exit 1
fi

printHelp()
{
    echoc BWHITE "NAME"
    echoc NOCOLOR "  toggle_skin_dstb_joint_ctrl_mode"
    echoc BWHITE "SYNOPSIS"
    echoc NOCOLOR "  toggle_skin_dstb_joint_ctrl_mode [" GREEN "toggle-delay" NOCOLOR "]"
    echoc BWHITE "DESCRIPTION"
    echoc NOCOLOR "  Toggels the mode of the skin distributed joint controller."
    echoc NOCOLOR "  The mode toggels between 'trq ref on' and 'trq ref off'"
    echoc NOCOLOR "  If no [toggle-delay] in seconds is given then a default delay of 1s is used."
    echoc BWHITE "AUTHORS"
    echoc NOCOLOR " Florian Bergner <florian.bergner@tum.de>"
}


main()
{
    if [ "$1" == "-h" ] || [ "$1" == "--help" ]; then
        printHelp
        exit 1
    fi
    
    toggleDelay=$1
    
    if [ -z $toggleDelay ]; then
        toggleDelay=1
    fi
    
    if [[ ! $toggleDelay =~ ^[0-9]+([.][0-9]+)?$ ]]; then
        echoc BLRED "ERROR: Invalid toggle delay '$toggleDelay'"
        printHelp
        exit 1
    fi

    flag=true
    
    while [ true ]
    do
        if [ $flag = true ]; then
            cmd="rosservice call /tom/arm_left/setSkinDstbJointControlCmd \"cmd: 'trq ref on'\""
            flag=false
        else
            cmd="rosservice call /tom/arm_left/setSkinDstbJointControlCmd \"cmd: 'trq ref off'\""
            flag=true
        fi
    
        echo $cmd
        eval $cmd
    
        #rosservice call /tom/arm_left/setSkinDstbJointControlCmd "cmd: 'trq ref on'"
    
        sleep $toggleDelay
    done
}


cleanup()
{
    echoc BYELLOW "\nExit (OK)"
    exit 0
}

# catch Ctrl+C
trap "cleanup" 2
#trap "cleanup" EXIT

main "$@"