#include "robot_visualizer/robot_visualizer.h"

#include <models/model_ur5.h>

int main(int argc, char **argv)
{
  ros::init(argc, argv, "robot_visualizer");
  ROS_INFO_STREAM("Starting robot_visualizer... \n");

  models::UR5Model ur5_model;
  
  robot_visualizer::RobotVisualizer visualizer(ur5_model);
  visualizer.start();

  return 0;
}
