#include "robot_visualizer/robot_visualizer.h"

#include <controller_utilities/load_param.h>

#include <tf_conversions/tf_eigen.h>
#include <Eigen/Geometry> 

using namespace robot_visualizer;

RobotVisualizer::RobotVisualizer(model_interface::ModelBase& model, double rate, const ros::NodeHandle& nh_)
    : nh(nh_),
      rate(rate),
      model(model)
{
}

RobotVisualizer::~RobotVisualizer()
{
}

void RobotVisualizer::set_T0_W(const config::Matrix4& T0_W)
{
    this->T0_W = T0_W;
}

bool RobotVisualizer::start()
{
    if( !init() ) {
        ROS_ERROR_STREAM("RobotVisualizer: error init");
        return false;
    }

    run();
    return true;
}

bool RobotVisualizer::init()
{
    bool ret = true;
    std::string err_str;

    if( !model.init() ) {
        ROS_INFO_STREAM("RobotVisualizer: error init model");
        ret = false;
    }

    // load topic name for joint states
    std::string joint_topic;
    if( !load_param::load_param(model.get_name(), "joint_topic", joint_topic, err_str) ) {
        ret = false; ROS_ERROR_STREAM("RobotVisualizer:" << " error setting joint_topic: " << err_str);
    }

    // load joint names
    std::vector<std::string> joint_names;
    if( !load_param::load_param_vec(model.get_name(), "joint_names", joint_names, config::NJOINT, err_str) ) {
        ret = false; ROS_ERROR_STREAM("RobotVisualizer:" << " error setting joint_names: " << err_str);
    }

    // set base wrt world transformation
    set_T0_W( model.T0_W() );
    std::cout << "RobotVisualizer: T0_W=" << std::endl << model.T0_W()  << std::endl;

    // load inital Q value from ros parameter service
    std::vector<double> vev_q;
    if( load_param::load_param_vec(model.get_name(), "Q_INIT", vev_q, config::NJOINT, err_str) ) {
        for( int i = 0; i < Q.size(); ++i)
            *(Q.data() + i) = vev_q[i];
    } else {
        ret = false; ROS_ERROR_STREAM("RobotVisualizer:" << " error setting Q0: " << err_str);
    }

    // setup transformations
    joint_transform.resize(config::NJOINT);
    joint_transform_W.resize(config::NJOINT);
    for(size_t i = 0; i < config::NJOINT; ++i) {
        joint_transform[i].setZero();
        joint_transform_W[i].setZero();
    }
    tf_stamped_transform.resize(config::NJOINT);
    joint_names_dh.resize(config::NJOINT);

    // setup dh tf names
    base_name = "tom_base_link";
    for(size_t i = 0; i < config::NJOINT; ++i) {
        joint_names_dh[i] = "dh_" + std::to_string(i);
    }

    // joint states msg
    joint_state.name = joint_names;
    joint_state.position.resize(config::NJOINT);

    // publisher, subscriber
    jointstate_pub = nh.advertise<sensor_msgs::JointState>(joint_topic, 10);
    jointdata_intern_sub = nh.subscribe<robot_msgs::controldata>("jointstate_intern", 10, &RobotVisualizer::controldata_callback, this);

    return ret;
}

void RobotVisualizer::run()
{
    ros::Rate loop_rate(rate);
    while( ros::ok() )
    {
        update();
        ros::spinOnce();
        loop_rate.sleep();
    }
}

void RobotVisualizer::update()
{
    // compute transformation matrix of joints
    forward_kinematic();
    // broadcast as tf tree
    broadcast_tftree();
    // publish jointstate topic
    publish_jointstate();
}

void RobotVisualizer::forward_kinematic()
{
    // compute forward kinematic of each joint (TODO: use some class)
    for( int j = 0; j < config::NJOINT; j++) {
        model.T_j(joint_transform[j], Q, j);
        joint_transform_W[j] = T0_W * joint_transform[j];
    }
}

void RobotVisualizer::broadcast_tftree()
{
    Eigen::Affine3d eigen_transform;
    tf::Transform tf_transform;

    // construct tf transformations
    for( size_t i = 0; i < config::NJOINT; ++i ) {
        eigen_transform = (Eigen::Affine3d)joint_transform_W[i].cast<double>();
        tf::transformEigenToTF(eigen_transform, tf_transform);

        tf_stamped_transform[i].setData(tf_transform);
        tf_stamped_transform[i].stamp_ = ros::Time::now();
        tf_stamped_transform[i].frame_id_ = base_name;
        tf_stamped_transform[i].child_frame_id_ = joint_names_dh[i];
    }

    // broadcast stack of transformations
    br.sendTransform(tf_stamped_transform);
}

void RobotVisualizer::publish_jointstate()
{
    std::vector<float> Q_vec = std::vector<float>(Q.data(), Q.data() + Q.size());

    joint_state.header.stamp = ros::Time::now();
    joint_state.position = std::vector<double>(Q_vec.begin(), Q_vec.end());
    jointstate_pub.publish(joint_state);
}

void RobotVisualizer::controldata_callback(const robot_msgs::controldataConstPtr& msg)
{
    // cpy joint positions
    Q = config::Vector6(msg->Q.data());
}
