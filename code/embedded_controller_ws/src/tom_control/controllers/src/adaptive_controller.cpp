#include "controllers/adaptive_controller.h"

#include <controller_utilities/load_param.h>

using namespace controller;
using namespace config;

AdaptiveController::AdaptiveController(model_interface::ModelBase &model, std::string name, Space space)
    : controller_interface::ControllerBase(name, space),
      model(model),
      Kd(Matrix6::Zero()),
      gamma_inv(1.0),
      integ_thresh(1e-6)
{
}

AdaptiveController::~AdaptiveController()
{
}

/* parameter update step size */
void AdaptiveController::set_gamma_inv(const float& gamma_inv)
{
    this->gamma_inv = gamma_inv;
}

/* gain Kd */
void AdaptiveController::set_Kd(const config::Matrix6& Kd)
{
    this->Kd = Kd;
}
void AdaptiveController::set_Kd(const float& kd)
{
    Kd = kd * Matrix6::Identity();
}
Matrix6 AdaptiveController::get_Kd() const {
    return Kd;
}

/* load gain values from ros_parameters service */
bool AdaptiveController::load_gains(const std::string& ns)
{
    // load Kp form ros parameter server
    std::vector<double> vec;
    std::string err_str;
    if( load_param::load_param_vec(ns, "gains_d", vec, config::NJOINT, err_str) ) {
        Kd.setZero();
        for(int i = 0; i < config::NJOINT; ++i)
            Kd(i, i) = vec[i];
    } else {
        ROS_ERROR_STREAM( err_str );
        return false;
    }
    return true;
}


/* init controller parameters, load from param server in nh namespace */
bool AdaptiveController::init(ros::NodeHandle& nh)
{
    // init parameter vector th
    if( model.init() ) {
        Yr = model.init_Yr();
        th = model.init_th();
        return true;
    }
    return false;
}

/* periodically called */
VectorDOF AdaptiveController::update(robot_msgs::JointState& state, const ros::Time& time, const ros::Duration& dt)
{
    // evaluate regressor
    model.Yr(Yr, state.Q, state.Qp, state.Qrp, state.Qrpp);

    // error space
    Vector6 Sq = state.Qp - state.Qrp;

    // adaptation law (& integration)
    if( Sq.norm() > integ_thresh )
        th -= scalar_t(dt.toSec()) * gamma_inv * Yr.transpose() * Sq;

    // tau
    return -Kd*Sq + Yr * th;
}
