#include "controllers/cartesian_controller.h"

#include <controller_utilities/load_param.h>

#include <controller_utilities/math.h>

using namespace config;
using namespace controller;
using namespace controller_msgs;

CartesianController::CartesianController(model_interface::ModelBase& model, std::string name, bool publish_data)
    : ControllerBase(name, CARTESIAN ),
      adaptivecontroller(model, "cartesian_adaptive", CARTESIAN),
      model(model),
      is_publish_data(publish_data),
      Kd(Matrix6::Identity()),
      Jef(Matrix6::Zero()),
      Jefp(Matrix6::Zero()),
      Tef_0(Matrix4::Zero()),
      I(Matrix6::Identity()),
      lamda_sq(0.05),
      is_cmd_updated(false),
      mode(BREAK)
{
}

CartesianController::~CartesianController()
{
}

/* solve dampled least squares problem */
void CartesianController::set_lamda_sq( config::scalar_t lamda_sq )
{
    this->lamda_sq = lamda_sq;
}

/* gain Kp */
void CartesianController::set_Kp(const config::Matrix6& Kp)
{
    ref.set_Kp(Kp);
}
void CartesianController::set_Kp(const config::scalar_t& kp)
{
    ref.set_Kp(kp);
}
config::Matrix6 CartesianController::get_Kp() const
{
    return ref.get_Kp();
}

/* gain Kd */
void CartesianController::set_Ki(const config::Matrix6& Ki)
{
    ref.set_Ki(Ki);
}
void CartesianController::set_Ki(const config::scalar_t& ki)
{
    ref.set_Ki(ki);
}
config::Matrix6 CartesianController::get_Ki() const
{
    return ref.get_Ki();
}

/* load gain values from ros_parameters service */
bool CartesianController::load_gains(const std::string& ns)
{
    bool ret = true;

    // load adaptive controller gains
    if( !adaptivecontroller.load_gains(ns) ) {
        ret = false;
    }
    Kd = adaptivecontroller.get_Kd();

    // load gains
    std::vector<double> vec_p, vec_i, vec_cd;
    Matrix6 Kp, Ki;
    std::string err_str;
    if( load_param::load_param_vec(ns, "gains_p", vec_p, config::NJOINT, err_str) &&
        load_param::load_param_vec(ns, "gains_i", vec_i, config::NJOINT, err_str)) {
        Kp.setZero(); Ki.setZero();
        for(int i = 0; i < config::NJOINT; ++i) {
            Kp(i, i) = scalar_t(vec_p[i]);
            Ki(i, i) = scalar_t(vec_i[i]);
        }
        ref.set_Kp(Kp); ref.set_Ki(Ki);
    } else {
        ret = false; ROS_ERROR_STREAM(ControllerBase::get_name() << " error setting gains: " << err_str);
    }

    return ret;
}

/* init controller parameters */
bool CartesianController::init(ros::NodeHandle& nh)
{
    bool ret = true;

    // init low level adaptive controller
    ret = adaptivecontroller.init_request(nh);

    // load gain values from rosparameter service ( print them )
    if( load_gains(ControllerBase::get_name()) ) {
        std::cout << ControllerBase::get_name() << " Kd=" << std::endl << Kd << std::endl;
        std::cout << ControllerBase::get_name() << " Kp=" << std::endl << ref.get_Kp() << std::endl;
        std::cout << ControllerBase::get_name() << " Ki=" << std::endl << ref.get_Ki() << std::endl;
    } else ret = false;

    // advertise services
    set_command_srv = nh.advertiseService(ControllerBase::get_name() + "/set_command", &CartesianController::set_command_handler, this);
    set_mode_srv = nh.advertiseService(ControllerBase::get_name() + "/set_mode", &CartesianController::set_mode_handler, this);
    reload_gains_srv = nh.advertiseService(ControllerBase::get_name() + "/reload_gains", &CartesianController::relaod_gains_handler, this);
    info_srv = nh.advertiseService(ControllerBase::get_name() + "/info", &CartesianController::info_handler, this);

    // publish data if is_publishdata is set
    if( is_publish_data ) {
        controllerdata_pub = nh.advertise<controller_msgs::controllerdata>(ControllerBase::get_name() + "/controllerdata", 1);
    }
    return ret;
}

/* called before first time to update */
void CartesianController::start(const ros::Time& time)
{
    state_des.setZero();

    ref.reset_integral();
    adaptivecontroller.set_Kd(Kd);
    adaptivecontroller.start_request(time);

    is_cmd_updated = false;
    set_cmd_time = time;
}

 /* called after last call to update */
void CartesianController::stop(const ros::Time& time)
{
    adaptivecontroller.stop_request(time);
}

/* periodically called */
VectorDOF CartesianController::update(robot_msgs::JointState& state, const ros::Time& time, const ros::Duration& dt)
{
    Vector3 Xpos, Xdpos;
    Quaternion qd;

    VectorDOF tau;
    Vector6 Xp, Xdp, Xdpp, Xrp, Xrpp, DeltaX, DeltaXp;
    Matrix6 Jef_inv;

    // switch to break if no new update is recived
    if( (time - set_cmd_time).toSec() > 0.5)
        is_cmd_updated = false;
    // remain in break until first command arrived
    Mode mode_ = mode;
    if(!is_cmd_updated) {
        mode_ = BREAK;
    }

    // desired wrt base frame
    Xdpos = state_des.D.head(3);
    qd.coeffs() = state_des.D.tail(4);
    Matrix3 Rd = qd.toRotationMatrix();
    Xdp = state_des.Dp;
    Xdpp = state_des.Dpp;

    // current ef position and orientation wrt base frame
    model.T_ef(Tef_0, state.Q);
    Xpos = Tef_0.topRightCorner(3, 1);
    Matrix3 R = Tef_0.topLeftCorner(3, 3);

    // ef velocity skew wrt base frame
    model.J_ef(Jef, state.Q);
    Xp = Jef * state.Qp;

    // error signal, based on Rd*R^T
    DeltaX << Xpos - Xdpos, -scalar_t(0.5)*( R.col(0).cross(Rd.col(0)) + R.col(1).cross(Rd.col(1)) + R.col(2).cross(Rd.col(2)) );

    // time derivative of error signal
    DeltaXp = Xp - state_des.Dp;

    // compute refernce values: Xd -> Xr
    Xrp = ref.refp(Xdp, DeltaX, dt, mode_);
    Xrpp = ref.refpp(Xdpp, DeltaX, DeltaXp, dt, mode_);

    // transform back to joint space ( slove least squares damped optimization problem ): Xr -> Qr
    Jef_inv = (Jef.transpose()*Jef + lamda_sq * I).inverse()*Jef.transpose();
    model.Jp_ef(Jefp, state.Q, state.Qp);

    state.Qrp = Jef_inv * Xrp;
    state.Qrpp = Jef_inv * ( Xrpp - Jefp * state.Qrp );

    // compute torque: Qrp -> tau
    tau = adaptivecontroller.update_request( state, time, dt );

    // publish internal controllerdata
    if( is_publish_data ) {
        Quaternion q(R);
        Vector7 X;
        X << Xpos, q.coeffs();

        controller_msgs::controllerdata msg;
        msg.header.stamp = time;
        msg.D = std::vector<scalar_t>(state_des.D.data(), state_des.D.data() + state_des.D.size());
        msg.Dp = std::vector<scalar_t>(Xdp.data(), Xdp.data() + Xdp.size());
        msg.X = std::vector<scalar_t>(X.data(), X.data() + X.size());
        msg.Xp = std::vector<scalar_t>(Xp.data(), Xp.data() + Xp.size());
        msg.E = std::vector<scalar_t>(DeltaX.data(), DeltaX.data() + DeltaX.size());
        msg.Ep = std::vector<scalar_t>(DeltaXp.data(), DeltaXp.data() + DeltaXp.size());
        msg.Ei = std::vector<scalar_t>(ref.get_integral().data(), ref.get_integral().data() + ref.get_integral().size());
        msg.Out = std::vector<scalar_t>(tau.data(), tau.data() + tau.size());
        controllerdata_pub.publish(msg);
    }

    return tau;
}

/* service handler */
bool CartesianController::set_command_handler(robot_msgs::writecontroldataRequest& req, robot_msgs::writecontroldataResponse& res)
{
    // look srv
    std::lock_guard<std::mutex> lock(srv_mutex);

    // cpy desired state
    state_des.D = config::Vector7(req.data.D.data());
    state_des.Dp = config::Vector6(req.data.Dp.data());
    state_des.Dpp = config::Vector6(req.data.Dpp.data());
    is_cmd_updated = true;
    set_cmd_time = ros::Time::now();

    return true;
}

bool CartesianController::set_mode_handler(controller_msgs::setmodeRequest& req, controller_msgs::setmodeResponse& res)
{
    // look srv
    std::lock_guard<std::mutex> lock(srv_mutex);

    Mode mode_ = Mode(req.mode);
    if( mode != mode_ ) {
        mode = mode_;
        ref.reset_integral();
        is_cmd_updated = false;
    }
    return true;
}

bool CartesianController::relaod_gains_handler(controller_msgs::reloadgainsRequest& req, controller_msgs::reloadgainsResponse& res)
{
    // look srv
    std::lock_guard<std::mutex> lock(srv_mutex);

    // reset integral part, load gains
    ref.reset_integral();
    return load_gains(ControllerBase::get_name());
}

bool CartesianController::info_handler(controller_msgs::infoRequest& req, controller_msgs::infoResponse& res)
{
    // look srv
    std::lock_guard<std::mutex> lock(srv_mutex);

    res.name = ControllerBase::get_name();
    res.running = uint8_t(ControllerBase::is_running());
    res.mode = uint8_t(mode);
    res.space = uint8_t(ControllerBase::get_space());

    Vector6 Kd_vec = Kd.diagonal();
    Vector6 Kp_vec = ref.get_Kp().diagonal();
    Vector6 Ki_vec = ref.get_Ki().diagonal();
    res.Kd = std::vector<scalar_t>(Kd_vec.data(), Kd_vec.data() + Kd_vec.size());
    res.Kp = std::vector<scalar_t>(Kp_vec.data(), Kp_vec.data() + Kp_vec.size());
    res.Ki = std::vector<scalar_t>(Ki_vec.data(), Ki_vec.data() + Ki_vec.size());
    return true;
}
