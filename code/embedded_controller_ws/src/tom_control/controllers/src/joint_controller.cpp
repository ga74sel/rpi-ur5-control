#include "controllers/joint_controller.h"

#include <controller_utilities/load_param.h>

using namespace config;
using namespace controller;
using namespace controller_msgs;

JointController::JointController(model_interface::ModelBase &model, std::string name, bool publish_data)
    : ControllerBase(name, JOINT),
      adaptivecontroller(model, "joint_adaptive", JOINT),
      is_publish_data(publish_data),
      Kd(Matrix6::Identity()),
      is_cmd_updated(false),
      mode(BREAK)
{
}

JointController::~JointController()
{
}

/* gain Kp */
void JointController::set_Kp(const config::Matrix6& Kp)
{
    ref.set_Kp(Kp);
}
void JointController::set_Kp(const config::scalar_t& kp)
{
    ref.set_Kp(kp);
}
config::Matrix6 JointController::get_Kp() const
{
    return ref.get_Kp();
}

/* gain Kd */
void JointController::set_Ki(const config::Matrix6& Ki)
{
    ref.set_Ki(Ki);
}
void JointController::set_Ki(const config::scalar_t& ki)
{
    ref.set_Ki(ki);
}
config::Matrix6 JointController::get_Ki() const
{
    return ref.get_Ki();
}

/* load gain values from ros_parameters service */
bool JointController::load_gains(const std::string& ns)
{
    bool ret = true;

    // load adaptive controller gains
    if( !adaptivecontroller.load_gains(ns) ) {
        ret = false;
    }
    Kd = adaptivecontroller.get_Kd();

    // load gains
    std::vector<double> vec_p, vec_i;
    Matrix6 Kp, Ki;
    std::string err_str;
    if( load_param::load_param_vec(ns, "gains_p", vec_p, config::NJOINT, err_str) &&
        load_param::load_param_vec(ns, "gains_i", vec_i, config::NJOINT, err_str) ) {
        Kp.setZero(); Ki.setZero();
        for(int i = 0; i < config::NJOINT; ++i) {
            Kp(i, i) = scalar_t(vec_p[i]) / Kd(i, i); // normalize by d gain ( since: 0 = kd*spp - kd*kkp*sp - kd*ki)
            Ki(i, i) = scalar_t(vec_i[i]) / Kd(i, i); // normalize by d gain
        }
        ref.set_Kp(Kp); ref.set_Ki(Ki);
    } else {
        ret = false; ROS_ERROR_STREAM(ControllerBase::get_name() << " error setting gains: " << err_str);
    }

    return ret;
}

/* init controller parameters */
bool JointController::init(ros::NodeHandle& nh)
{
    bool ret = true;

    // init low level adaptive controller
    ret = adaptivecontroller.init_request(nh);

    // load gain values from rosparameter service ( print them )
    if( load_gains(ControllerBase::get_name()) ) {
        std::cout << ControllerBase::get_name() << " Kd=" << std::endl << Kd << std::endl;
        std::cout << ControllerBase::get_name() << " Kp=" << std::endl << ref.get_Kp() << std::endl;
        std::cout << ControllerBase::get_name() << " Ki=" << std::endl << ref.get_Ki() << std::endl;
    } else ret = false;

    // advertise service
    set_command_srv = nh.advertiseService(ControllerBase::get_name() + "/set_command", &JointController::set_command_handler, this);
    set_mode_srv = nh.advertiseService(ControllerBase::get_name() + "/set_mode", &JointController::set_mode_handler, this);
    reload_gains_srv = nh.advertiseService(ControllerBase::get_name() + "/reload_gains", &JointController::relaod_gains_handler, this);
    info_srv = nh.advertiseService(ControllerBase::get_name() + "/info", &JointController::info_handler, this);

    // publish data if is_publishdata is set
    if( is_publish_data ) {
        controllerdata_pub = nh.advertise<controller_msgs::controllerdata>(ControllerBase::get_name() + "/controllerdata", 1);
    }

    return ret;
}

/* called before first time to update */
void JointController::start(const ros::Time& time)
{
    is_cmd_updated = false;
    state_des.setZero();
    ref.reset_integral();
    adaptivecontroller.set_Kd(Kd);
    adaptivecontroller.start_request(time);
    set_cmd_time = time;
}

 /* called after last call to update */
void JointController::stop(const ros::Time& time)
{
    adaptivecontroller.stop_request(time);
}

/* periodically called */
config::VectorDOF JointController::update(robot_msgs::JointState& state, const ros::Time& time, const ros::Duration& dt)
{
    // errors
    VectorDOF tau;
    Vector6 DeltaQ = state.Q - state_des.D.head(6);
    Vector6 DeltaQp = state.Qp - state_des.Dp;

    // switch to break if no new update is recieved
    if( (time - set_cmd_time).toSec() > 0.5)
        is_cmd_updated = false;
    // remain in break until first command arrived
    Mode mode_ = mode;
    if(!is_cmd_updated)
        mode_ = BREAK;

    // compute refernce: Qd -> Qr
    state.Qrp = ref.refp(state_des.Dp, DeltaQ, dt, mode_);
    state.Qrpp = ref.refpp(state_des.Dpp, DeltaQ, DeltaQp, dt, mode_);

    // compute torque: Qr -> tau
    tau = adaptivecontroller.update_request( state, time, dt );

    // publish internal controllerdata
    if( is_publish_data ) {
        controller_msgs::controllerdata msg;
        msg.header.stamp = time;
        msg.D = std::vector<scalar_t>(state_des.D.data(), state_des.D.data() + state_des.D.size());
        msg.Dp = std::vector<scalar_t>(state_des.Dp.data(), state_des.Dp.data() + state_des.Dp.size()); 
        msg.X = std::vector<scalar_t>(state.Q.data(), state.Q.data() + state.Q.size());
        msg.Xp = std::vector<scalar_t>(state.Qp.data(), state.Qp.data() + state.Qp.size());
        msg.E = std::vector<scalar_t>(DeltaQ.data(), DeltaQ.data() + DeltaQ.size());
        msg.Ep = std::vector<scalar_t>(DeltaQp.data(), DeltaQp.data() + DeltaQp.size());
        msg.Ei = std::vector<scalar_t>(ref.get_integral().data(), ref.get_integral().data() + ref.get_integral().size());
        msg.Out = std::vector<scalar_t>(state.tau.data(), state.tau.data() + state.tau.size());
        controllerdata_pub.publish(msg);
    }

    return tau;
}

/* service handler */
bool JointController::set_command_handler(robot_msgs::writecontroldataRequest& req, robot_msgs::writecontroldataResponse& res)
{
    // look srv
    std::lock_guard<std::mutex> lock(srv_mutex);

    // cpy desired state
    state_des.D = config::Vector7(req.data.D.data());
    state_des.Dp = config::Vector6(req.data.Dp.data());
    state_des.Dpp = config::Vector6(req.data.Dpp.data());

    is_cmd_updated = true;
    set_cmd_time = ros::Time::now();
}

bool JointController::set_mode_handler(controller_msgs::setmodeRequest& req, controller_msgs::setmodeResponse& res)
{
    // look srv
    std::lock_guard<std::mutex> lock(srv_mutex);

    Mode mode_ = Mode(req.mode);
    if( mode != mode_ ) {
        mode = mode_;
        ref.reset_integral();
        is_cmd_updated = false;
        state_des.setZero();
    }

    return true;
}

bool JointController::relaod_gains_handler(controller_msgs::reloadgainsRequest& req, controller_msgs::reloadgainsResponse& res)
{
    // look srv
    std::lock_guard<std::mutex> lock(srv_mutex);

    ref.reset_integral();
    return load_gains(ControllerBase::get_name());
}

bool JointController::info_handler(controller_msgs::infoRequest& req, controller_msgs::infoResponse& res)
{
    // look srv
    std::lock_guard<std::mutex> lock(srv_mutex);

    res.name = ControllerBase::get_name();
    res.running = uint8_t(ControllerBase::is_running());
    res.mode = uint8_t(mode);
    res.space = uint8_t(ControllerBase::get_space());

    Vector6 Kd_vec = Kd.diagonal();
    Vector6 Kp_vec = ref.get_Kp().diagonal();
    Vector6 Ki_vec = ref.get_Ki().diagonal();
    res.Kd = std::vector<scalar_t>(Kd_vec.data(), Kd_vec.data() + Kd_vec.size());
    res.Kp = std::vector<scalar_t>(Kp_vec.data(), Kp_vec.data() + Kp_vec.size());
    res.Ki = std::vector<scalar_t>(Ki_vec.data(), Ki_vec.data() + Ki_vec.size());
    return true;
}
