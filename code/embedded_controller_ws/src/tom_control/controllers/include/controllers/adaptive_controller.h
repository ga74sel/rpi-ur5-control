/**
 * @file adaptive_controller.h
 *
 * @brief compute adaptive parameter updates
 *
 * @ingroup controllers
 *
 */

#ifndef ADAPTIVE_CONTROLLER_H_
#define ADAPTIVE_CONTROLLER_H_

#include <controller_interface/controller_base.h>

#include <robot_msgs/joint_state.h>

#include <controller_msgs/reloadgains.h>

#include <model_interface/model_base.h>

namespace controller
{

/**
 * Implementation of pasivity based adaptive update law
 *
 * gets reference signal from higher level controller
 * and computes regressorbased torque values
 *
 */
class AdaptiveController : public controller_interface::ControllerBase
{
public:
    /** 
    * @brief constructor
    *
    * @param robot dynamic and kinematic model
    * @param controller name
    * @param configuration space
    */
    AdaptiveController(model_interface::ModelBase& model, std::string name, Space space = JOINT);
    virtual ~AdaptiveController();

    /** 
    * @brief set step size for gradient
    *
    * @param step size
    */
    void set_gamma_inv(const float& gamma_inv);

    /** 
    * @brief set derivative gain
    *
    * @param derivative gain
    */
    void set_Kd(const config::Matrix6& Kd);
    void set_Kd(const float& kd);

    /** 
    * @brief return derivative gain
    *
    * @return derivative gain
    */
    config::Matrix6 get_Kd() const;

    /** 
    * @brief load gains from ros namespace
    *
    * @param namespace
    */
    bool load_gains(const std::string& ns);

private:
    /** 
    * @brief init interal parameter
    *
    * @param ros nh for namespace
    */
    bool init(ros::NodeHandle& nh);

    /** 
    * @brief performs update step of the controller, called periodically
    *
    * @param current jointstate
    * @param current time
    * @param current deltatime since last call
    * @return computed torque value
    */
    config::VectorDOF update(robot_msgs::JointState& state, const ros::Time& time, const ros::Duration& dt);

private:
    model_interface::ModelBase& model;      // dynamic, kinematic model

    config::scalar_t integ_thresh;          // threshold for integration
    config::scalar_t gamma_inv;             // adaptive gain
    config::Matrix6 Kd;                     // velo gain

    config::MatrixX Yr;                     // regressor
    config::VectorX th;                     // unknown parameter
};

/*
 * Template implementation
 */



}

#endif
