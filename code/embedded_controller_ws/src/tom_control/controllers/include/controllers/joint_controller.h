/**
 * @file joint_controller.h
 *
 * @brief joint space controller
 *
 * @ingroup controllers
 *
 */

#ifndef JOINT_CONTROLLER_H_
#define JOINT_CONTROLLER_H_

#include <controller_interface/controller_base.h>

#include <controller_msgs/reloadgains.h>
#include <controller_msgs/controllerdata.h>
#include <controller_msgs/info.h>
#include <controller_msgs/setmode.h>
#include <controller_msgs/mode.h>

#include <robot_msgs/writecontroldata.h>

#include <controller_utilities/reference.h>

#include <model_interface/model_base.h>

#include <mutex>

#include "controllers/adaptive_controller.h"

namespace controller
{

/**
 * Implementation of joint space controller
 *
 * compute torque value based on desired input commands (set via set_command_srv)
 * internally uses AdaptiveController for parameter updates
 *
 */
class JointController : public controller_interface::ControllerBase
{
public:
    /** 
    * @brief constructor
    *
    * @param robot dynamic and kinematic model
    * @param controller name
    * @param flag to turn on interal data publishing
    */
    JointController(model_interface::ModelBase& model, std::string name, bool publish_data = false);
    virtual ~JointController();

    /** 
    * @brief set prop gain
    *
    * @param prop gain
    */
    void set_Kp(const config::Matrix6& Kp);
    void set_Kp(const config::scalar_t& kp);

    /** 
    * @brief return prop gain
    *
    * @return prop gain
    */
    config::Matrix6 get_Kp() const;

    /** 
    * @brief set integ gain
    *
    * @param integ gain
    */
    void set_Ki(const config::Matrix6& Ki);
    void set_Ki(const config::scalar_t& ki);

    /** 
    * @brief return integ gain
    *
    * @return integ gain
    */
    config::Matrix6 get_Ki() const;

    /** 
    * @brief load gains from ros namespace
    *
    * @param namespace
    */
    bool load_gains(const std::string& ns);

private:
    /** 
    * @brief init interal parameter
    *
    * @param ros nh for namespace
    */
    bool init(ros::NodeHandle& nh);

    /** 
    * @brief start the controller, called befor update
    *
    * @param starting time
    */
    void start(const ros::Time& time);

    /** 
    * @brief performs update step of the controller, called periodically
    *
    * @param current jointstate
    * @param current time
    * @param current deltatime since last call
    * @return computed torque value
    */
    config::VectorDOF update(robot_msgs::JointState& state, const ros::Time& time, const ros::Duration& dt);

    /** 
    * @brief stop the controller, called befor stopping
    *
    * @param stopping time
    */
    void stop(const ros::Time& time);

    /// @brief service handle
    bool set_command_handler(robot_msgs::writecontroldataRequest& req, robot_msgs::writecontroldataResponse& res);
    /// @brief service handle
    bool set_mode_handler(controller_msgs::setmodeRequest& req, controller_msgs::setmodeResponse& res);
    /// @brief service handle
    bool relaod_gains_handler(controller_msgs::reloadgainsRequest& req, controller_msgs::reloadgainsResponse& res);
    /// @brief service handle
    bool info_handler(controller_msgs::infoRequest& req, controller_msgs::infoResponse& res);

private:
    ros::ServiceServer set_command_srv;         // set a new desired state D
    ros::ServiceServer set_mode_srv;            // change between BREAK, PD, PID
    ros::ServiceServer reload_gains_srv;        // change gains during runtime server
    ros::ServiceServer info_srv;                // get information about this controller
    ros::Publisher controllerdata_pub;          // publish current controller data ( used for debugging )       

    AdaptiveController adaptivecontroller;      // reference -> tau

    robot_msgs::JointState state_des;           // desired jointstate

    config::Matrix6 Kd;                         // derivative gain
    reference::Reference<config::Vector6> ref;  // reference cmd

    controller_msgs::Mode mode;                 // BREAK, PD, PID

    bool is_publish_data;                       // publish internal data
    bool is_cmd_updated;                        // true if desired state is recieved

    ros::Time set_cmd_time;                     // time step at which set_mode was recived

    std::mutex srv_mutex;
};

}

#endif
