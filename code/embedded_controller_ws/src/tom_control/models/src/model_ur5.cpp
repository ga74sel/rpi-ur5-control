#include <models/model_ur5.h>

#include <controller_utilities/load_param.h>

using namespace models;

UR5Model::UR5Model(const std::string& name)
    : ModelBase(name)
{
}

UR5Model::~UR5Model()
{
}

/* inertia matrix */
void UR5Model::M(config::MatrixDOF& M, const config::VectorDOF& q ) const
{
    matrix_M(M, q);
}

/* coriolis centripetal matrix */ 
void UR5Model::C(config::MatrixDOF& C, const config::VectorDOF& q, const config::VectorDOF& qp ) const
{
    matrix_C(C, q, qp);
}

/* gravitational vector */
void UR5Model::G(config::VectorDOF& G, const config::VectorDOF& q ) const
{
    matrix_G(G, q);
}

/* regressor matrix */
config::MatrixX UR5Model::init_Yr() const
{
    config::MatrixX Yr(6, 45);
    Yr.setZero();
    return Yr;
}

void UR5Model::Yr(config::MatrixX& Yr, const config::VectorDOF& q, const config::VectorDOF& qp, const config::VectorDOF& qrp, const config::VectorDOF& qrpp ) const
{
    matrix_Y(Yr, q, qp, qrp, qrpp);
}

config::MatrixX UR5Model::init_Yr_j(int j) const
{
    // TODO
    config::MatrixX Yr_j(6, 45);
    Yr_j.setZero();
    return Yr_j;
}

void UR5Model::Yr_j(config::MatrixX& Yr_j, const config::VectorDOF& q, const config::VectorDOF& qp, const config::VectorDOF& qrp, const config::VectorDOF& qrpp, int j ) const
{
    // TODO
    matrix_Y(Yr_j, q, qp, qrp, qrpp);
}

/* parameter vector */
config::VectorX UR5Model::init_th() const
{
    config::VectorX th(45);
    matrix_th(th);
    return th;
}

/* base wrt world */
config::Matrix4 UR5Model::T0_W() const
{
    return T0;
}

/* gravity vector wrt world */
config::Vector3 UR5Model::g_W() const
{
    return gW;
}

/* gravity vector wrt base */
config::Vector3 UR5Model::g_0() const
{
    return g0;
}

/* endeffector transformation matrix */
void UR5Model::T_ef(config::Matrix4& T_ef, const config::VectorDOF& q ) const
{
    matrix_T6_0(T_ef, q);
}

/* joint transformation */
void UR5Model::T_j(config::Matrix4& T_j, const config::VectorDOF& q, int j) const
{
    switch(j) {
        case 0:
            matrix_T1_0(T_j, q);
            break;
        case 1:
            matrix_T2_0(T_j, q);
            break;
        case 2:
            matrix_T3_0(T_j, q);
            break;
        case 3:
            matrix_T4_0(T_j, q);
            break;
        case 4:
            matrix_T5_0(T_j, q);
            break;
        case 5:
            matrix_T6_0(T_j, q);
            break;
    }
}

/* center of mass transformaiton */
void UR5Model::Tcm_j(config::Matrix4& Tcm_j, const config::VectorDOF& q, int j) const
{
    switch(j) {
        case 0:
            matrix_Tcm1_0(Tcm_j, q);
            break;
        case 1:
            matrix_Tcm2_0(Tcm_j, q);
            break;
        case 2:
            matrix_Tcm3_0(Tcm_j, q);
            break;
        case 3:
            matrix_Tcm4_0(Tcm_j, q);
            break;
        case 4:
            matrix_Tcm5_0(Tcm_j, q);
            break;
        case 5:
            matrix_Tcm6_0(Tcm_j, q);
            break;
    }
}

/* endeffector jacobian matrix */
void UR5Model::J_ef(config::MatrixDOF& J_ef, const config::VectorDOF& q ) const
{
    matrix_J6_0(J_ef, q);
}

/* jacobian matrix at j*/
void UR5Model::J_j(config::MatrixDOF& J_j, const config::VectorDOF& q, int j) const
{
    switch(j) {
        case 0:
            matrix_J1_0(J_j, q);
            break;
        case 1:
            matrix_J2_0(J_j, q);
            break;
        case 2:
            matrix_J3_0(J_j, q);
            break;
        case 3:
            matrix_J4_0(J_j, q);
            break;
        case 4:
            matrix_J5_0(J_j, q);
            break;
        case 5:
            matrix_J6_0(J_j, q);
            break;
    }
}

/* center of mass jacobian matrix at j*/
void UR5Model::Jcm_j(config::MatrixDOF& Jcm_j, const config::VectorDOF& q, int j) const
{
    switch(j) {
        case 0:
            matrix_Jcm1_0(Jcm_j, q);
            break;
        case 1:
            matrix_Jcm2_0(Jcm_j, q);
            break;
        case 2:
            matrix_Jcm3_0(Jcm_j, q);
            break;
        case 3:
            matrix_Jcm4_0(Jcm_j, q);
            break;
        case 4:
            matrix_Jcm5_0(Jcm_j, q);
            break;
        case 5:
            matrix_Jcm6_0(Jcm_j, q);
            break;
    }
}

/* jacobian derivatives */
void UR5Model::Jp_ef(config::MatrixDOF& Jp_ef, const config::VectorDOF& q, const config::VectorDOF& qp ) const
{
    matrix_J6_0p(Jp_ef, q, qp);
}

/* jacobian derivatives */
void UR5Model::Jp_j(config::MatrixDOF& Jp_j, const config::VectorDOF& q, const config::VectorDOF& qp, int j) const
{
    switch(j) {
        case 0:
            matrix_J1_0p(Jp_j, q, qp);
            break;
        case 1:
            matrix_J2_0p(Jp_j, q, qp);
            break;
        case 2:
            matrix_J3_0p(Jp_j, q, qp);
            break;
        case 3:
            matrix_J4_0p(Jp_j, q, qp);
            break;
        case 4:
            matrix_J5_0p(Jp_j, q, qp);
            break;
        case 5:
            matrix_J6_0p(Jp_j, q, qp);
            break;
    }
}

/* jacobian derivatives */
void UR5Model::Jpcm_j(config::MatrixDOF& Jp_j, const config::VectorDOF& q, const config::VectorDOF& qp, int j) const
{
    switch(j) {
        case 0:
            matrix_Jcm1_0p(Jp_j, q, qp);
            break;
        case 1:
            matrix_Jcm2_0p(Jp_j, q, qp);
            break;
        case 2:
            matrix_Jcm3_0p(Jp_j, q, qp);
            break;
        case 3:
            matrix_Jcm4_0p(Jp_j, q, qp);
            break;
        case 4:
            matrix_Jcm5_0p(Jp_j, q, qp);
            break;
        case 5:
            matrix_Jcm6_0p(Jp_j, q, qp);
            break;
    }    
}

bool UR5Model::init_request(const std::string& ns)
{
    bool ret = true;
    std::string err_str = "";

    // length
    if( !load_param::load_param(ns, "L1", L1, err_str) ) { ret = false; }
    if( !load_param::load_param(ns, "L2", L2, err_str) ) { ret = false; }
    if( !load_param::load_param(ns, "L3", L3, err_str) ) { ret = false; }
    if( !load_param::load_param(ns, "L4", L4, err_str) ) { ret = false; }
    if( !load_param::load_param(ns, "L5", L5, err_str) ) { ret = false; }
    if( !load_param::load_param(ns, "L6", L6, err_str) ) { ret = false; }
    if( !load_param::load_param(ns, "L7", L7, err_str) ) { ret = false; }
    if( !load_param::load_param(ns, "L8", L8, err_str) ) { ret = false; }
    if( !load_param::load_param(ns, "L9", L9, err_str) ) { ret = false; }
    if( !load_param::load_param(ns, "L10", L10, err_str) ) { ret = false; }
    if( !load_param::load_param(ns, "L11", L11, err_str) ) { ret = false; }
    if( !load_param::load_param(ns, "L12", L12, err_str) ) { ret = false; }

    // mass
    if( !load_param::load_param(ns, "m1", m1, err_str) ) { ret = false; }
    if( !load_param::load_param(ns, "m2", m2, err_str) ) { ret = false; }
    if( !load_param::load_param(ns, "m3", m3, err_str) ) { ret = false; }
    if( !load_param::load_param(ns, "m4", m4, err_str) ) { ret = false; }
    if( !load_param::load_param(ns, "m5", m5, err_str) ) { ret = false; }
    if( !load_param::load_param(ns, "m6", m6, err_str) ) { ret = false; }

    // inertia
    if( !load_param::load_param(ns, "I111", I111, err_str) ) { ret = false; }
    if( !load_param::load_param(ns, "I112", I112, err_str) ) { ret = false; }
    if( !load_param::load_param(ns, "I113", I113, err_str) ) { ret = false; }
    if( !load_param::load_param(ns, "I122", I122, err_str) ) { ret = false; }
    if( !load_param::load_param(ns, "I123", I123, err_str) ) { ret = false; }
    if( !load_param::load_param(ns, "I133", I133, err_str) ) { ret = false; }

    if( !load_param::load_param(ns, "I211", I211, err_str) ) { ret = false; }
    if( !load_param::load_param(ns, "I212", I212, err_str) ) { ret = false; }
    if( !load_param::load_param(ns, "I213", I213, err_str) ) { ret = false; }
    if( !load_param::load_param(ns, "I222", I222, err_str) ) { ret = false; }
    if( !load_param::load_param(ns, "I223", I223, err_str) ) { ret = false; }
    if( !load_param::load_param(ns, "I233", I233, err_str) ) { ret = false; }

    if( !load_param::load_param(ns, "I311", I311, err_str) ) { ret = false; }
    if( !load_param::load_param(ns, "I312", I312, err_str) ) { ret = false; }
    if( !load_param::load_param(ns, "I313", I313, err_str) ) { ret = false; }
    if( !load_param::load_param(ns, "I322", I322, err_str) ) { ret = false; }
    if( !load_param::load_param(ns, "I323", I323, err_str) ) { ret = false; }
    if( !load_param::load_param(ns, "I333", I333, err_str) ) { ret = false; }

    if( !load_param::load_param(ns, "I411", I411, err_str) ) { ret = false; }
    if( !load_param::load_param(ns, "I412", I412, err_str) ) { ret = false; }
    if( !load_param::load_param(ns, "I413", I413, err_str) ) { ret = false; }
    if( !load_param::load_param(ns, "I422", I422, err_str) ) { ret = false; }
    if( !load_param::load_param(ns, "I423", I423, err_str) ) { ret = false; }
    if( !load_param::load_param(ns, "I433", I433, err_str) ) { ret = false; }

    if( !load_param::load_param(ns, "I511", I511, err_str) ) { ret = false; }
    if( !load_param::load_param(ns, "I512", I512, err_str) ) { ret = false; }
    if( !load_param::load_param(ns, "I513", I513, err_str) ) { ret = false; }
    if( !load_param::load_param(ns, "I522", I522, err_str) ) { ret = false; }
    if( !load_param::load_param(ns, "I523", I523, err_str) ) { ret = false; }
    if( !load_param::load_param(ns, "I533", I533, err_str) ) { ret = false; }

    if( !load_param::load_param(ns, "I611", I611, err_str) ) { ret = false; }
    if( !load_param::load_param(ns, "I612", I612, err_str) ) { ret = false; }
    if( !load_param::load_param(ns, "I613", I613, err_str) ) { ret = false; }
    if( !load_param::load_param(ns, "I622", I622, err_str) ) { ret = false; }
    if( !load_param::load_param(ns, "I623", I623, err_str) ) { ret = false; }
    if( !load_param::load_param(ns, "I633", I633, err_str) ) { ret = false; }

    // gravity wrt world
    if( !load_param::load_param(ns, "gx", gx, err_str) ) { ret = false; }
    if( !load_param::load_param(ns, "gy", gy, err_str) ) { ret = false; }
    if( !load_param::load_param(ns, "gz", gz, err_str) ) { ret = false; }
    gW << gx, gy, gz;

    // load base to world transformation
    config::Matrix4 TW_0;
    if( !load_param::load_transf_quad(ns, "Q0_W", "t0_W", T0, err_str) ) { ret = false; }
    TW_0 = T0.inverse();

    // gravity wrt base
    g0 = TW_0.topLeftCorner(3, 3) * gW;

    if(!ret) {
        ROS_ERROR_STREAM("UR5Model: " << "error loading ns=" << ns);
    }
    return ret;
}
