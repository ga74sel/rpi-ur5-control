/**
 * @file model_ur5.h
 *
 * @brief model of ur5 robot
 *
 * @ingroup models
 *
 */

#ifndef MODEL_UR5_H_
#define MODEL_UR5_H_

#include <model_interface/model_base.h>

namespace models
{

/**
* Defines the kinematic and dynamic model of UR5 robot
*
* used by trajectory generator, controllers, simulator
*/
class UR5Model : public model_interface::ModelBase
{
public:
    /** 
    * @brief constructor
    *
    * @param name of robot
    */
    UR5Model(const std::string& name = "UR5Model");
    virtual ~UR5Model();

    /** 
    * @brief inertia matrix computation
    *
    * @param inertia Matrix
    * @param joint position
    */
    void M(config::MatrixDOF& M, const config::VectorDOF& q ) const;

    /** 
    * @brief coriolis centripetal matrix computation
    *
    * @param coriolis centripetal matrix
    * @param joint position
    * @param joint velocity
    */ 
    void C(config::MatrixDOF& C, const config::VectorDOF& q, const config::VectorDOF& qp ) const;

    /** 
    * @brief gravitational vector computation
    *
    * @param gravitational vector
    * @param joint position
    */
    void G(config::VectorDOF& G, const config::VectorDOF& q ) const;

    /** 
    * @brief returns regressor matrix of correct size
    */
    config::MatrixX init_Yr() const;

    /** 
    * @brief regressor matrix computation
    *
    * @param regressor matrix
    * @param joint position
    * @param joint velocity
    * @param reference joint velocity
    * @param reference joint acceleration
    */
    void Yr(config::MatrixX& Yr, const config::VectorDOF& q, const config::VectorDOF& qp, const config::VectorDOF& qrp, const config::VectorDOF& qrpp ) const;

    /** 
    * @brief returns regressor matrix of correct size for joint j
    * @param joint j
    */
    config::MatrixX init_Yr_j(int j) const;

    /** 
    * @brief regressor matrix computation for joint j
    *
    * @param regressor matrix
    * @param joint position
    * @param joint velocity
    * @param reference joint velocity
    * @param reference joint acceleration
    * @param joint j
    */
    void Yr_j(config::MatrixX& Yr_j, const config::VectorDOF& q, const config::VectorDOF& qp, const config::VectorDOF& qrp, const config::VectorDOF& qrpp, int j ) const;

    /** 
    * @brief parameter vector
    */
    config::VectorX init_th() const;

    /** 
    * @brief gravity vector wrt world
    */
    config::Matrix4 T0_W() const;

    /** 
    * @brief gravity vector wrt word
    */
    config::Vector3 g_W() const;
    
    /** 
    * @brief gravity vector wrt base
    */
    config::Vector3 g_0() const;

    /** 
    * @brief endeffector transformation matrix
    */
    void T_ef(config::Matrix4& T_ef, const config::VectorDOF& q ) const;

    /** 
    * @brief joint transformation j
    */
    void T_j(config::Matrix4& T_j, const config::VectorDOF& q, int j) const;
    
    /** 
    * @brief center of mass transformaiton
    */
    void Tcm_j(config::Matrix4& Tcm, const config::VectorDOF& q, int j) const;

    /** 
    * @brief endeffector jacobian matrix
    */
    void J_ef(config::MatrixDOF& J_ef, const config::VectorDOF& q ) const;

    /** 
    * @brief jacobian matrix at j
    */
    void J_j(config::MatrixDOF& J_j, const config::VectorDOF& q, int j) const;

    /** 
    * @brief center of mass jacobian matrix at j
    */
    void Jcm_j(config::MatrixDOF& Jcm_j, const config::VectorDOF& q, int j) const;

    /** 
    * @brief jacobian derivatives 
    */
    void Jp_ef(config::MatrixDOF& Jp_ef, const config::VectorDOF& q, const config::VectorDOF& qp ) const;

    /** 
    * @brief jacobian derivatives 
    */
    void Jp_j(config::MatrixDOF& Jp_j, const config::VectorDOF& q, const config::VectorDOF& qp, int j) const;

    /** 
    * @brief jacobian derivatives center of mass
    */
    void Jpcm_j(config::MatrixDOF& Jp_j, const config::VectorDOF& q, const config::VectorDOF& qp, int j) const;

private:
    bool init_request(const std::string& ns);

private:
    /* matlab generated functions */

    /* inertia matrix */
    void matrix_M(Eigen::Matrix<config::scalar_t, 6, 6>& M,
                  const Eigen::Matrix<config::scalar_t, 6, 1>& q ) const;

    /* coriolis centripetal matrix */ 
    void matrix_C(Eigen::Matrix<config::scalar_t, 6, 6>& C,
                  const Eigen::Matrix<config::scalar_t, 6, 1>& q,
                  const Eigen::Matrix<config::scalar_t, 6, 1>& qp ) const;

    /* gravitational vector */
    void matrix_G(Eigen::Matrix<config::scalar_t, 6, 1>& G,
                  const Eigen::Matrix<config::scalar_t, 6, 1>& q ) const;

    /* regressor matrix */
    void matrix_Y(Eigen::Matrix<config::scalar_t, 6, 45>& Y,
                     const Eigen::Matrix<config::scalar_t, 6, 1>& q,
                     const Eigen::Matrix<config::scalar_t, 6, 1>& qp,
                     const Eigen::Matrix<config::scalar_t, 6, 1>& qrp,
                     const Eigen::Matrix<config::scalar_t, 6, 1>& qrpp ) const;

    /* parameter vector theta */
    void matrix_th(Eigen::Matrix<config::scalar_t, 45, 1>& th ) const;

    /* transformations */
    void matrix_T1_0(Eigen::Matrix<config::scalar_t, 4, 4>& T1_0,
                     const Eigen::Matrix<config::scalar_t, 6, 1>& q ) const;
    void matrix_T2_0(Eigen::Matrix<config::scalar_t, 4, 4>& T2_0,
                     const Eigen::Matrix<config::scalar_t, 6, 1>& q ) const;
    void matrix_T3_0(Eigen::Matrix<config::scalar_t, 4, 4>& T3_0,
                     const Eigen::Matrix<config::scalar_t, 6, 1>& q ) const;
    void matrix_T4_0(Eigen::Matrix<config::scalar_t, 4, 4>& T4_0,
                     const Eigen::Matrix<config::scalar_t, 6, 1>& q ) const;
    void matrix_T5_0(Eigen::Matrix<config::scalar_t, 4, 4>& T5_0,
                     const Eigen::Matrix<config::scalar_t, 6, 1>& q ) const;
    void matrix_T6_0(Eigen::Matrix<config::scalar_t, 4, 4>& T6_0,
                     const Eigen::Matrix<config::scalar_t, 6, 1>& q ) const;

    void matrix_Tcm1_0(Eigen::Matrix<config::scalar_t, 4, 4>& Tcm1_0,
                       const Eigen::Matrix<config::scalar_t, 6, 1>& q ) const;
    void matrix_Tcm2_0(Eigen::Matrix<config::scalar_t, 4, 4>& Tcm2_0,
                       const Eigen::Matrix<config::scalar_t, 6, 1>& q ) const;
    void matrix_Tcm3_0(Eigen::Matrix<config::scalar_t, 4, 4>& Tcm3_0,
                       const Eigen::Matrix<config::scalar_t, 6, 1>& q ) const;
    void matrix_Tcm4_0(Eigen::Matrix<config::scalar_t, 4, 4>& Tcm4_0,
                       const Eigen::Matrix<config::scalar_t, 6, 1>& q ) const;
    void matrix_Tcm5_0(Eigen::Matrix<config::scalar_t, 4, 4>& Tcm5_0,
                       const Eigen::Matrix<config::scalar_t, 6, 1>& q ) const;
    void matrix_Tcm6_0(Eigen::Matrix<config::scalar_t, 4, 4>& Tcm6_0,
                       const Eigen::Matrix<config::scalar_t, 6, 1>& q ) const;

    /* jacobians */
    void matrix_J1_0(Eigen::Matrix<config::scalar_t, 6, 6>& J1_0,
                     const Eigen::Matrix<config::scalar_t, 6, 1>& q ) const;             
    void matrix_J2_0(Eigen::Matrix<config::scalar_t, 6, 6>& J2_0,
                     const Eigen::Matrix<config::scalar_t, 6, 1>& q ) const;  
    void matrix_J3_0(Eigen::Matrix<config::scalar_t, 6, 6>& J3_0,
                     const Eigen::Matrix<config::scalar_t, 6, 1>& q ) const;  
    void matrix_J4_0(Eigen::Matrix<config::scalar_t, 6, 6>& J4_0,
                     const Eigen::Matrix<config::scalar_t, 6, 1>& q ) const;  
    void matrix_J5_0(Eigen::Matrix<config::scalar_t, 6, 6>& J5_0,
                     const Eigen::Matrix<config::scalar_t, 6, 1>& q ) const;  
    void matrix_J6_0(Eigen::Matrix<config::scalar_t, 6, 6>& J6_0,
                     const Eigen::Matrix<config::scalar_t, 6, 1>& q ) const;

    void matrix_Jcm1_0(Eigen::Matrix<config::scalar_t, 6, 6>& Jcm1_0,
                       const Eigen::Matrix<config::scalar_t, 6, 1>& q ) const;
    void matrix_Jcm2_0(Eigen::Matrix<config::scalar_t, 6, 6>& Jcm2_0,
                       const Eigen::Matrix<config::scalar_t, 6, 1>& q ) const;
    void matrix_Jcm3_0(Eigen::Matrix<config::scalar_t, 6, 6>& Jcm3_0,
                       const Eigen::Matrix<config::scalar_t, 6, 1>& q ) const;    
    void matrix_Jcm4_0(Eigen::Matrix<config::scalar_t, 6, 6>& Jcm4_0,
                       const Eigen::Matrix<config::scalar_t, 6, 1>& q ) const;
    void matrix_Jcm5_0(Eigen::Matrix<config::scalar_t, 6, 6>& Jcm5_0,
                       const Eigen::Matrix<config::scalar_t, 6, 1>& q ) const;
    void matrix_Jcm6_0(Eigen::Matrix<config::scalar_t, 6, 6>& Jcm6_0,
                       const Eigen::Matrix<config::scalar_t, 6, 1>& q ) const;

    /* jacobian derivatives */
    void matrix_J1_0p(Eigen::Matrix<config::scalar_t, 6, 6>& J1_0p,
                      const Eigen::Matrix<config::scalar_t, 6, 1>& q,
                      const Eigen::Matrix<config::scalar_t, 6, 1>& qp ) const;             
    void matrix_J2_0p(Eigen::Matrix<config::scalar_t, 6, 6>& J2_0p,
                      const Eigen::Matrix<config::scalar_t, 6, 1>& q,
                      const Eigen::Matrix<config::scalar_t, 6, 1>& qp ) const;  
    void matrix_J3_0p(Eigen::Matrix<config::scalar_t, 6, 6>& J3_0p,
                      const Eigen::Matrix<config::scalar_t, 6, 1>& q,
                      const Eigen::Matrix<config::scalar_t, 6, 1>& qp ) const;  
    void matrix_J4_0p(Eigen::Matrix<config::scalar_t, 6, 6>& J4_0p,
                      const Eigen::Matrix<config::scalar_t, 6, 1>& q,
                      const Eigen::Matrix<config::scalar_t, 6, 1>& qp ) const;  
    void matrix_J5_0p(Eigen::Matrix<config::scalar_t, 6, 6>& J5_0p,
                      const Eigen::Matrix<config::scalar_t, 6, 1>& q,
                      const Eigen::Matrix<config::scalar_t, 6, 1>& qp ) const;  
    void matrix_J6_0p(Eigen::Matrix<config::scalar_t, 6, 6>& J6_0p,
                      const Eigen::Matrix<config::scalar_t, 6, 1>& q,
                      const Eigen::Matrix<config::scalar_t, 6, 1>& qp ) const;

    void matrix_Jcm1_0p(Eigen::Matrix<config::scalar_t, 6, 6>& Jcm1_0p,
                        const Eigen::Matrix<config::scalar_t, 6, 1>& q,
                        const Eigen::Matrix<config::scalar_t, 6, 1>& qp ) const;
    void matrix_Jcm2_0p(Eigen::Matrix<config::scalar_t, 6, 6>& Jcm2_0p,
                        const Eigen::Matrix<config::scalar_t, 6, 1>& q,
                        const Eigen::Matrix<config::scalar_t, 6, 1>& qp ) const;
    void matrix_Jcm3_0p(Eigen::Matrix<config::scalar_t, 6, 6>& Jcm3_0p,
                        const Eigen::Matrix<config::scalar_t, 6, 1>& q,
                        const Eigen::Matrix<config::scalar_t, 6, 1>& qp ) const;    
    void matrix_Jcm4_0p(Eigen::Matrix<config::scalar_t, 6, 6>& Jcm4_0p,
                        const Eigen::Matrix<config::scalar_t, 6, 1>& q,
                        const Eigen::Matrix<config::scalar_t, 6, 1>& qp ) const;
    void matrix_Jcm5_0p(Eigen::Matrix<config::scalar_t, 6, 6>& Jcm5_0p,
                        const Eigen::Matrix<config::scalar_t, 6, 1>& q,
                        const Eigen::Matrix<config::scalar_t, 6, 1>& qp ) const;
    void matrix_Jcm6_0p(Eigen::Matrix<config::scalar_t, 6, 6>& Jcm6_0p,
                        const Eigen::Matrix<config::scalar_t, 6, 1>& q,
                        const Eigen::Matrix<config::scalar_t, 6, 1>& qp ) const;

private:
    // parameters
    config::scalar_t L1, L2, L3, L4, L5, L6, L7, L8, L9, L10, L11, L12;
    config::scalar_t m1, m2, m3, m4, m5, m6;
    config::scalar_t I111, I112, I113, I122, I123, I133;
    config::scalar_t I211, I212, I213, I222, I223, I233;
    config::scalar_t I311, I312, I313, I322, I323, I333;
    config::scalar_t I411, I412, I413, I422, I423, I433;
    config::scalar_t I511, I512, I513, I522, I523, I533;
    config::scalar_t I611, I612, I613, I622, I623, I633;
    config::scalar_t gx, gy, gz;

    config::Vector3 gW, g0;
    config::Matrix4 T0;
};

}

#endif
