
#ifndef PRIORITYSWITCHER_H_
#define PRIORITYSWITCHER_H_

#include <unistd.h>
#include <sched.h>
#include <sys/resource.h>

namespace rt_utilities
{

/* settings */
const int POLICY_NORMAL = 0;
const int POLICY_FIFO = 1;
const int POLICY_RR = 2;

const int PRIO_RT = 95;
const int PRIO_NORMAL = 0;

/*
 * Change the priority and scheduler of the current thread
 */
class PrioritySwitcher 
{
public:
	PrioritySwitcher(int policy = 0);
	virtual ~PrioritySwitcher();

    int swtich_realtime_prio();
    int switch_normal_prio();

private:
	pid_t pid;
	pid_t default_prio;
	int default_scheduler_policy;
	int policy;

	void save_default();
};

}

#endif //PRIORITYSWITCHER_H_