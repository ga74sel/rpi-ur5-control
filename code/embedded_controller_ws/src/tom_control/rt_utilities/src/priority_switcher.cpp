#include "rt_utilities/priority_switcher.h"

using namespace rt_utilities;

PrioritySwitcher::PrioritySwitcher(int policy)
    : pid((pid_t) 0), default_prio(-1), policy(policy)
{
	save_default();
}

PrioritySwitcher::~PrioritySwitcher()
{
}

int PrioritySwitcher::swtich_realtime_prio()
{
    struct sched_param param;
	param.sched_priority = PRIO_RT;
	if(policy == POLICY_FIFO)
		return sched_setscheduler(pid, SCHED_FIFO, &param);
	if(policy == POLICY_RR)
		return sched_setscheduler(pid, SCHED_RR, &param);
	return -1;
}

int PrioritySwitcher::switch_normal_prio()
{
    struct sched_param param;
	param.sched_priority = PRIO_NORMAL;
	int rc = sched_setscheduler(pid, default_scheduler_policy, &param);
	rc += setpriority(PRIO_PROCESS, (int) pid, default_prio);
	return rc;
}

void PrioritySwitcher::save_default()
{
	pid = getpid();
	default_prio = getpriority(PRIO_PROCESS, (int) pid);
	default_scheduler_policy = sched_getscheduler((int) pid);
}