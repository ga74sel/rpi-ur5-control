/**
 * @file communication_base.h
 *
 * @brief communication interface used by controller_manager and simulator/robot to exchange state
 *
 * @ingroup hardware_interface
 *
 */

#ifndef ROBOT_HW_BASE_H_
#define ROBOT_HW_BASE_H_

#include <ros/ros.h>
#include <robot_msgs/joint_state.h>

namespace hardware_interface
{

/**
 * Interface of base class of controller interface
 *
 * used to manager jointstate connection between two nodes
 * virtual functions must be reimplemented
 *
 */
class CommunicationBase
{
public:
    CommunicationBase() {};
    virtual ~CommunicationBase() {};

    virtual bool init(ros::NodeHandle& nh) = 0;

    /* check if communication is still possible */
    virtual bool is_alive() = 0;

    /* read current joint state */
    virtual bool read(robot_msgs::JointState& jointstate, ros::Time& time) = 0;

    /* write new joint state */
    virtual bool write(const robot_msgs::JointState& jointstate, const ros::Time& time) = 0;
};

}

#endif