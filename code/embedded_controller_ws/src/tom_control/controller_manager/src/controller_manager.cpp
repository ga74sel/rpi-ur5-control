#include "controller_manager/controller_manager.h"
#include <QElapsedTimer>

using namespace controller_manager;

ControllerManager::ControllerManager(hardware_interface::CommunicationBase& hw, double rate, bool coupled_mode, const ros::NodeHandle& nh_)
    : RtThreads::Thread(RtThreads::Thread::RtThreadMode, "controller_manager"),
      hw(hw),
      coupled_mode(coupled_mode),
      rate(rate),
      nh(nh_),
      state(INITIALIZED),
      is_weight_changed(false),
      warning_time(10.0 / rate)
{
    if( RtThreads::Thread::isRtThread() ) {
        RtThreads::Thread::rtThread()->setPriority(95);
        RtThreads::Thread::rtThread()->setStackSize(16*1024*1024);
        RtThreads::Thread::rtThread()->setStackPreFaultSize(64*1024*1024);
        ROS_INFO_STREAM("ControllerManager: using rtthread");
    }
    else {
        ROS_INFO_STREAM("ControllerManager: using qthread");
    }

    list_controllers_srv = nh.advertiseService("controller_manager/list", &ControllerManager::list_controllers_handler, this);
    set_weights_srv = nh.advertiseService("controller_manager/set_weights", &ControllerManager::set_weights_handler, this);
    if(coupled_mode)
        compute_control_srv = nh.advertiseService("controller_manager/compute_control", &ControllerManager::compute_control_handler, this);

    jointstate.setZero();
}

ControllerManager::~ControllerManager() 
{
    stop_request();
}

bool ControllerManager::add(controller_interface::ControllerBasePtr controller, config::scalar_t gain)
{
    // add controller to list of avaialable controllers
    ROS_INFO_STREAM("ControllerManager: adding " << controller->get_name() << ", idx=" << controllers.size());
    controller_names[controller->get_name()] = controllers.size();
    controllers.push_back( controller );
    weights.push_back( gain );

    return true;
}

bool ControllerManager::start_request()
{
    // initalize everything
    if( !init() ) {
        ROS_ERROR_STREAM("ControllerManager: error init");
        return false;
    }

    idx_started.clear();
    idx_stopped.clear();

    ros::Time cur_t = ros::Time::now();
    prev_t = cur_t;

    for(size_t i = 0; i < controllers.size(); ++i)
        if( weights[i] > 0.0 )
            controllers[i]->start_request( cur_t );
    state = INITIALIZED;

    // go
    RtThreads::Thread::start();
    return true;
}

void ControllerManager::stop_request()
{
    if( state != RUNNING )
        return;
    
    state = STOPPING;
    QElapsedTimer timer;

    while(state != CONSTRUCTED) {
        RtThreads::Thread::usleep(10*1000);
        if( timer.elapsed() > 500 ) {
            RtThreads::Thread::terminate();
            RtThreads::Thread::wait(100);
            ROS_WARN_STREAM("ControllerManager: Thread not responding. Trigger unclean termination");
            state = CONSTRUCTED;
            return;
        }
    }
}

void ControllerManager::run()
{
    // start the loop
    state = RUNNING;

    ros::Rate loop_rate(rate);
    while(ros::ok() && state != STOPPING) {
        if(!coupled_mode)
            update();
        ros::spinOnce();
        loop_rate.sleep();
    }

    // stop controllers
    for(size_t i = 0; i < controllers.size(); ++i)
        controllers[i]->stop_request( ros::Time::now() );

    state = CONSTRUCTED;
}

bool ControllerManager::init()
{
    bool ret = true;

    for( size_t i = 0; i < controllers.size(); ++i) {
        if( !controllers[i]->init_request( nh ) ) {
            ret = false;
            ROS_ERROR_STREAM("ControllerManager: error init controller" << controllers[i]->get_name() );
        }
    }

    if( !hw.init( nh ) ) {
        ret = false;
        ROS_ERROR_STREAM("ControllerManager: error init hw");
    }

    if( controllers.empty() ) {
        ret = false;
        ROS_ERROR_STREAM("ControllerManager: no controllers added" );
    }

    return ret;
}

void ControllerManager::update()
{
    // get time
    config::VectorDOF weigted_tau;
 
    // start/stop controllers
    if( is_weight_changed ) {
        is_weight_changed = false;
        weights = weights_mod;
        for( size_t i = 0; i < idx_stopped.size(); ++i)
            controllers[idx_stopped[i]]->stop_request(ros::Time::now());
        for( size_t i = 0; i < idx_started.size(); ++i)
            controllers[idx_started[i]]->start_request(ros::Time::now());
        idx_stopped.clear();
        idx_started.clear();
    }

    // get joint state from hardware
    ros::Time cur_t;
    ros::Duration dt;
    if( hw.read(jointstate, cur_t) ) {
        dt = cur_t - prev_t;
        prev_t = cur_t;

        // update all controller ( same time index )
        weigted_tau.setZero();
        for(size_t i = 0; i < controllers.size(); ++i ) {
            if( weights[i] > 0.0 )
                weigted_tau += weights[i] * controllers[i]->update_request(jointstate, cur_t, dt);
        }

        // set new state
        jointstate.tau = weigted_tau;
        hw.write(jointstate, cur_t);
    }

    // check if main loop is running to slow
    if( dt.toSec() > warning_time ) {
        ROS_WARN_STREAM("ControllerManager: LOW UPDATE TIME GOT:" << dt.toSec());
    }
}

bool ControllerManager::compute_control_handler(robot_msgs::computecontroldataRequest& req, robot_msgs::computecontroldataResponse& res)
{
    // look srv
    std::lock_guard<std::mutex> lock(srv_mutex);

    jointstate.Q = config::Vector6( req.data.Q.data() );
    jointstate.Qp = config::Vector6( req.data.Qp.data() );
    jointstate.Qpp = config::Vector6( req.data.Qpp.data() );
    ros::Time cur_t = req.data.header.stamp;
    ros::Duration dt( req.data.header.dt );

    // start/stop controllers
    if( is_weight_changed ) {
        is_weight_changed = false;
        weights = weights_mod;
        for( size_t i = 0; i < idx_stopped.size(); ++i)
            controllers[idx_stopped[i]]->stop_request(ros::Time::now());
        for( size_t i = 0; i < idx_started.size(); ++i) {
            controllers[idx_started[i]]->start_request(ros::Time::now());
        }
        idx_stopped.clear();
        idx_started.clear();
    }

    config::VectorDOF weigted_tau;
    weigted_tau.setZero();

    // update all controller ( same time index )
    for(size_t i = 0; i < controllers.size(); ++i ) {
        if( weights[i] > 0.0 )
            weigted_tau += weights[i] * controllers[i]->update_request(jointstate, cur_t, dt);
    }

    // set new state
    jointstate.tau = weigted_tau;
    res.data = res.data;
    res.data.tau = std::vector<config::scalar_t>(jointstate.tau.data(), jointstate.tau.data() + jointstate.tau.size());
    return true;
}

bool ControllerManager::set_weights_handler(controller_manager_msgs::setweightsRequest& req, controller_manager_msgs::setweightsResponse& res)
{
    // look srv
    std::lock_guard<std::mutex> lock(srv_mutex);

    // copy current weights
    weights_mod = weights;

    auto& vec = req.weights;
    for( size_t i = 0; i < vec.size(); ++i) {

        // check if name exsits
        auto it = controller_names.find( vec[i].name );
        if( it != controller_names.end() ) {
            // controller exists, set new weight
            int idx = it->second;
            if( weights_mod[idx] != vec[i].weight ) {
                // changed, send new start request
                weights_mod[idx] = vec[i].weight;
                if( weights_mod[idx] > 0.0 )
                    idx_started.push_back(idx);
                else
                    idx_stopped.push_back(idx);
            }
        }
    }
    is_weight_changed = true;
    return true;
}

bool ControllerManager::list_controllers_handler(controller_manager_msgs::listcontrollersRequest& req, controller_manager_msgs::listcontrollersResponse& res)
{
    // look srv
    std::lock_guard<std::mutex> lock(srv_mutex);

    // get controller infos
    auto& infos = res.infos;
    infos.resize( controllers.size() );
    for( size_t i = 0; i < infos.size(); ++i ) {
        infos[i].weight = weights[i];
        infos[i].name = controllers[i]->get_name();
        infos[i].space = controllers[i]->get_space();
        infos[i].id = i;
    }
    return true;
}
