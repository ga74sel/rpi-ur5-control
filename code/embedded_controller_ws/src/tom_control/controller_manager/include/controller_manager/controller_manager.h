/**
 * @file controller_manager.h
 *
 * @brief start, stops and update of controllers
 *
 * @ingroup controller_manager
 *
 */

#ifndef CONTROLLER_MANAGER_H_
#define CONTROLLER_MANAGER_H_

#include <RtThreads/Thread.h>

#include <mutex>

#include <controller_interface/controller_base.h>

#include <hardware_interface/communication_base.h>

#include <controller_manager_msgs/listcontrollers.h>
#include <controller_manager_msgs/setweights.h>

#include <robot_msgs/joint_state.h>
#include <robot_msgs/computecontroldata.h>

namespace controller_manager 
{

/**
 * Implementation of manager class of controllers
 *
 * starts, stops and updates controllers
 * 
 * Two possibilites to use:
 * 1.   Let this thread run freely without tight coupeling to robot control loop 
 *      In this case the update controllers function updates all controllers with at the rate of this thread and
 *      the jointstate is read and writen through hardware_interface
 * 
 * 2.   Use the service compute_control to initiate a full control update ( coupled mode )
 *      In this case the update controllers function is only called if someone calls that service
 *      jointstate is transported via service
 */
class ControllerManager : public RtThreads::Thread
{
public:
    enum State {CONSTRUCTED, INITIALIZED, RUNNING, STOPPING};

    /** 
    * @brief constructor
    *
    * @param hardware_interface to comunicate read/write jointstate in non coupeled_mode
    * @param thread freq
    * @param coupled_mode weather to compute updates based on service calls or freely
    * @param ros nodehande
    */
    ControllerManager(hardware_interface::CommunicationBase& hw, double rate = 1000.0, bool coupled_mode = true, const ros::NodeHandle& nh = ros::NodeHandle());
    ~ControllerManager();

    /** 
    * @brief add a controller to the controller manager
    *
    * @param shared controller ptr to controller
    * @param controller gain
    */
    bool add(controller_interface::ControllerBasePtr controller, config::scalar_t gain);

    /** 
    * @brief init and start controller thread
    */
    bool start_request();

    /** 
    * @brief stop all controllers and end thread 
    */
    void stop_request();

private:
    /** 
    * @brief init interal parameter
    */
    bool init();

    /** 
    * @brief run loop
    */
    void run();

    /** 
    * @brief controller updates
    */
    void update();

    /// @brief service handle
    bool list_controllers_handler(controller_manager_msgs::listcontrollersRequest& req, controller_manager_msgs::listcontrollersResponse& res);
    /// @brief service handle
    bool set_weights_handler(controller_manager_msgs::setweightsRequest& req, controller_manager_msgs::setweightsResponse& res);
    /// @brief service handle
    bool compute_control_handler(robot_msgs::computecontroldataRequest& req, robot_msgs::computecontroldataResponse& res);

private:
    State state;

    bool coupled_mode;                         

    ros::NodeHandle nh;
    ros::ServiceServer set_weights_srv;         // set weights of controllers
    ros::ServiceServer list_controllers_srv;    // list available controllers
    ros::ServiceServer compute_control_srv;     // force the controller to run a iteration

    hardware_interface::CommunicationBase& hw;  // comunicate with robot

    robot_msgs::JointState jointstate;          // Current Q, Qp, Qpp
    bool is_weight_changed;                     // true if controller switched

    controller_interface::Controllers controllers;  // vector of controllers
    std::vector<int> idx_started, idx_stopped;      // started and stopped controller indices

    std::vector<config::scalar_t> weights;          // vector of controller weights
    std::vector<config::scalar_t> weights_mod;      // modified vector of controller weights
    
    std::map<std::string, int> controller_names;    // mapping between names and vector id

    double rate;                // control frequency
    ros::Time prev_t;           // prev_time of control loop
    double warning_time;        // warn about low update time  
    
    std::mutex srv_mutex;
};

}

#endif
