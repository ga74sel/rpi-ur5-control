#include "robot_simulator/rpi_communication.h"

using namespace rpi;
using namespace config;
using namespace robot_msgs;

RPICommunication::RPICommunication(const std::string& src_name, const std::string& dst_name, double time_out)
    : state(CONSTRUCTED),
      time_out(time_out),
      src_name(src_name),
      dst_name(dst_name)
{
    jointstate.setZero();
}

RPICommunication::~RPICommunication()
{
}

bool RPICommunication::init(ros::NodeHandle& nh)
{
    bool ret;

    // connections 
    torquedata_sub = nh.subscribe<robot_msgs::controldata>(dst_name + "/torquedata", 1, &RPICommunication::torquedata_callback, this);
    joint_state_server = nh.advertiseService(src_name + "/read_joint_state", &RPICommunication::joint_state_handler, this);

    // wait till destination is ready
    ROS_INFO_STREAM(src_name + " waiting for " + dst_name + " to be ready...");
    ret = ros::service::waitForService(dst_name + "/available", ros::Duration(10.0));
    ROS_INFO_STREAM(src_name + " to " + dst_name + " is ready");

    state = INITIALIZED;
    jointstate.setZero();
    cmd_time = ros::Time::now();

    return ret;
}

/* check if communication is still possible */
bool RPICommunication::is_alive()
{
    // check if communication channel is still open
    if( state == CONNECTED ) {

        static bool time_flag = true;
        if(time_flag) {
            time_flag = false;
            ROS_INFO_STREAM("is_alive               : " << (ros::Time::now() - cmd_time).toSec() );
        }

        // connected: check time limit
        if( (ros::Time::now() - cmd_time).toSec() > time_out ) {
            ROS_INFO_STREAM("error time: " << (ros::Time::now() - cmd_time).toSec());
            state = ERROR;
            return false;
        }
        return true;
    }
    else if( state == ERROR ) {
        // stay in error state
        return false;
    }
    // not started yet
    return false;
}

/* read current joint state */
bool RPICommunication::read(robot_msgs::JointState& jointstate_, ros::Time& time)
{
    // look jointstate
    std::lock_guard<std::mutex> lock(jointstate_mutex);

    // return last command recieved by controller
    time = cmd_time;
    jointstate_.tau = jointstate.tau;
    return state == CONNECTED;
}

/* write new joint state */
bool RPICommunication::write(const robot_msgs::JointState& jointstate_, const ros::Time& time)
{
    // look jointstate
    std::lock_guard<std::mutex> lock(jointstate_mutex);

    // check if connected
    if( state != CONNECTED ) {
        if( state == INITIALIZED)
            state = RECEIVED1;
        else if( state == RECEIVED2)
            state = CONNECTED;
    }

    // save internally
    cur_time = time;
    jointstate.Q = jointstate_.Q;
    jointstate.Qp = jointstate_.Qp;
    jointstate.Qpp = jointstate_.Qpp;
    return true;
}

void RPICommunication::torquedata_callback(const robot_msgs::controldataConstPtr& msg)
{
    // look jointstate
    std::lock_guard<std::mutex> lock(jointstate_mutex);

    // check if connected
    if( state != CONNECTED ) {
        if( state == INITIALIZED )
            state = RECEIVED2;
        else if( state == RECEIVED1 ) {
            state = CONNECTED;
        }
    }

    // check if msg contains NANs
    if( jointstate.tau.array().isNaN().any() ) {
        ROS_ERROR_STREAM("RPICommunication::torquedata_callback : got NAN tau");
        state = ERROR;
        return;
    }

    // save new command
    jointstate.tau = Vector6( msg->tau.data() );
    cmd_time = ros::Time::now();
}

bool RPICommunication::joint_state_handler(robot_msgs::readcontroldata::Request& req, robot_msgs::readcontroldata::Response& res)
{
    // look jointstate
    std::lock_guard<std::mutex> lock(jointstate_mutex);

    if( state == RECEIVED1 || state == CONNECTED ) {
        res.data.header.stamp = cur_time;
        res.data.Q = std::vector<scalar_t>(jointstate.Q.data(), jointstate.Q.data() + jointstate.Q.size());
        res.data.Qp = std::vector<scalar_t>(jointstate.Qp.data(), jointstate.Qp.data() + jointstate.Qp.size());
        res.data.Qpp = std::vector<scalar_t>(jointstate.Qpp.data(), jointstate.Qpp.data() + jointstate.Qpp.size());
        return true;
    }
    return false;
}