/**
 * @file conv_joint_state.h
 *
 * @brief helper function to convert a jointstate to message/service
 *
 * @ingroup robot_msgs
 *
 */

#ifndef CONST_JOINT_STATE_H_
#define CONST_JOINT_STATE_H_

#include "robot_msgs/joint_state.h"

#include "robot_msgs/writecontroldata.h"

#include "robot_msgs/readcontroldata.h"

#include "robot_msgs/computecontroldata.h"

namespace robot_msgs
{

/** 
* @brief convert jointstate to ros msg
*
* @param controldata
* @param jointstate
* @param current time
* @param delta time
*/
inline bool write_jointstate(robot_msgs::controldata& data, const JointState& state, const ros::Time& time, const ros::Duration& dt) {
    // header
    data.header.stamp = time;
    data.header.dt = dt.toSec();

    // desired
    data.D = std::vector<config::scalar_t>(state.D.data(), state.D.data() + state.D.size());
    data.Dp = std::vector<config::scalar_t>(state.Dp.data(), state.Dp.data() + state.Dp.size());
    data.Dpp = std::vector<config::scalar_t>(state.Dpp.data(), state.Dpp.data() + state.Dpp.size());

    // current
    data.Q = std::vector<config::scalar_t>(state.Q.data(), state.Q.data() + state.Q.size());
    data.Qp = std::vector<config::scalar_t>(state.Qp.data(), state.Qp.data() + state.Qp.size());
    data.Qpp = std::vector<config::scalar_t>(state.Qpp.data(), state.Qpp.data() + state.Qpp.size());

    // reference
    data.Qrp = std::vector<config::scalar_t>(state.Qrp.data(), state.Qrp.data() + state.Qrp.size());
    data.Qrpp = std::vector<config::scalar_t>(state.Qrpp.data(), state.Qrpp.data() + state.Qrpp.size());

    // tau
    data.tau = std::vector<config::scalar_t>(state.tau.data(), state.tau.data() + state.tau.size());
    return true;
}

/** 
* @brief convert jointstate to ros srv
*
* @param controldata
* @param jointstate
* @param current time
* @param delta time
*/
inline bool write_jointstate(robot_msgs::writecontroldata& msg, const JointState& state, const ros::Time& time, const ros::Duration& dt) {
    return write_jointstate(msg.request.data, state, time, dt);
}

/** 
* @brief convert jointstate to ros srv
*
* @param controldata
* @param jointstate
* @param current time
*/
inline bool write_jointstate(robot_msgs::writecontroldata& msg, const JointState& state, const ros::Time& time) {
    ros::Duration dt(0);
    return write_jointstate(msg.request.data, state, time, dt);
}

/** 
* @brief convert jointstate to ros srv
*
* @param controldata
* @param jointstate
* @param current time
* @param delta time
*/
inline bool write_jointstate(robot_msgs::computecontroldata& msg, const JointState& state, const ros::Time& time, const ros::Duration& dt) {
    return write_jointstate(msg.request.data, state, time, dt);
}

/** 
* @brief convert jointstate to ros srv
*
* @param controldata
* @param jointstate
* @param current time
*/
inline bool write_jointstate(robot_msgs::computecontroldata& msg, const JointState& state, const ros::Time& time) {
    ros::Duration dt(0);
    return write_jointstate(msg.request.data, state, time, dt);
}

/** 
* @brief convert msg to jointstate
*
* @param controldata
* @param jointstate
* @param current time
* @param delta time
*/
inline bool read_jointstate(const robot_msgs::controldata& data, JointState& state, ros::Time& time, ros::Duration& dt) {
    // header
    time = data.header.stamp;
    dt = ros::Duration(data.header.dt);

    // desired
    state.D = config::Vector7( data.D.data() );
    state.Dp = config::Vector6( data.Dp.data() );
    state.Dpp = config::Vector6( data.Dpp.data() );

    // current
    state.Q = config::Vector6( data.Q.data() );
    state.Qp = config::Vector6( data.Qp.data() );
    state.Qpp = config::Vector6( data.Qpp.data() );

    // reference
    state.Qrp = config::Vector6( data.Qrp.data() );
    state.Qrpp = config::Vector6( data.Qrpp.data() );

    // tau
    state.tau = config::Vector6( data.tau.data() );
}

/** 
* @brief convert srv to jointstate
*
* @param controldata
* @param jointstate
* @param current time
* @param delta time
*/
inline bool read_jointstate(const robot_msgs::readcontroldata& msg, JointState& state, ros::Time& time, ros::Duration& dt) {
    return read_jointstate(msg.response.data, state, time, dt);
}

/** 
* @brief convert srv to jointstate
*
* @param controldata
* @param jointstate
* @param current time
*/
inline bool read_jointstate(const robot_msgs::readcontroldata& msg, JointState& state, ros::Time& time) {
    ros::Duration dt;
    return read_jointstate(msg.response.data, state, time, dt);
}

/** 
* @brief convert srv to jointstate
*
* @param controldata
* @param jointstate
* @param current time
* @param delta time
*/
inline bool read_jointstate(const robot_msgs::computecontroldata& msg, JointState& state, ros::Time& time, ros::Duration& dt) {
    return read_jointstate(msg.response.data, state, time, dt);
}

/** 
* @brief convert srv to jointstate
*
* @param controldata
* @param jointstate
* @param current time
*/
inline bool read_jointstate(const robot_msgs::computecontroldata& msg, JointState& state, ros::Time& time) {
    ros::Duration dt;
    return read_jointstate(msg.response.data, state, time, dt);
}

}

#endif

