#ifndef CONTROLLER_BASE
#define CONTROLLER_BASE

/**
 * @file controller_base.h
 *
 * @brief base class for all controllers
 *
 * @ingroup controller_interface
 *
 */

#include <ros/node_handle.h>
#include <robot_msgs/joint_state.h>
#include <string>
#include <memory>

namespace controller_interface
{

/**
 * Interface description of all controllers
 *
 * virtual functions must be reimplemented
 *
 */
class ControllerBase
{
public:
    enum State {CONSTRUCTED, INITIALIZED, RUNNING};
    enum Space {CARTESIAN, JOINT};

    /** 
    * @brief constructor
    *
    * @param controller name
    * @param configuration space
    */
    ControllerBase(std::string name, Space space)
        : name(name),
          state(CONSTRUCTED),
          space(space) {}
    virtual ~ControllerBase() {}

    /** 
    * @brief init interal parameter
    *
    * @param ros nh for namespace
    */
    bool init_request(ros::NodeHandle& nh) {
        if( init(nh) ) {
            state = INITIALIZED;
            return true;
        }
        return false;
    }

    /** 
    * @brief start the controller
    *
    * @param starting time
    */
    bool start_request(const ros::Time& time) {
        if( state == INITIALIZED ) {
            state = RUNNING;
            start( time );
            return true;
        }
        return false;
    }

    /** 
    * @brief stop the controller
    *
    * @param stopping time
    */
    bool stop_request(const ros::Time& time) {
        if( state == RUNNING ) {
            state = INITIALIZED;
            stop( time );
            return true;
        }
        return false;
    }

    /** 
    * @brief performs update step of the controller, called periodically
    *
    * @param current jointstate
    * @param current time
    * @param current deltatime since last call
    */
    config::VectorDOF update_request(robot_msgs::JointState& jointstate, const ros::Time& time, const ros::Duration& dt) {
        if( state == RUNNING )
            return update( jointstate, time, dt);
        return config::VectorDOF::Zero();
    }

    /** 
    * @brief get the controller name
    *
    * @return controller name
    */
    std::string get_name() const { return name; }
    /** 
    * @brief get the configuration space
    *
    * @return configuration space
    */
    Space get_space() const { return space; }
    /** 
    * @brief return if is running
    *
    * @return running flag
    */
    bool is_running() const { return state == RUNNING; }

private:
    /** 
    * @brief init interal parameter
    *
    * @param ros nh for namespace
    */
    virtual bool init(ros::NodeHandle& nh) = 0;

    /** 
    * @brief start the controller, called befor update
    *
    * @param starting time
    */
    virtual void start(const ros::Time& time) {}

    /** 
    * @brief performs update step of the controller, called periodically
    *
    * @param current jointstate
    * @param current time
    * @param current deltatime since last call
    * @return computed torque value
    */
    virtual config::VectorDOF update(robot_msgs::JointState& state, const ros::Time& time, const ros::Duration& dt) = 0;

    /** 
    * @brief stop the controller, called befor stopping
    *
    * @param stopping time
    */
    virtual void stop(const ros::Time& time) {}

private:
    State state;            // current controller state
    Space space;            // space
    std::string name;       // controller identifier
};

typedef std::shared_ptr<controller_interface::ControllerBase> ControllerBasePtr;
typedef std::shared_ptr<const controller_interface::ControllerBase> ControllerBaseConstPtr;
typedef std::vector<ControllerBasePtr> Controllers;

}

#endif
