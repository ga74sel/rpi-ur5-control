/**
 * @file model_base.h
 *
 * @brief base class for all robot models
 *
 * @ingroup model_interface
 *
 */

#ifndef MODEL_BASE_H_
#define MODEL_BASE_H_

#include <controller_utilities/config.h>

/**
* Defines the basic interface every model should provide
*
* used by trajectory generator, controllers, simulator
*/

namespace model_interface
{

class ModelBase
{
public:
    /** 
    * @brief constructor
    *
    * @param name of robot
    */
    ModelBase(const std::string& name)
        : name(name),
          is_init(false)
    {
    }
    virtual ~ModelBase()
    {
    }

    /** 
    * @brief init interal parameter
    */
    bool init() {
        if( !is_init ) {
            is_init = init_request(name);
        }
        return is_init;
    }

    /* inertia matrix */

    /** 
    * @brief inertia matrix computation
    *
    * @param inertia Matrix
    * @param joint position
    */
    virtual void M(config::MatrixDOF& M, const config::VectorDOF& q ) const = 0;

    /** 
    * @brief coriolis centripetal matrix computation
    *
    * @param coriolis centripetal matrix
    * @param joint position
    * @param joint velocity
    */ 
    virtual void C(config::MatrixDOF& C, const config::VectorDOF& q, const config::VectorDOF& qp ) const = 0;

    /** 
    * @brief gravitational vector computation
    *
    * @param gravitational vector
    * @param joint position
    */
    virtual void G(config::VectorDOF& G, const config::VectorDOF& q ) const = 0;

    /** 
    * @brief returns regressor matrix of correct size
    */
    virtual config::MatrixX init_Yr() const = 0;

    /** 
    * @brief regressor matrix computation
    *
    * @param regressor matrix
    * @param joint position
    * @param joint velocity
    * @param reference joint velocity
    * @param reference joint acceleration
    */
    virtual void Yr(config::MatrixX& Yr, const config::VectorDOF& q, const config::VectorDOF& qp, const config::VectorDOF& qrp, const config::VectorDOF& qrpp ) const = 0;

    /** 
    * @brief returns regressor matrix of correct size for joint j
    * @param joint j
    */
    virtual config::MatrixX init_Yr_j(int j) const = 0;

    /** 
    * @brief regressor matrix computation for joint j
    *
    * @param regressor matrix
    * @param joint position
    * @param joint velocity
    * @param reference joint velocity
    * @param reference joint acceleration
    * @param joint j
    */
    virtual void Yr_j(config::MatrixX& Yr_j, const config::VectorDOF& q, const config::VectorDOF& qp, const config::VectorDOF& qrp, const config::VectorDOF& qrpp, int j ) const = 0;

    /** 
    * @brief parameter vector
    */
    virtual config::VectorX init_th() const = 0;

    /** 
    * @brief gravity vector wrt world
    */
    virtual config::Vector3 g_W() const = 0;
    
    /** 
    * g@brief ravity vector wrt base
    */
    virtual config::Vector3 g_0() const = 0;

    /** 
    * @brief base wrt world transformation
    */
    virtual config::Matrix4 T0_W() const = 0;

    /** 
    * @brief endeffector transformation matrix
    */
    virtual void T_ef(config::Matrix4& T_ef, const config::VectorDOF& q ) const = 0;

    /** 
    * @brief joint transformation
    */
    virtual void T_j(config::Matrix4& T_j, const config::VectorDOF& q, int j) const = 0;

    /** 
    * @brief center of mass transformaiton
    */
    virtual void Tcm_j(config::Matrix4& Tcm, const config::VectorDOF& q, int j) const = 0;

    /** 
    * @brief endeffector jacobian matrix
    */
    virtual void J_ef(config::MatrixDOF& J_ef, const config::VectorDOF& q ) const = 0;

    /** 
    * @brief jacobian matrix at j*/
    virtual void J_j(config::MatrixDOF& J_j, const config::VectorDOF& q, int j) const = 0;

    /** 
    * @brief center of mass jacobian matrix at j
    */
    virtual void Jcm_j(config::MatrixDOF& Jcm_j, const config::VectorDOF& q, int j) const = 0;

    /** 
    * @brief jacobian derivatives 
    */
    virtual void Jp_ef(config::MatrixDOF& Jp_ef, const config::VectorDOF& q, const config::VectorDOF& qp ) const = 0;

    /** 
    * @brief jacobian derivatives 
    */
    virtual void Jp_j(config::MatrixDOF& Jp_j, const config::VectorDOF& q, const config::VectorDOF& qp, int j) const = 0;

    /** 
    * @brief jacobian derivatives 
    */
    virtual void Jpcm_j(config::MatrixDOF& Jp_j, const config::VectorDOF& q, const config::VectorDOF& qp, int j) const = 0;

    /** 
    * @brief get model name 
    */
    std::string get_name() const { return name; }

private:
   virtual bool init_request(const std::string& ns) = 0;

private:
    std::string name;
    bool is_init;
};

}

#endif
