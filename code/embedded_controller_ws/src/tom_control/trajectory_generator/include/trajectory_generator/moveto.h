/**
 * @file moveto.h
 *
 * @brief helper classes for fast creation of TrajectoryStates
 *
 * @ingroup trajectory_generator
 *
 */

#ifndef MOVE_TO_H_
#define MOVE_TO_H_

#include <stdlib.h>
#include <string>
#include <robot_msgs/controldata.h>

#include <trajectory_generator/states.h>
#include <trajectory_generator/transitions.h>
#include <trajectory_generator/trajectories.h>

using namespace trajectory_generator;

template<typename T, typename... Args>
std::unique_ptr<T> make_unique(Args&&... args) {
    return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
}

/**
 * Point to point motion in Joint Space
 */
class MoveToPointJointState : public JointTrajectoryState
{
public:
    /** 
    * @brief constructor
    *
    * @param state name
    * @param controller name
    * @param controller mode 
    * @param time to complete task
    * @param inital jointstate
    * @param final jointstate
    * @param is final realtive to inital
    */
    MoveToPointJointState(const std::string& name, const std::string& controller_name, const Mode& mode, double t,
                          const config::Vector6& Q_i, const config::Vector6& Q_f, bool relative = false)
        : JointTrajectoryState(name, controller_name, mode, make_unique<CubicTimeLaw>(t), make_unique<LinearJointPath>(Q_i, Q_f), relative)
    {
    }

    /** 
    * @brief constructor
    *
    * @param state name
    * @param controller name
    * @param controller mode 
    * @param time to complete task
    * @param final jointstate
    * @param is final realtive to inital
    */
    MoveToPointJointState(const std::string& name, const std::string& controller_name, const Mode& mode, double t,
                          const config::Vector6& Q_f, bool relative = false)
        : JointTrajectoryState(name, controller_name, mode, make_unique<CubicTimeLaw>(t), make_unique<LinearJointPath>(Q_f), relative)
    {
    }
};

/*
 * point to point motion in cartesian space
 */
class MoveToPointCartesianState : public CartesianTrajectoryState
{
public:
    /** 
    * @brief constructor
    *
    * @param state name
    * @param controller name
    * @param controller mode 
    * @param time to complete task
    * @param inital position
    * @param final position
    * @param inital rotation
    * @param final rotation
    * @param is final realtive to inital
    */
    MoveToPointCartesianState(const std::string& name, model_interface::ModelBase& model, const std::string& controller_name, const Mode& mode, double t,
                              const config::Vector3& X_i, const config::Vector3& X_f,
                              const config::Matrix3& R_i, const config::Matrix3& R_f, bool relative = false)
        : CartesianTrajectoryState(name, model, controller_name, mode, make_unique<CubicTimeLaw>(t), make_unique<LinearCartesianPath>(X_i, X_f),
                                   make_unique<SLERPRotationPath>(R_i, R_f), relative)
    {
    }
    
    /** 
    * @brief constructor
    *
    * @param state name
    * @param controller name
    * @param controller mode 
    * @param time to complete task
    * @param final position
    * @param final rotation
    * @param is final realtive to inital
    */
    MoveToPointCartesianState(const std::string& name, model_interface::ModelBase& model, const std::string& controller_name, const Mode& mode, double t,
                              const config::Vector3& X_f, const config::Matrix3& R_f, bool relative = false)
        : CartesianTrajectoryState(name, model, controller_name, mode, make_unique<CubicTimeLaw>(t), make_unique<LinearCartesianPath>(X_f),
                                   make_unique<SLERPRotationPath>(R_f), relative)
    {
    }
};

/** 
* @brief wait transition between two states
*
* @param state from
* @param state to
* @param time to wait in seconds
*/
void wait(StateBasePtr a, StateBasePtr b, double time) {
    TransitionBasePtr t = std::make_shared<TimeTransition>("T", a, b, time);
    a->add_transition( t );
}

/** 
* @brief reach jointstate transition between
*
* @param state from
* @param state to
*/
void reach_joint(StateBasePtr a, StateBasePtr b) {
    TransitionBasePtr t = std::make_shared<ReachJointTransition>("T", a, b);
    a->add_transition( t );
}

/** 
* @brief reach cartesian state transition
*
* @param state from
* @param state to
*/
void reach_cartesian(model_interface::ModelBase& model, StateBasePtr a, StateBasePtr b, config::scalar_t eps = 1e-2) {
    TransitionBasePtr t = std::make_shared<ReachCartesianTransition>("T", model, a, b, eps);
    a->add_transition( t );
}

#endif
