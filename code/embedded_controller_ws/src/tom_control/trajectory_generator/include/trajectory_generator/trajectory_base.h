#ifndef TRAJECTORY_BASE_H_
#define TRAJECTORY_BASE_H_

#include <Eigen/Dense>
#include <robot_msgs/joint_state.h>
#include <memory>

namespace trajectory_generator
{

typedef config::scalar_t scalar_t;

/*
 * BaseClass of a Timing law
 * 
 * manages the time evolution along a path by specifing a function
 * for the parameter s(t)
 */
class TimeLaw
{
public:
    TimeLaw(scalar_t t_f)
        : t_f(t_f)
    {
    }
    virtual ~TimeLaw()
    {
    }

    /** set final time */
    void set_final(scalar_t t_f) {
        this->t_f = t_f;
    }

    /**
     * called before update to give final time
     */
    virtual void start_request() {
        start( t_f );            
    }

    /**
     * dt:  time
     * s:   parameter [0, 1]
     * sp:  ds/dt
     * spp: d/dt (ds/dt)
     */
    virtual void update(scalar_t t, scalar_t& s, scalar_t& sp, scalar_t& spp) = 0;

private:
    virtual void start(scalar_t t_f) = 0;

private:
    scalar_t t_f;
};

/*
 * Describe a path in jointspace
 *  Q_i:    inital joint position
 *  Q_f:    final joint position
 *  is_rel: Q_f will be interpreted as relative transformation
 */
class JointPath
{
public: 
    JointPath(const config::Vector6& Q_i, const config::Vector6& Q_f)
        : Q_i(Q_i),
          Q_f(Q_f),
          is_inital_set(true)
    {
    }
    JointPath(const config::Vector6& Q_f)
        : Q_f(Q_f),
          is_inital_set(false)
    {
    }
    virtual ~JointPath()
    {
    }

    /* set the final joint position */
    void set_final(const config::Vector6& Q_f) {
        this->Q_f = Q_f;
    }

    /*
     * called before update gives inital position of the path
     * Q_i: inital position (current position)
     * Q_f: computed final position (output)
     */
    virtual void start_request(const config::Vector6& Q_i, config::Vector6& Q_f, bool is_rel = false) {
        config::Vector6 Q_i_ = Q_i;
        Q_f = this->Q_f;

        if( is_inital_set )
            Q_i_ = this->Q_i;

        if( is_rel )
            Q_f = Q_i + this->Q_f;

        start( Q_i_, Q_f );            
    }

    /*
     * s: parameter that evolves in time s=[0,1]
     * Q: position vector Q
     * t_vec: tangential vector d/ds X
     * c_vec: curvature vector d^2/ds^2 X
     */
    virtual void update(float s, config::Vector6& Q, config::Vector6& t_vec, config::Vector6& c_vec) = 0;

private:
    virtual void start(const config::Vector6& Q_i, const config::Vector6& Q_f) = 0;

private:
    config::Vector6 Q_i, Q_f;
    bool is_inital_set;
};

/*
 * Describe a path in cartesian space
 *  X_i:    inital joint position
 *  X_f:    final joint position
 */
class CartesianPath
{
public: 
    CartesianPath(const config::Vector3& X_i, const config::Vector3& X_f)
        : X_i(X_i),
          X_f(X_f),
          is_inital_set(true)
    {
    }
    CartesianPath(const config::Vector3& X_f)
        : X_f(X_f),
          is_inital_set(false)
    {
    }
    virtual ~CartesianPath()
    {
    }

    /* set the final joint position */
    void set_final(const config::Vector3& X_f) {
        this->X_f = X_f;
    }

    /*
     * called before update gives inital position of the path
     * X_i: inital position (current position)
     * X_f: computed final position (output)
     */
    virtual void start_request(const config::Vector3& X_i, config::Vector3& X_f, bool is_rel = false) {
        config::Vector3 X_i_ = X_i;
        X_f = this->X_f;

        if( is_inital_set )
            X_i_ = this->X_i;
        if( is_rel )
            X_f = X_i_ + this->X_f;

        start(X_i_, X_f);
    }

    /*
     * s: parameter that evolves in time s=[0,1]
     * X: position vector X
     * t_vec: tangential vector d/ds X
     * c_vec: curvature vector d^2/ds^2 X
     */
    virtual void update(float s, config::Vector3& X, config::Vector3& t_vec, config::Vector3& c_vec) = 0;

private:
    virtual void start(const config::Vector3& X_i, const config::Vector3& X_f) = 0;

private:
    config::Vector3 X_i, X_f;
    bool is_inital_set;
};

/*
 * Describe a path in cartesian space
 *  X_i:    inital joint position
 *  X_f:    final joint position
 */
class RotationPath
{
public: 
    RotationPath(const config::Matrix3& R_i, const config::Matrix3& R_f)
        : R_i(R_i),
          R_f(R_f),
          is_inital_set(true)
    {
    }
    RotationPath(const config::Matrix3& R_f)
        : R_f(R_f),
          is_inital_set(false)
    {
    }
    virtual ~RotationPath()
    {
    }

    /* set the final joint position */
    void set_final(const config::Matrix3& R_f) {
        this->R_f = R_f;
    }

    /*
     * called before update gives inital rotation of the path
     * R_i: inital rotation (current rotation)
     * R_f: computed final rotation (output)
     */
    virtual void start_request(const config::Matrix3& R_i, config::Matrix3& R_f, bool is_rel = false) {
        config::Matrix3 R_i_ = R_i;
        R_f = this->R_f;

        if( is_inital_set )
            R_i_ = this->R_i;
        if( is_rel )
            R_f = R_i * this->R_f;
        start(R_i_, R_f);
    }

    /*
     * s: parameter that evolves in time s=[0,1]
     * q:   Quaternion
     * qp:  1. Derivative d/ds q (not normalized)
     * qpp: 2. Derivative d^2/ds^2 q (not normalized)
     */
    virtual void update(float s, config::Quaternion& q, config::Vector4& qp, config::Vector4& qpp) = 0;

private:
    virtual void start(const config::Matrix3& R_i, const config::Matrix3& R_f) = 0;

protected:
    config::Matrix3 R_i, R_f;
    bool is_inital_set;
};

}

#endif