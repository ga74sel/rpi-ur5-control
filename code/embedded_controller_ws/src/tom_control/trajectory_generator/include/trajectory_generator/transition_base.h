#ifndef TRANSITION_BASE_H_
#define TRANSITION_BASE_H_

#include <memory>
#include <ros/ros.h>
#include <robot_msgs/joint_state.h>

namespace trajectory_generator
{

// forward declaration Statebase
class StateBase;
typedef std::shared_ptr<StateBase> StateBasePtr;
typedef std::shared_ptr<const StateBase> StateBaseConstPtr;

/* 
 * Baseclass for every transition
 * reimplement:
 *  - init()
 *  - start()
 *  - stop()
 *  - fulfilled()
 */
class TransitionBase
{
public:
    enum State {CONSTRUCTED, INITIALIZED, RUNNING};

    TransitionBase(const std::string& name, StateBaseConstPtr src, StateBasePtr dst)
        : name(name),
          src(src),
          dst(dst),
          active(false),
          set(false),
          src_idx(-1),
          dst_idx(-1)
    { 
    }
    virtual ~TransitionBase() {}

    /* returns if transition is active */
    bool is_active() const { return active; }

    /* returns true if transition if fullfilled */
    bool is_fulfilled() const { return set; }

    /* get ids of states */
    int get_src_id() const { return src_idx; }
    int get_dst_id() const { return dst_idx; }

    /* init transition */
    bool init_request(ros::NodeHandle& nh);

    /* stop transition */
    bool stop_request(robot_msgs::JointState& jointstate, const ros::Time& time) {
        active = false;
        stop( jointstate, time );
        return true;
    }

    /* stop transition */
    bool start_request(robot_msgs::JointState& jointstate, const ros::Time& time) {
        active = true;
        start( jointstate, time );
        return true;
    }

    /* called periodically, joint state contrains current angles, velo, accel */
    void update_request(robot_msgs::JointState& jointstate, const ros::Time& time, const ros::Duration& dt) {
        if( active ) {
            // check if transition is fulfilled
            set = fulfilled( jointstate, time, dt);
            if( set )
                active = false;
        }
    }

private:
    /* init controller parameters */
    virtual bool init(ros::NodeHandle& nh) { return true; };

    /* called before first time to update joint state contains desired position */
    virtual void start(robot_msgs::JointState& state, const ros::Time& time) {}

    /* called periodically, return true is transition criteria is matched */
    virtual bool fulfilled(robot_msgs::JointState& state, const ros::Time& time, const ros::Duration& dt) = 0;

    /* called after last call to update */
    virtual void stop(robot_msgs::JointState& state, const ros::Time& time) {}

protected:
    StateBaseConstPtr src;
    StateBasePtr dst;

private:
    std::string name;
    int src_idx, dst_idx;
    bool active;
    bool set;
};

typedef std::shared_ptr<TransitionBase> TransitionBasePtr;
typedef std::shared_ptr<const TransitionBase> TransitionBaseConstPtr;

}

#endif