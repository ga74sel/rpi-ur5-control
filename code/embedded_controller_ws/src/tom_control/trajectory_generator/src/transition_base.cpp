
#include "trajectory_generator/transition_base.h"
#include "trajectory_generator/state_base.h"

using namespace trajectory_generator;

/* init transition */
bool TransitionBase::init_request(ros::NodeHandle& nh)
{
    src_idx = src->get_id();
    dst_idx = dst->get_id();

    ROS_INFO_STREAM(name << " src = " << src_idx << " dst = " << dst_idx);

    return init(nh);
}