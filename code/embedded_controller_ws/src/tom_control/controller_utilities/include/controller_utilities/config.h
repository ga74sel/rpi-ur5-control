/**
 * @file controller_utilities.h
 *
 * @brief configuration file of framework
 *
 * @ingroup controller_utilities
 *
 */

#ifndef CONFIG_H_
#define CONFIG_H_

#include <Eigen/Dense>
#include <Eigen/Geometry>

/**
 * Configuration of the framework
 *
 * Specify the precision with scalar_t
 * Specify the number of DOF with NJOINT
 * 
 */
namespace config
{
    // number of robot joints
    const int NJOINT = 6;

    // scalar type
    typedef float scalar_t;

    // Eigen::Vectors
    typedef Eigen::Matrix<scalar_t, 7, 1> Vector7;
    typedef Eigen::Matrix<scalar_t, 6, 1> Vector6;
    typedef Eigen::Matrix<scalar_t, 6, 1> Vector5;
    typedef Eigen::Matrix<scalar_t, 4, 1> Vector4;
    typedef Eigen::Matrix<scalar_t, 3, 1> Vector3;

    // Eigen::Matrices
    typedef Eigen::Matrix<scalar_t, 7, 7> Matrix7;
    typedef Eigen::Matrix<scalar_t, 6, 6> Matrix6;
    typedef Eigen::Matrix<scalar_t, 6, 5> Matrix5;
    typedef Eigen::Matrix<scalar_t, 4, 4> Matrix4;
    typedef Eigen::Matrix<scalar_t, 3, 3> Matrix3;

    // Quaterionion
    typedef Eigen::Quaternion<scalar_t> Quaternion;

    // Robot specific vectors
    typedef Eigen::Matrix<scalar_t, NJOINT, 1> VectorDOF;
    typedef Eigen::Matrix<scalar_t, NJOINT, NJOINT> MatrixDOF;

    // Dynamic types
    // typedef Eigen::Matrix<scalar_t, Eigen::Dynamic, 1> VectorX;
    // typedef Eigen::Matrix<scalar_t, Eigen::Dynamic, Eigen::Dynamic> MatrixX;
    typedef Eigen::Matrix<scalar_t, 45, 1> VectorX;
    typedef Eigen::Matrix<scalar_t, 6, 45> MatrixX;

}

#endif
