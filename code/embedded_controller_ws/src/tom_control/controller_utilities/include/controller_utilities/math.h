/**
 * @file math.h
 *
 * @brief helper functions for math
 *
 * @ingroup controller_utilities
 *
 */

#ifndef MATH_H_
#define MATH_H_

#include "config.h"

namespace math
{
    //config::scalar_t eps = std::numeric_limits<scalar_t>::epsilon();
    static const config::scalar_t eps = config::scalar_t(1e-5);
    static const config::scalar_t one = config::scalar_t(1) - eps;

    /** 
    * @brief numeric integration of angular velocity w
    * q_n+1 = qr * qn
    * qr = [ cos(||w||*dt/2), sin(||w||*dt/2) * w / ||w||]
    * 
    * @param quaternion
    * @param angular velocity
    * @param integration setp size
    */
    inline config::Quaternion integ_quad(const Eigen::Quaternion<config::scalar_t>& q, 
                                             const config::Vector3& w,
                                             config::scalar_t dt) {
        typedef config::scalar_t scalar_t;

        scalar_t w_norm = w.norm();
        if( w_norm <= eps ) {
            return q;
        }

        scalar_t arg = scalar_t(0.5) * w_norm * dt;
        config::Quaternion qr;
        qr.coeffs() << sin(arg) / w_norm * w, cos(arg);

        return qr * q;
    }

    /** 
    * @brief spherical linear interpolation - slerp
    * Smoothly interploate between two quaternions
    * 
    * @param quaternion 1
    * @param quaternion 2
    * @param quaternion result
    * @param parametric value \em [0, 1]
    */
    inline void slerp(const Eigen::Quaternion<config::scalar_t>& q1,
                      const Eigen::Quaternion<config::scalar_t>& q2,
                      Eigen::Quaternion<config::scalar_t>& q,
                      config::scalar_t s)
    {
        typedef config::scalar_t scalar_t;

        scalar_t d = q1.dot(q2);
        scalar_t abs_d = std::abs(d);
        if( abs_d >= one ) {
            q.coeffs() = q1.coeffs();
            return;
        }

        scalar_t theta = std::acos(abs_d);
        scalar_t sin_theta = std::sin(theta);

        scalar_t scale0 = std::sin( (scalar_t(1) - s) * theta ) / sin_theta;
        scalar_t scale1 = std::sin( (s * theta) ) / sin_theta;
        if( d < 0 ) {
            scale1 = -scale1; // take the shortest path
        }
        q.coeffs() = scale0 * q1.coeffs() + scale1 * q2.coeffs();
    }

    /**
    * @brief spherical linear interpolation - slerp
    * Derivative of interpolation with respect to s: qp = d/ds q
    * 
    * @param quaternion 1
    * @param quaternion 2
    * @param vector4 of quaterion derivative (not a unit quad)
    * @param time [0, 1]
    */
    inline void slerp_diff(const Eigen::Quaternion<config::scalar_t>& q1,
                           const Eigen::Quaternion<config::scalar_t>& q2,
                           config::Vector4& qp,
                           config::scalar_t s)
    {
        typedef config::scalar_t scalar_t;

        scalar_t d = q1.dot(q2);
        scalar_t abs_d = std::abs(d);
        if( abs_d >= one ) {
            qp.setZero();
            return;
        }

        scalar_t theta = std::acos(abs_d);
        scalar_t sin_theta = std::sin(theta);

        scalar_t scale0 = -theta * std::cos( (scalar_t(1) - s) * theta ) / sin_theta;
        scalar_t scale1 = theta * std::cos( s*theta ) / sin_theta;

        if( d < 0 ) {
            scale1 = -scale1; // take the shortest path
        }
        qp = scale0 * q1.coeffs() + scale1 * q2.coeffs(); // qp = [x,y,z,w]
    }

    /**
    * @brief compute angular velocity omega based on given quaternion and quaternion derivative
    * quaternion propagation: w = 2 * quad_multip( d/dt q, q^* )
    * 
    * @param quaternion
    * @param quaterion time derivative: dtq = d/dt q
    * @param angular velocity
    */
    inline void quad_omega(const Eigen::Quaternion<config::scalar_t>& q,
                           const config::Vector4& dtq,
                           config::Vector3& w)
    {
        Eigen::Matrix<config::scalar_t, 3, 4> H;
        Eigen::Matrix<config::scalar_t, 4, 1> dtq_;

        auto& q_ = q.coeffs(); // q_ = [x,y,z,w]

        H << -q_(0), q_(3), -q_(2), q_(1), -q_(1), q_(2), q_(3), -q_(0), -q_(2), -q_(1), q_(0), q_(3);
        dtq_ << dtq[3], dtq[0], dtq[1], dtq[2];
        w = config::scalar_t(2.0) * H * dtq_;
    }

    /**
    * @brief hat operator
    * @param v Vector3x1
    * @return hat[v] Matrix3x3
    */
    inline config::Matrix3 hat(const config::Vector3& t) 
    {
        config::Matrix3 S;
        S << 0, -t(2), t(1), t(2), 0, -t(0), -t(1), t(0), 0;
        return S;
    }

    /**
    * @brief convert from angle-axis representation to rotation matrix
    * @param rotreguiz vector Vector3x1
    * @return rotation Matrix3x3
    */
    inline config::Matrix3 angleAxisToRotation(const config::Vector3& v)
    {
        static config::Matrix3 I = config::Matrix3::Identity();
        config::Matrix3 R;

        config::scalar_t theta = v.norm();
        if( theta )
            R = Eigen::AngleAxis<config::scalar_t>( theta, v / theta );
        else
            R << I;
        return R;
    }

    /**
    * @brief transform a point based on given homogeneous transformation
    * @param transformation matrix
    * @param point X
    */
    inline void transf_pt(const config::Matrix4& T, config::Vector3& X)
    {
        X = T.topLeftCorner(3,3) * X + T.topRightCorner(3, 1);
    }

    /**
    * @brief tranform a rotation matrix based on given homogeneous transformation
    * @param transformation matrix
    * @param rotation matrix
    */
    inline void transf_rot(const config::Matrix4& T, config::Matrix3& R)
    {
        R = T.topLeftCorner(3,3) * R;
    }

    /**
    * @brief tranform a vector based on given homogeneous transformation
    * @param transformation matrix
    * @param vector v
    */
    inline void transf_vec(const config::Matrix4& T, config::Vector3& v)
    {
        v = T.topLeftCorner(3,3) * v;
    }
}

#endif
