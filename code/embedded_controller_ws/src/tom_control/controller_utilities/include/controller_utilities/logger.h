/**
 * @file controller_utilities.h
 *
 * @brief helper functions to load parameters
 *
 * @ingroup controller_utilities
 *
 */

#ifndef LOGGER_H_
#define LOGGER_H_

#include <stdint.h>
#include <vector>
#include <string>

/**
 * Simple Logger to capture data statistics
 *
 * takes samples with update function and create a histogram via print_hist
 * 
 */
class Logger
{
public:
    Logger(uint64_t hist_size = 100000);
    virtual ~Logger();

    void set_outofsync(int outofsync_cnt);

    /* update statistics */
    void update(uint64_t dt);

    /* print */
    void print_hist(const std::string& file);
    void print_stat();

private:
    uint64_t size;
    std::vector<int> hist;
    std::vector<uint64_t> outliers;

    uint64_t max;
    uint64_t min;
    double avg;
    int cnt;
    int outofsync_cnt;
    int hist_overflow_cnt;

    int mask;
};

#endif