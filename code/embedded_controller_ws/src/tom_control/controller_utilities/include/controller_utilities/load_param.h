/**
 * @file load_param.h
 *
 * @brief helper functions to load parameters
 *
 * @ingroup controller_utilities
 *
 */

#ifndef LOAD_PARAM_H_
#define LOAD_PARAM_H_

#include <ros/ros.h>
#include <string>
#include <vector>

#include "config.h"

namespace load_param
{
    /** 
    * @brief load single parameter
    *
    * @param namespace name
    * @param parameter name
    * @param value
    * @param err_str
    */
    inline bool load_param(const std::string& ns, const std::string& name, double& val, std::string& err_str)
    {
        std::stringstream ss;

        if(!ros::param::has(ns)) {
            err_str = "retrive_param: no namespace= " + ns;
            return false;
        }
        ss << ns << "/" << name;
        
        ros::param::get(ss.str(), val);
        return true;
    }

    /** 
    * @brief load single parameter
    *
    * @param namespace name
    * @param parameter name
    * @param value
    * @param err_str
    */
    inline bool load_param(const std::string& ns, const std::string& name, float& val, std::string& err_str)
    {
        std::stringstream ss;

        if(!ros::param::has(ns)) {
            err_str = "retrive_param: no namespace= " + ns;
            return false;
        }
        ss << ns << "/" << name;

        ros::param::get(ss.str(), val);
        return true;
    }

    /** 
    * @brief load single parameter
    *
    * @param namespace name
    * @param parameter name
    * @param value
    * @param err_str
    */
    inline bool load_param(const std::string& ns, const std::string& name, std::string& val, std::string& err_str)
    {
        std::stringstream ss;

        if(!ros::param::has(ns)) {
            err_str = "retrive_param: no namespace= " + ns;
            return false;
        }
        ss << ns << "/" << name;
        
        ros::param::get(ss.str(), val);
        return true;
    }

    /** 
    * @brief load parameter vector
    *
    * @param namespace name
    * @param parameter name
    * @param value vector
    * @param err_str
    */
    inline bool load_param_vec(const std::string& ns, const std::string& name, std::vector<double>& vec, size_t dim, std::string& err_str)
    {
        std::stringstream ss;

        if(!ros::param::has(ns)) {
            err_str = "retrive_param: no namespace= " + ns;
            return false;
        }
        ss << ns << "/" << name;
        ros::param::get(ss.str(), vec);
        if( vec.size() != dim ) {
            err_str = "retrive_param: wrong dimension of " + name;
            return false;
        }
        return true;
    }

    /** 
    * @brief load parameter vector
    *
    * @param namespace name
    * @param parameter name
    * @param value vector
    * @param err_str
    */
    inline bool load_param_vec(const std::string& ns, const std::string& name, std::vector<std::string>& vec, size_t dim, std::string& err_str)
    {
        std::stringstream ss;

        if(!ros::param::has(ns)) {
            err_str = "retrive_param: no namespace= " + ns;
            return false;
        }
        ss << ns << "/" << name;
        ros::param::get(ss.str(), vec);
        if( vec.size() != dim ) {
            err_str = "retrive_param: wrong dimension of " + name;
            return false;
        }
        return true;
    }

    /** 
    * @brief load a transformation based on rotation R, translation t
    *
    * @param namespace name
    * @param parameter rotation name
    * @param parameter translation name
    * @param transfomation matrix
    * @param err_str
    */
    inline bool load_transf_rot(const std::string& ns, const std::string& r_name, const std::string& t_name, config::Matrix4& H, std::string& err_str)
    {
        std::vector<double> r_vec, t_vec;
        config::Matrix3 R;
        config::Vector3 t;

        if( !load_param_vec(ns, r_name, r_vec, 9, err_str) )
            return false;
        if( !load_param_vec(ns, t_name, t_vec, 3, err_str) )
            return false;

        config::scalar_t* ptr = R.data();
        for( size_t i = 0; i < r_vec.size(); ++i) {
            *(ptr++) = (config::scalar_t)r_vec[i];
        }

        ptr = t.data();
        for( size_t i = 0; i < t_vec.size(); ++i) {
            *(ptr++) = (config::scalar_t)t_vec[i];
        }

        H << R, t, 0, 0, 0, 1;
        return true;
    }

    /** 
    * @brief load a transformation based on quaternion q, translation t
    *
    * @param namespace name
    * @param parameter quaternion name
    * @param parameter translation name
    * @param transfomation matrix
    * @param err_str
    */
    inline bool load_transf_quad(const std::string& ns, const std::string& q_name, const std::string& t_name, config::Matrix4& H, std::string& err_str)
    {
        std::vector<double> q_vec, t_vec;
        config::Quaternion q;
        config::Matrix3 R;
        config::Vector3 t;

        if( !load_param_vec(ns, q_name, q_vec, 4, err_str) )
            return false;
        if( !load_param_vec(ns, t_name, t_vec, 3, err_str) )
            return false;

        q = config::Quaternion(q_vec[0], q_vec[1], q_vec[2], q_vec[3]); // w xyz

        config::scalar_t* ptr = t.data();
        for( size_t i = 0; i < t_vec.size(); ++i) {
            *(ptr++) = (config::scalar_t)t_vec[i];
        }

        R = q.toRotationMatrix();
        H << R, t, 0, 0, 0, 1;
        return true;
    }
}

#endif
