#include "controller_utilities/reference.h"

using namespace config;
using namespace reference;
using namespace controller_msgs;

template <typename Derived>
Reference<Derived>::Reference()
    : Kp(DerivedMatrix::Identity()),
      Ki(DerivedMatrix::Zero()),
      iE(DerivedVector::Zero()),
      is_antiwindup(false),
      is_ki_set(false)
{
}

template <typename Derived>
Reference<Derived>::Reference(const DerivedMatrix& Kp, const DerivedMatrix& Ki )
    : Kp(Kp),
      Ki(Ki),
      iE(DerivedVector::Zero()),
      is_antiwindup(false),
      is_ki_set(false)
{
}

template <typename Derived>
Reference<Derived>::~Reference()
{
}

/* set proportional gain */
template <typename Derived>
void Reference<Derived>::set_Kp(const DerivedMatrix& Kp)
{
    this->Kp = Kp;
}

template <typename Derived>
void Reference<Derived>::set_Kp(const DerivedScalar& kp)
{
    this->Kp = kp * DerivedMatrix::Identity();
}

/* set integral gain */
template <typename Derived>
void Reference<Derived>::set_Ki(const DerivedMatrix& Ki)
{
    if( !(Ki.diagonal().array() == DerivedScalar(0.0)).any() )
        is_ki_set = true;
    this->Ki = Ki;
}

template <typename Derived>
void Reference<Derived>::set_Ki(const DerivedScalar& ki)
{
    if( ki != DerivedScalar(0.0) )
        is_ki_set = true;
    this->Ki = ki * DerivedMatrix::Identity();
}

/* set limits for integral part */
template <typename Derived>
void Reference<Derived>::set_limits(const DerivedScalar& i_min, const DerivedScalar& i_max )
{
    if( i_min > i_max )
        return;
    this->i_min = i_min * DerivedVector::Ones();
    this->i_max = i_max * DerivedVector::Ones();

    // normalized by i gain
    this->i_min_n = this->i_min.array() / Ki.diagonal().array();
    this->i_max_n = this->i_max.array() / Ki.diagonal().array();
    is_antiwindup = true;
}

template <typename Derived>
void Reference<Derived>::set_limits(const DerivedVector& i_min, const DerivedVector& i_max ) 
{
    if( (i_min.array() > i_max.array()).any() )
        return;
    this->i_min = i_min;
    this->i_max = i_max;

    // normalized by i gain
    this->i_min_n = this->i_min.array() / Ki.diagonal().array();
    this->i_max_n = this->i_max.array() / Ki.diagonal().array();
    is_antiwindup = true;       
}

/* reset integral part */
template <typename Derived>
void Reference<Derived>::reset_integral()
{
    iE.setZero();
}

/* compute reference<Derived> values */
template <typename Derived>
Eigen::Matrix<typename Derived::Scalar, Derived::RowsAtCompileTime, 1>
Reference<Derived>::refp(const DerivedVector& Dp, const DerivedVector& E, const ros::Duration& dt, controller_msgs::Mode mode)
{
    DerivedVector Xrp;

    switch(mode)
    {
        case BREAK:
            Xrp.setZero();
            break;
        case PD:
            Xrp = Dp - Kp*E;
            break;
        case PID:
            iE += dt.toSec() * E;
            // limit the integrated error by +-limits / Ki
            if( is_antiwindup && is_ki_set) {
                iE = iE.array().min( i_max_n.array() ).max( i_min_n.array() );
            }
            // limit the contribution of i part by output by +-limits
            if( is_antiwindup )
                Xrp = Dp - Kp*E - (Ki * iE).array().min( i_max.array() ).max( i_min.array() ).matrix();
            else
                Xrp = Dp - Kp*E - Ki*iE;
            break;
    }

    return Xrp;
}

/* compute reference<Derived> derivative */
template <typename Derived>
Eigen::Matrix<typename Derived::Scalar, Derived::RowsAtCompileTime, 1>
Reference<Derived>::refpp(const DerivedVector& Dpp, const DerivedVector& E, const DerivedVector& Ep, const ros::Duration& dt, controller_msgs::Mode mode)
{
    DerivedVector Xrpp;

    switch(mode)
    {
        case BREAK:
            Xrpp.setZero();
            break;
        case PD:
            Xrpp = Dpp - Kp*Ep;
            break;
        case PID:
            Xrpp = Dpp - Kp*Ep - Ki*E;
            break;
    }

    return Xrpp;
}

// Explicit template instantiation
template class reference::Reference<config::VectorDOF>;
template class reference::Reference<config::Vector3>;