# Matlab Code

## Generate new model
See [paper](documentation/paper/regressor_formulation.pdf) for details.
```
generate_model/generate_ur5_model.m
```

## Export model to C++
```
generate_cpp/export_ur5_model.m
```

## Example model
Pre generated C++ code for `UR5` robot can be found in folder model
