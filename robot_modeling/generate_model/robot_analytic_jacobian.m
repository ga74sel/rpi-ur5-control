function [JA, JAp] = robot_analytic_jacobian(J0, varargin)
% Compute the analytical jacobian based on geometric jacobian
% Important: Input Arg are Symbolic!
%
% Input:
%   J0: Matrix 6x6 contraining the geometric jacobian
%
% Output:
%   JA: Matrix 6x6 containing the analytical jacobian
%   JA: Matrix 6x6 containing the time derivative analytical jacobian
%

    %% Parse Input
    p = inputParser;
    addRequired(p, 'J0');
    addOptional(p, 'euler', 'zyz');
    parse(p, J0, varargin{:});
    
    euler = p.Results.euler;
    
    % check dimension
    [n, m] = size( J0 );
    JA = sym(zeros(n, m), 'r' );
    JAp = sym( zeros(n, m), 'r' );
    
    if( n ~= 6 && m ~= 6 )
        fprintf('robot_analytic_jacobian: error wrong input dimensions\n');
        return
    end
    
    % init variables
    e = sym('e%d', [3,1], 'real');
    ep = sym('e%dp', [3,1], 'real');
    q = sym('q%d', [m,1], 'real');
    qp = sym('q%dp', [m,1], 'real');
    
    % build matrix based on parameterization
    E = sym(zeros(n, m));
    
    if( strcmp(euler, 'zyz') )
        E(1:3, 1:3) = sym(eye(3,3));
        E(4:6, 4:6) = [-cos(e(2))*cos(e(1))/sin(e(2)) -cos(e(2))*sin(e(1)) 1;...
                       -sin(e(1)), cos(e(1)), 0;...
                       cos(e(1))/sin(e(2)), sin(e(1))/sin(e(2)) 0];
    else
        fprintf('robot_analytic_jacobian: wrong euler convention\n');
        return
    end
    
    JA = E * J0;
    
    % take the derivative
    for i = 1 : n
        for j = 1 : m
            % take the derivative with respect to every joint
            for k = 1 : size(q, 1)
                JAp(i,j) = JAp(i,j) + diff(JA(i,j),q(k))*qp(k);
            end
            % take the derivative with respect to every euler angle
            for k = 1 : size(ep, 1)
                JAp(i,j) = JAp(i,j) + diff(JA(i,j),e(k))*ep(k);
            end
        end
    end
    
    simplify( JA );
    simplify( JAp );
end

