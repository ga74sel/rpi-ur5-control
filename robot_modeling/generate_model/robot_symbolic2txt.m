function robot_symbolic2txt(M, names, file_name, mode)
%%  matrix2txt(M, names, file_name)
%   writes symbolic matrices to a txt file 'file_name'
%
%   M       symbolic matrices [ n x m ] or [ n x m x N ]
%   names   <char>  or cell array [ 1 x N ]
%   mode    'w': write  'a': append

if iscell(names)
    N = length(names);
else
    N = 1;
end

fp = fopen(file_name, mode);         % create file

for i = 1:N
    
    if iscell(names)
        txt = char(M(:,:,i));
        var_len = length(names{i});
    else
        txt = char(M);
        var_len = length(names);
    end
    
    lines = strsplit(txt(8:end-1), '],');
    spaces = repmat(' ', 1,var_len + 3);

    fprintf(fp, '\n');
    if iscell(names)
        fprintf(fp, '%s = %s]; ...\n', names{i}, lines{1});
    else
        if length(lines) == 1
            fprintf(fp, '%s = %s;\n', names, lines{1});
            break;
        else    
            fprintf(fp, '%s = %s]; ...\n', names, lines{1});
        end
    end
    
    for j = 2:length(lines)-1
        fprintf(fp, '%s%s]; ...\n',   spaces, lines{j});
    end
    fprintf(fp, '%s%s;\n',        spaces, lines{length(lines)});

end 

fclose(fp);         % close file
    
end