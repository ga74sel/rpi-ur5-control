function [Yr, th, Yr_stack, th_stack] = robot_regressor_iterative(H, J, varargin)
% Compute robot regressor iteratively for each joint base on joint
% homogeneous transformations and joint jacobians (not center of mass)
%
% Input:
%   H: 4x4xn Matrix containing all Absolute Joint Transformations
%   J: 6xnxn Matrix containing all Joint Jacobians 6xn
%   th:lx1xn Matrix containing all theta descriptions
%
% Optional:
%   axis: 1xn string containing the axis of translation
%         only used if theta descriptions is 6 dim vector
%
% Ouput:
%   Yr: nxk Matrix that contains the endeffector regressor 
%   th: kx1 Matrix that contains the vector of unknowns
%   Yr_stack: nx1 Cell that contains regressors of all individual links
%   th_stack: nx1 Cell that contains unkonwns of all individual links
%

%% Parse Input

% number of cm + for loadmass
n = size(J, 3);

% default values
g = [0, 0, sym('gz')];
axis = repmat(' ', n, 1);

p = inputParser;
addRequired(p, 'H');
addRequired(p, 'J');
addOptional(p, 'g', g);
addOptional(p, 'th_description', 0);
addOptional(p, 'axis', axis);
parse(p, H, J, varargin{:});

% get values
th_description = p.Results.th_description;
g = p.Results.g;
axis = p.Results.axis;
    
%% For each Joint compute the sub-Regressor

% 1. create stack of joint regressors
Yr_stack = cell(n, 1);
th_stack = cell(n, 1);
Yr = [];
th = [];

for i = 1 : n
    [Yr_i, th_i] = regressor( H(:,:,i), J(:,:,i), th_description{i}, g, axis(i));
    
    fprintf('joint %d\n', i);
    
    % stack regressors together
    Yr = [Yr, Yr_i];
    th = [th; th_i];
    
    Yr_stack{i} = Yr;
    th_stack{i} = th;
end

end

function [Yr, th] = regressor( H, J, th, g, axis)
    n = length(th);
    
    % 1. check which case applies based on given describtion of th
    if n == 16
        % complete 10 dof
        Yr = regressor_full( H, J, g);
    elseif n == 9
        % cm is translated along a single axis only
        % cm has regular shape ineria matirx
        Yr = regressor_singe_axis( H, J, g, axis);
    elseif n == 3
        % cm is tranlated along a single axis only
        % cm is described as a point mass
        Yr = regressor_point_mass_singe_axis( H, J, g, axis);
    end
        
    % 2. search for zero columns or theta element to reduce Yr
    drop = unique( [find(sum(abs(Yr)) == 0) find(th == 0)'] );
    Yr(:, drop) = [];
    th(drop) = [];
end

function Yr = regressor_full( H, J, g)
% regressor of joint
% General case:
%   - geometry: I = diag(Ixx, Ixy, Ixz, Iyy Iyz, Izz)
%   - CM translated by p = [px, py, pz]

    % get parameters
    n = size(J, 2);
    Jv = J(1:3, :);
    Jw = J(4:6, :);
    R = H(1:3, 1:3);
    
    % define syms
    Q = sym('q%d', [n,1], 'real');
    Qp = sym('q%dp', [n,1], 'real');
    Qrp = sym('qr%dp', [n,1], 'real');
    Qrpp = sym('qr%dpp', [n,1], 'real');
    
    % W1(q, qp)
    w = R' * Jw * Qp; % 3x1
    Z = Jw' * R; % 2x3
    C = [w(2) w(3) 0; w(1) 0 w(3); 0 w(1) w(2)]; % 3x3
    N = Jw'*Jw * Qp; % 2x1
  
    D = [w(1)*Z(:,1)-N w(2)*Z(:,2)-N w(3)*Z(:,3)-N Z*C]; % 2x6
    
    W1 = [ Jv'*Jv*Qp, ...
           (cross_m(Jv, Jw*Qp) + cross_m(Jv*Qp, Jw))' * R, ...
           -D ];
    
    % W2(q, qp)
    d = R'*Jw*Qp;
    B = [diag(d) [d(2) d(3) 0; d(1) 0 d(3); 0 d(1) d(2)]];
    W2 = Jw' * R * B;
    
    % Y1(q, qp)
    Dv = sym(zeros(3, n), 'r');
    Dw = sym(zeros(3, n), 'r');
    for i = 1 : n
        Dv(:,i) = diff(Jv,Q(i)) * Qrp; % use this to introduce Qr
        Dw(:,i) = diff(Jw,Q(i)) * Qrp;
    end
    Y11 = -Dv'*Jv*Qp;
    
    t = cross(Jv * Qp, Jw * Qrp);
    S = sym(zeros(3, n), 'r');
    for i = 1 : n
       S(:, i) = diff(R', Q(i)) * t;
    end
    Y12 = -( cross_m(Dv, Jw*Qp) + cross_m(Jv*Qp, Dw) )' * R - S';
    
    t = Jw*Qp;
    Y13 = sym(zeros(n, n), 'r');
    for i = 1 : n
        Y13(:,i) = diff(Jw', Q(i)) * t;
    end
    Y13 = -Y13' * Qrp % use this to introduce Qr
    
    t1 = R' * Jw * Qp*Qrp';
    t2 = ( Qp' * Jw' * Jw * Qrp * eye(3) - Jw * Qp*Qrp' * Jw')*R;
    E = sym( zeros(n, 9), 'r'); % matrix with stacked E matrices
    for i = 1 : n
        E_ = t1 * diff(Jw', Q(i)) * R - diff(R', Q(i)) * t2;
        E(i,:) = reshape(E_,[1, 9]);
    end
    K = [[1:6] == 1;...
         [1:6] == 4;...
         [1:6] == 5;...
         [1:6] == 4;...
         [1:6] == 2;...
         [1:6] == 6;...
         [1:6] == 5;...
         [1:6] == 6;...
         [1:6] == 3];
    Y14 = E * K;

    % Y2(q, qp)    
    d = R'*Jw*Qrp;
    B = [diag(d) [d(2) d(3) 0; d(1) 0 d(3); 0 d(1) d(2)]];
    Y2 = sym(zeros(3, n), 'r');
    for i = 1 : n
        Y2(:,i) = diff(R' * Jw, Q(i)) * Qp;
    end
    Y2 = -Y2' * B;

    % Y2(q, qp)   
    d = R'*Jw*Qrp;
    B = [diag(d) [d(2) d(3) 0; d(1) 0 d(3); 0 d(1) d(2)]];
    Y2 = sym(zeros(3, n));
    for i = 1 : n
        Y2(:,i) = diff(R' * Jw, Q(i)) * Qp;
    end
    Y2_a = -Y2' * B;
    
    d = R'*Jw*Qp;
    B = [diag(d) [d(2) d(3) 0; d(1) 0 d(3); 0 d(1) d(2)]];
    Y2 = sym(zeros(3, n));
    for i = 1 : n
        Y2(:,i) = diff(R' * Jw, Q(i)) * Qrp;
    end
    Y2_b = -Y2' * B; 
    Y2 = (Y2_a + Y2_b) / 2;

    % Y3(q)
    Y31 = Jv'*g; % removed minus
    
    Y32 = -( cross_m(Jw, g)' * R );
    
    Y1 = [Y11 + Y31 Y12 + Y32 Y14]; % Y13 has wrong dimension, remove it for testing
    
    % Y
    W1p = time_diff_m(W1, Q, Qp, Qrp, Qrpp);
    W2p = time_diff_m_combined(W2, Q, Qp, Qrp, Qrpp); 
    
    Yr = [ W1p+Y1 W2p+Y2 ];
    simplify(Yr);
end

function Yr = regressor_singe_axis( H, J, g, axis)
% regressor of joint
% Special case:
%   - regular geometry: I = diag(Ixx, Iyy, Izz)
%   - CM lies on x or z axis: p = [px, 0, 0] / p = [0, 0, px]

    % get parameters
    n = size(J, 2);
    Jv = J(1:3, :);
    Jw = J(4:6, :);
    R = H(1:3, 1:3);
    
    r = R(:, axis-'x' + 1); % extract colum 1 if x, 2 if y, 3 if z
    
    % define syms
    Q = sym('q%d', [n,1], 'real');
    Qp = sym('q%dp', [n,1], 'real');
    Qrp = sym('qr%dp', [n,1], 'real');
    Qrpp = sym('qr%dpp', [n,1], 'real');
    
    % W1(q, qp)
    b = sym(zeros(n, 1), 'r');
    for i = 1 : n
        b(i) = r' * Jw(:,i) * Qp' * Jw' * r;
    end

    W1 = [ Jv'*Jv*Qp, ...
           (cross_m(Jv, Jw*Qp) + cross_m(Jv*Qp, Jw))' * r, ...
           Jw'*Jw*Qp-b ];

    % W2(q, qp)
    d = R'*Jw*Qp;
    B = [diag(d) [d(2) d(3) 0; d(1) 0 d(3); 0 d(1) d(2)]];
    W2 = Jw' * R * B;

    % Y1(q, qp)
    Dv = sym(zeros(3, n), 'r');
    Dw = sym(zeros(3, n), 'r');
    for i = 1 : n
        Dv(:,i) = diff(Jv,Q(i)) * Qrp; % use this to introduce Qr
        Dw(:,i) = diff(Jw,Q(i)) * Qrp;
    end
    Y11 = -Dv'*Jv*Qp;

    t = Jw*Qp;
    Y13 = sym(zeros(n, n), 'r');
    for i = 1 : n
        Y13(:,i) = diff(Jw', Q(i)) * t;
    end
    Y13 = -Y13' * Qrp;

    t = cross(Jv * Qp, Jw * Qrp);
    s = sym(zeros(n, 1), 'r');
    for i = 1 : n
       s(i) = diff(r', Q(i)) * t;
    end
    y121 = -( cross_m(Dv, Jw*Qp) + cross_m(Jv*Qp, Dw) )' * r - s;

    t1 = r' * Jw * Qp * Qrp';
    t2 = (Qrp' * Jw' * Jw * Qp * eye(3) - Jw * Qp*Qrp' * Jw') * r;
    y141 = sym(zeros(n, 1), 'r');
    for i = 1 : n
        y141(i) = t1 * diff(Jw', Q(i)) * r - diff(r', Q(i)) * t2;
    end

    d = R'*Jw*Qrp;
    B = [diag(d) [d(2) d(3) 0; d(1) 0 d(3); 0 d(1) d(2)]];
    Y2 = sym(zeros(3, n));
    for i = 1 : n
        Y2(:,i) = diff(R' * Jw, Q(i)) * Qp;
    end
    Y2_a = -Y2' * B;
    
    d = R'*Jw*Qp;
    B = [diag(d) [d(2) d(3) 0; d(1) 0 d(3); 0 d(1) d(2)]];
    Y2 = sym(zeros(3, n));
    for i = 1 : n
        Y2(:,i) = diff(R' * Jw, Q(i)) * Qrp;
    end
    Y2_b = -Y2' * B; 
    Y2 = (Y2_a + Y2_b) / 2;
    
    % Y3(q)
    Y31 = Jv'*g;
    
    y321 = -( cross_m(Jw, g)' * r );
    
    Y1 = [Y11+Y31 y121+y321 Y13+y141];

    % Y
    W1p = time_diff_m(W1, Q, Qp, Qrp, Qrpp);
    W2p = time_diff_m_combined(W2, Q, Qp, Qrp, Qrpp);
   
    Yr = [ W1p+Y1 W2p+Y2 ];
    simplify(Yr);
end

function Yr = regressor_point_mass_singe_axis( H, J, g, axis)
% regressor of joint
% Special case:
%   - point mass I = diag(0 0 0)
%   - CM lies on x or z axis: p = [px, 0, 0] / p = [0, 0, px]

    % get parameters
    n = size(J, 2);
    Jv = J(1:3, :);
    Jw = J(4:6, :);
    R = H(1:3, 1:3);
    
    r = R(:, axis-'x' + 1); % extract colum 1 if x, 2 if y, 3 if z
    
    % define syms
    Q = sym('q%d', [n,1], 'real');
    Qp = sym('q%dp', [n,1], 'real');
    Qrp = sym('qr%dp', [n,1], 'real');
    Qrpp = sym('qr%dpp', [n,1], 'real');

    % W1(q, qp)
    b = sym(zeros(n, 1), 'r');
    for i = 1 : n
        b(i) = r' * Jw(:,i) * Qp' * Jw' * r;
    end

    W1 = [ Jv'*Jv*Qp, ...
           (cross_m(Jv, Jw*Qp) + cross_m(Jv*Qp, Jw))' * r, ...
           Jw'*Jw*Qp-b ];

     % Y1_(q, qp)
    Dv = sym(zeros(3, n), 'r');
    Dw = sym(zeros(3, n), 'r');
    for i = 1 : n
        Dv(:,i) = diff(Jv,Q(i)) * Qrp;
        Dw(:,i) = diff(Jw,Q(i)) * Qrp;
    end

    Y11 = -Dv'*Jv*Qp;
    Y31 = Jv'*g; % removed minus

    t = Jw*Qp;
    Y13 = sym(zeros(n, n), 'r');
    for i = 1 : n
        Y13(:,i) = diff(Jw', Q(i)) * t;
    end
    Y13 = -Y13' * Qrp; 

    t = cross(Jv * Qrp, Jw * Qp); % Where !!
    s = sym(zeros(n, 1), 'r');
    for i = 1 : n
       s(i) = diff(r', Q(i)) * t;
    end
    y121 = -( cross_m(Dv, Jw*Qp) + cross_m(Jv*Qp, Dw) )' * r - s;

    y321 = -( cross_m(Jw, g)' * r );

    t1 = r' * Jw * Qp*Qrp';
    t2 = ((Jw * Qp)' * (Jw * Qp) * eye(3) - Jw * Qrp*Qp' * Jw') * r;

    y141 = sym(zeros(n, 1), 'r');
    for i = 1 : n
        y141(i) = t1 * diff(Jw', Q(i)) * r - diff(r', Q(i)) * t2;
    end

    Y1 = [Y11+Y31 y121+y321 Y13+y141];

    % Y
    W1p = time_diff_m(W1, Q, Qp, Qrp, Qrpp);

    Yr = W1p+Y1;
    simplify(Yr);
end

% Helper fnc --------------------------------------------------------------

% Define cross puduct between matrix A and matrix/vector B
function C = cross_m(A, B)
    n_a = size(A, 2);
    n_b = size(B, 2);
    if n_a == n_b
        C = cross(A, B); 
    elseif(n_a > n_b)
        C = cross(A, repmat(B, 1, n_a));
    else
        C = cross(repmat(A, 1, n_b), B);
    end     
end

function Mp = time_diff_m_combined(M, Q, Qp, Qrp, Qrpp)
    [n, m] = size(M);
    Mp = sym(zeros(n, m), 'r');
    for i = 1 : n
        for j = 1 : m
            
            for k = 1 : n
                dm = diff(M(i, j), Qp(k));
                for l = 1 : n
                    Mp(i, j) = Mp(i, j) + 1/2 * diff(dm, Q(l)) * Qp(l) * Qrp(k);
                end
                Mp(i, j) = Mp(i, j) + 1/2 * diff(M(i,j), Q(k)) * Qrp(k) + dm * Qrpp(k);
            end
            
        end
    end
end

% Total Differential of Matrix M
function Mp = time_diff_m(M, Q, Qp, Qrp, Qrpp)
    [n, m] = size(M);
    Mp = sym(zeros(n, m), 'r');
    for i = 1 : n
        for j = 1 : m
            for k = 1 : size(Q, 1)
                Mp(i, j) = Mp(i, j) + diff(M(i,j), Q(k)) * Qrp(k) + diff(M(i, j), Qp(k)) * Qrpp(k);
            end
        end
    end
end

