function [M, C, G] = robot_DynamicModel(Jcm, Tcm, varargin)
% Compute robot dynamic model with iterative euler-lagange method using DH
% Important: 
%   Input Arg are Symbolic
%
% Input:
%   Jcm: 6xnxn Matrix containing all Jacobians 6xn of Center of Masses
%   Tcm: 4x4xn Matrix containing all Absolute CM Transformations
%   Optional: Inertia Matrix I, mass vector m, state vec q, state vel qp
%
% Output:
%   M: nxn Inertia matrix 
%   C: nxn Coriolis and Centripetal Matrix
%   G: nx1 gravitational torques
%
    % number of joints
    n_joints = size(Jcm, 2);
    n_cm = size(Jcm, 3);

    % gerate symbolic expressions for Inertias and Masses
    I = generateInetiaMatrix(n_cm, 'real');
    m = sym('m%d', [n_cm,1], 'real');
    % gerate symbolic expressions for joint varaibles and joint derivatives
    q = sym('q%d', [n_joints,1], 'real');
    qp = sym('q%dp', [n_joints,1], 'real');
    % generate gravitation vector 
    g = [sym('gx', 'real'); sym('gy', 'real'); sym('gz', 'real')];
    
    %% Parse Input
    p = inputParser;
    addRequired(p, 'Jcm');
    addRequired(p, 'Tcm');
    addOptional(p, 'I', I);
    addOptional(p, 'm', m);
    addOptional(p, 'q', q);
    addOptional(p, 'qp', qp);
    addOptional(p, 'g', g);
    parse(p, Jcm, Tcm, varargin{:});
    
    I = p.Results.I;
    m = p.Results.m;
    q = p.Results.q;
    qp = p.Results.qp;
    g = p.Results.g;
    
    %% Compute Vector of gravitational torques
    G = sym( zeros(n_joints,1), 'r');
    P = sym(0);

    for i = 1 : n_cm
        P = P + m(i)*g'*Tcm(1:3,4,i);
    end

    for k = 1 : n_joints
       G(k) = diff(P, q(k));
    end
    G = expand( G );
    G = simplify( G );

    %% Compute Inertia matrix M
    M = sym( zeros(n_joints, n_joints), 'r');

    for i = 1 : n_cm
        M = M + m(i)*Jcm(1:3,:,i)'*Jcm(1:3,:,i) + Jcm(4:6,:,i)'*Tcm(1:3,1:3,i)*I(:,:,i)*Tcm(1:3,1:3,i)'*Jcm(4:6,:,i);
    end
    M = expand( M );
    M = simplify( M );

    %% Compute Coriolis and Centripetal Matrix C
    C = sym( zeros(n_joints, n_joints), 'r');

    for k = 1 : n_joints
        for j = 1 : n_joints
            for i = 1 : n_joints
                C(k,j) = C(k,j) + ( diff(M(k,j), q(i)) + diff(M(k,i), q(j)) - diff(M(i,j), q(k)) )*qp(i);
            end
        end
    end
    C = expand( 1/2 * C );
    C = simplify( C );

    %% Evalutate properties of M
    symmetric_M = simplify( M - transpose(M) );
    if symmetric_M ~= sym(zeros(n_joints,n_joints))
        error('Matrix M is not symmetric');
    end

    %% Evalutate properties of Skew symmetric Matrix N = Mp - 2*C

    % Compute time derivative of Matrix M
    Mp = sym(zeros(n_joints, n_joints));
    for i = 1 : size(Mp,1)
        for j = 1 : size(Mp,2)
            for k = 1 : n_joints
                Mp(i,j) = Mp(i,j) + diff(M(i,j),q(k))*qp(k);
            end
        end
    end
    Mp = simplify( Mp );

    % Form skew symetric matrix
    N = simplify( Mp - 2.*C );

    % Check if N is skew symetric: n_ij = - n_ji
    skewsymmetric_N = simplify( N + transpose(N));
    if(skewsymmetric_N ~= sym( zeros(n_joints, n_joints) ) )
        error('Matrix N = Mp - 2*C is not skewsymmetric');
    end

end

function M = generateInetiaMatrix(n, set)
    M = sym( zeros(3,3,n), 'r');
    
    for k = 1 : n
        for i = 1 : 3 
            for j = i : 3
                M(i,j,k) = sym(sprintf('I%d%d%d', k, i, j), set);
                M(j,i,k) = sym(sprintf('I%d%d%d', k, i, j), set);
            end
        end
    end
end

