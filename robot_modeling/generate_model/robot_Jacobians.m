function [J, Jp] = robot_Jacobians(T, type, varargin)
% Compute all Jacobian Matices for Robot Kinematics
% Important: Input Arg are Symbolic!
%
% Input:
%   T_W: 3d Matrix containing all Absolute Joint Transformations in first
%   n/2 entires and all Absolute CM Transformations in next n/2 entires
%   type: String containing 'r' or 'p' to describe type of each joint
%   Tcm_W: 3d Matrix containing all Absolute CM Transformations
%   CM: True if T also contains information about Center of Masses
%   Derivative: If true time derivatives of J are also computed
%
% Output:
%   J_W: Matrix 6x4xn that contains all Jacobians of Joints
%   Jcm_W: Matrix 6x4xn that contains all Jacobians of CMs
%
    %% Parse Input
    p = inputParser;
    addRequired(p, 'T');
    addRequired(p, 'type');
    addOptional(p, 'Tcm', 0);
    addOptional(p, 'CM', true);
    addOptional(p, 'T0_W', sym(eye(4), 'r'));
    addOptional(p, 'Derivative', false);
    parse(p, T, type, varargin{:});

    Tcm = p.Results.Tcm;
    T0_W = p.Results.T0_W;
    CM = int32(p.Results.CM);
    Derivative = p.Results.Derivative;
    
    N_joints = length(type);
    N_links = size(T, 3);
    if(CM == 1)
        N_cm = size(Tcm, 3);
    else
        N_cm = 0;
    end 

    %% Compute all Jacobians for Joints
    J = sym( zeros(6, N_joints, N_links+N_cm), 'r');

    % Combine all Absolut Joint Transformation and World Transf
    % T0_W, T1_W, T2_W, ..., Tn_W
    
    T_cat = cat(3, T, Tcm);
    T_cat_I = cat(3, T0_W, T, Tcm);
    
    % for Joints (cm = 0) and CMs (cm = 1) do
    N_ = [N_links, N_cm];
    for cm = 0 : CM
        k = cm*N_links; % index
        
        % for every joint or CM do
        for i = 1 : N_(cm+1)
            % for every colum in Jacobian (Matrix i) do
            for j = 1 : min(N_joints, i)
                
                z = T_cat_I(1:3, 3, j);
                t_start = T_cat_I(1:3, 4, j);
                t_dest = T_cat(1:3, 4, k+i);

                if type(j) == 'r'
                    % Revolute Joint
                    J(:,j,k+i) = simplify( [cross(z, (t_dest - t_start)); z] );
                else
                    % Prismatic Joint
                    J(:,j,k+i) = simplify( [z; [0;0;0]] );
                end
                
            end
        end        
    end
    
    %% Compute time derivatives of Jacobian matrices
    if( Derivative == true )
        % gerate symbolic expressions for joint varaibles and joint derivatives
       	q = sym('q%d', [N_joints,1], 'real');
        qp = sym('q%dp', [N_joints,1], 'real');
        
        % Time derivatives
        Jp = sym( zeros(size(J)), 'r' );
        
        % for every joint or CM do
        for k = 1 : N_links+N_cm
            % for every element in current Jacobian do
            for i = 1 : 6
                for j = 1 : N_joints
                    % take the derivative with respect to every joint
                    for m = 1 : N_joints
                        Jp(i,j,k) = Jp(i,j,k) + diff(J(i,j,k),q(m))*qp(m);
                    end
                end
            end
        end
        Jp = simplify( Jp );
    end

end


