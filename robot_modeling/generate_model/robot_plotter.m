function robot_plotter(HT, Q, L, varargin)
% Plots Coordinate Frames (xyz-axis)
%
% Input:
%   HT: 4x4xn Matrix containing all Homogeneous Transformations
%   L: vector of length
%   Q: Configuration used for displaying
%   joint:  true, false
%   projection: true, false
%   text:   name of frames
%   color:  color of center points
%   length: length of frame axis
%
% Ouput:
%   -

    %% Parse Input
    p = inputParser;
    addRequired(p, 'HT');
    addRequired(p, 'Q');
    addRequired(p, 'L');
    addOptional(p, 'Q_val', zeros(size(Q)));
    addOptional(p, 'L_val', ones(size(L)));
    addOptional(p, 'joint', true);
    addOptional(p, 'projection', false);
    addOptional(p, 'text', 'O_');
    addOptional(p, 'color', 'b');
    addOptional(p, 'aLength', 0.06);
    parse(p, HT, Q, L, varargin{:});

    Q_val = p.Results.Q_val;
    L_val = p.Results.L_val;
    joint = p.Results.joint;
    projection = p.Results.projection;
    base_str = p.Results.text;
    color = p.Results.color;
    aLength = p.Results.aLength;

    %% Setup
    grid on
    hold on
    
    n = size(HT, 3);
    tOff = 0.005;
    
    axisX_0=[aLength;0;0;1];
    axisY_0=[0;aLength;0;1];
    axisZ_0=[0;0;aLength;1];
    
    % origins
    O = zeros(4, (n+1)*2);
    
    % axis lines
    axisX = zeros(4,(n+1)*2);
    axisY = zeros(4,(n+1)*2);
    axisZ = zeros(4,(n+1)*2);
    
    %% Compute axis and frame names
    
    % World center
    str = {'O_W'};

    axisX(:,2) = axisX_0;
    axisY(:,2) = axisY_0;
    axisZ(:,2) = axisZ_0;
    
    %% Substitude the symbolic equations
    HT = subs(HT, Q, Q_val);   % comfiguration 0
    HT = subs(HT, L, L_val);             % length 1
    
    % frames
    j = 3;
    for k = 1 : n
        str{end+1} = [base_str num2str(k-1)];
        
        O(:,j) = HT(:,4,k);
        O(:,j+1) = HT(:,4,k);
        
        axisX(:,j) = HT(:,4,k);
        axisY(:,j) = HT(:,4,k);
        axisZ(:,j) = HT(:,4,k);
        
        axisX(:,j+1) = HT(:,:,k)*axisX_0;
        axisY(:,j+1) = HT(:,:,k)*axisY_0;
        axisZ(:,j+1) = HT(:,:,k)*axisZ_0;
        
        j = j + 2;
    end
    
    %% Plot all coordinate frames at once
    hold on;
    plot3(reshape(axisX(1,:)',2,[]), reshape(axisX(2,:)',2,[]), reshape(axisX(3,:)',2,[]), 'r -', 'Linewidth',2)
    plot3(reshape(axisY(1,:)',2,[]), reshape(axisY(2,:)',2,[]), reshape(axisY(3,:)',2,[]), 'g -', 'Linewidth',2)
    plot3(reshape(axisZ(1,:)',2,[]), reshape(axisZ(2,:)',2,[]), reshape(axisZ(3,:)',2,[]), 'b -', 'Linewidth',2)
    text(O(1,1:2:end)'+tOff,O(2,1:2:end)'+tOff,O(3,1:2:end)'+tOff,str);
    
    %% Plot robot links
    linksX = reshape(O(1,4:end-1)',2,[]);
    linksY = reshape(O(2,4:end-1)',2,[]);
    linksZ = reshape(O(3,4:end-1)',2,[]);
    
    if(joint == true)
        plot3(linksX, linksY, linksZ,'k -', 'Linewidth',1.5)
    end
    
    %% Plot projection on xy plane
    if(projection == true)
        linksZ = zeros(size(linksZ));
        plot3(linksX, linksY, linksZ,'k --', 'Linewidth',1.5)
    end
    
    %% Plot Origins
    plot3(O(1,:), O(2,:), O(3,:), 'k .','MarkerSize',30, 'color', color);
    
end