function [Y, parameter] = robot_regessor(M, C, G, varargin)
% Forms a Regression Matrix based on dynamic model of robot
%
% Inputs:
%   M nxn Matrix 
%   C nxn Matrix
%   G nx1 Matrix
%   errorspace Flag that indicates if Joint error space is used
%   fullmodel Flag indicated if entire kinematic model should be used
%             (true: full model, false: only grafity)
%
% Ouputs:
%   Y nxr Regression matrix
%   parameter rx1 parameter vector
%
    %% Parse Input
    p = inputParser;
    addRequired(p, 'M');
    addRequired(p, 'C');
    addRequired(p, 'G');
    addOptional(p, 'errorspace', false);
    addOptional(p, 'fullmodel', true);
    parse(p, M, C, G, varargin{:});
    errorspace = p.Results.errorspace;
    fullmodel = p.Results.fullmodel;

    % save parameters here
    parameter = [];

    % number of joints
    n = size(M, 1);
    
    % generate symbolic expressions for joint varaibles and joint derivatives
    q = sym('q%d', [n,1], 'real');
    qp = sym('q%dp', [n,1], 'real');
    qpp = sym('q%dpp', [n,1], 'real');
    
    state_var = [q', qp', qpp'];
    
    % generate symbolic expressions for reference states if errorspace set
    if(errorspace == true)
    	qrp = sym('qr%dp', [n,1], 'real');
        qrpp = sym('qr%dpp', [n,1], 'real');
        state_var = [ state_var, qrp', qrpp'];
    end

    % 1. compute equation of motion
    % use gravity only if fullmodel set to false
    if( fullmodel == true)
        if( errorspace == true )
            equ = expand( M*qrpp + C*qrp + G );
        else
            equ = expand( M*qpp + C*qp + G );
        end
    else
        equ = expand( G );
    end
    
    % foreach center of mass
    for i = 1 : size(equ, 1)
        % filter empty equ
        if equ(i) == 0
            continue;
        end
        
        % extract child expressions ( expr1 + expr2 + expr3 + ... )
        expr = children(equ(i));
        
        % foreach child expression remove state variables
        for j = 1 : size(expr, 2)
            % remove q, qp, qpp entries and save remaining expresion in parameters
            cut_expr = remove_term( expr(j),  state_var );
            parameter = [parameter, cut_expr];
        end
        
    end
    
    % remove multible parameters
    parameter = unique(parameter);
    r = size(parameter, 2);

    % substitude parameter terms with single variable z#
    z = sym('z%d', [1,r], 'real');
    
    equ = subs(equ, parameter, z);

    % form regressor matrix
    Y = simplify( equationsToMatrix(equ, z) );
    parameter = parameter';
end

function [exp, rm_exp] = remove_term( exp,  varlist)
    subexp = children(exp);
    rm_exp = [];
    rm_idx = [];
    
    for i = 1 : size(subexp, 2)
        var = symvar(subexp(i));
        flag = false;
                
        if size(var) > 0
            for j = 1 : size(varlist,2)
                if var == varlist(j)
                    flag = true;
                    break;
                end
            end
        else
            flag = true;
        end
        
        if flag == true
            rm_exp = [rm_exp, subexp(i)];
            rm_idx = [rm_idx, i];
        end
    end
    
    subexp(rm_idx) = [];
    exp = prod(subexp);
end