%% Generate kinematic and dynamic equ of UR5 model
%   Uses the specified DH-Parameters to generate all kinematic and dynamic
%   equations. Generates a minimal form of the manipulator regressor
%

%% 1. Define all Symbols

% Set joint variables and link / center of mass length
syms q1 q2 q3 q4 q5 q6 L1 L2 L3 L4 L5 L6 L7 L8 real

% Set unknowns per link:
% Important: UR5 is special case, cm translation only in one axis
%   mass: mx
%   cm tranlation: Lx
%   inertia tensor: Ixx
syms m6 L12 I611 I622 I633 I612 I613 I623 real
syms m5 L11 I511 I522 I533 I512 I513 I523 real
syms m4 L10 I411 I422 I433 I412 I413 I423 real
syms m3 L9 I311 I322 I333 I312 I313 I323 real
syms m2 L8 I211 I222 I233 I212 I213 I223 real
syms m1 L7 I111 I122 I133 I112 I113 I123 real
syms gx gy gz real

% Combine into vectors, add real length to generate a plot of robot
Q = [q1 q2 q3 q4 q5 q6];
Q_val = [0, 0, 0, 0, 0, 0];
L = [L1 L2 L3 L4 L5 L6 L7 L8 L9 L10 L11 L12];
L_val = [0.089159 -0.42500 -0.39225 0.10915 0.09465 0.08230 ...
         0.045000 -0.21200 -0.19100 0.05450 0.04650 0.04100];
g = [gx; gy; gz];
T0_W = eye(4);

%% 2. Define the DH table
% first n entries are joint frames
% last n entries are cm frames ( wrt to joints )
DH = [q1 L1 0 pi/2;... 	% frame 1
      q2 0 L2 0;...     % frame 2
      q3 0 L3 0;...   	% frame 3
      q4 L4 0 pi/2;... 	% frame 4
      q5 L5 0 -pi/2;... % frame 5
      q6 L6 0 0;...     % frame 6
      q1 L7 0 0;... 	% cm 1
      q2 0 L8 0;...     % cm 2
      q3 0 L9 0;...   	% cm 3
      q4 L10 0 0;...    % cm 4
      q5 L11 0 0;...    % cm 5
      q6 L12 0 0        % cm 6
      ];

%% 3. Compute all Absolute Homogenous Tranformations for Joints and CMs
[Tjoint, Tcm] = robot_ForwardKinematic( DH, 'T0_W', T0_W, 'CM', true, 'convention', 'distal');

%% 4. Plot the robot frames 
figure
robot_plotter(cat(3, T0_W, Tjoint), Q, L, 'L_val', L_val, 'Q_val', Q_val);
hold on;
xlabel('x'), ylabel('y'), zlabel('z');
robot_plotter(Tcm, Q, L, 'joint', false, 'text', 'CM_', 'color', 'r', 'L_val', L_val, 'Q_val', Q_val);

%% 5. Compute all Jacobians of Joints
% UR5 has only revolute joints 'r'
[J, Jp] = robot_Jacobians(Tjoint, 'rrrrrr', 'T0_W', T0_W, 'CM', true, 'Tcm', Tcm, 'Derivative', true);
Jjoint = J(:,:,1:6);
Jcm = J(:,:,7:end);

%% 6. Compute analytical Jacobian for endeffector
[JA, JAp] = robot_analytic_jacobian(Jjoint(:,:,end), 'euler', 'zyz');

%% 7. Compute the dynamic model
[M, C, G] = robot_DynamicModel(Jcm, Tcm, 'g', g);
% [Y_ref, th_ref] = robot_regessor(M, C, G, 'errorspace', true);
% tau_ref = simplify( Y_ref * th_ref );

%% 8. Compute robot regressor per joint
% the regressor computation requires the transformations and jacobians of
% the center of mass at the starting point of each joint ( without the
% translation of of the center of mass). To get these transfomrations the
% cm lengthes in the DH table are removed and the kinematic is computed
% once again to obtain Tcm_ and Jcm_
DH_ = DH;
DH_(7:end, 2) = zeros(6,1); DH_(7:end, 3) = zeros(6,1);

[Tjoint_, Tcm_] = robot_ForwardKinematic( DH_, 'CM', true, 'convention', 'distal');
J_ = robot_Jacobians(Tjoint_, 'rrrrrr', 'CM', true, 'Tcm', Tcm_);
Jjoint_ = J_(:,:,1:6);
Jcm_ = J_(:,:,7:end);

%% 9. Compute the regressor
% the paramter vector per joint is allready knwon before computing the
% actual regressor eqations. Since the cm translation is only into one
% direction ( t_cm = [tx, 0, 0] or t_cm = [0, 0, tz] ), the parameter
% vector of each link has a dimension of nine
th6 = [m6; m6*L12; m6*L12*L12; I611; I622; I633; I612; I613; I623];            % z-axis
th5 = [m5; m5*L11; m5*L11*L11; I511; I522; I533; I512; I513; I523];            % z-axis
th4 = [m4; m4*L10; m4*L10*L10; I411; I422; I433; I412; I413; I423];            % z-axis
th3 = [m3; m3*L9; m3*L9*L9; I311; I322; I333; I312; I313; I323];               % x-axis
th2 = [m2; m2*L8; m2*L8*L8; I211; I222; I233; I212; I213; I223];               % x-axis
th1 = [m1; m1*L7; m1*L7*L7; I111; I122; I133; I112; I113; I123];               % z-axis

% the axis of the center of mass translation for each link
axis = 'zxxzzz';

% the regressor is computed from the cm transformations and jacobians
th_description = { th1, th2, th3, th4, th5, th6 };
[Y_it, th_it, Yr_stack, th_stack] = robot_regressor_iterative(Tcm_, Jcm_, 'th_description', th_description, 'g', g, 'axis', axis);
tau_it = simplify( Y_it * th_it );

% optional: compair the standard regressor with the iterative regressor
% n = 6;
% q = sym('q%d', [n,1], 'real');
% qp = sym('q%dp', [n,1], 'real');
% qpp = sym('q%dpp', [n,1], 'real');
% qrp = sym('qr%dp', [n,1], 'real');
% qrpp = sym('qr%dpp', [n,1], 'real');
% tau_ref = simplify(M*qrpp + C*qrp + G);
