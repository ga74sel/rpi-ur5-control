%% Export UR5 model to c++
%   Takes symbolic object generated before, substitudes variabels and
%   writes the results to a c++ header file
%
%   Change the settings to match dof, c++ types, include header
%

%% settings
dof = 6;
scalar_type = 'scalar_t';
namespace = 'UR5Model';
includes_list = {'#include "models/model_ur5.h"', ...
                 'using namespace models;' ...
                 'using namespace config;'};

%% Generate substituded version for M,C,G,Yr
[T_sub, T_var_list] = sym2substitude(T, 'min_occ_tig', 3, 'dof', 6);
[J_sub, J_var_list] = sym2substitude(J, 'min_occ_tig', 3, 'dof', 6);
[Jp_sub, Jp_var_list] = sym2substitude(Jp, 'min_occ_tig', 3, 'dof', 6);
[M_sub, M_var_list] = sym2substitude(M);
[C_sub, C_var_list] = sym2substitude(C);
[G_sub, G_var_list] = sym2substitude(G);
[Y_sub, Y_var_list] = sym2substitude(Y_it);

%% Generate Transformations:
sub_obj = T_sub;
sub_varlist = T_var_list;
varname_out_list = {'T1_0', 'T2_0', 'T3_0', 'T4_0', 'T5_0', 'T6_0', 'Tcm1_0', 'Tcm2_0', 'Tcm3_0', 'Tcm4_0', 'Tcm5_0', 'Tcm6_0'};
vartype_out = {'Eigen::Matrix<%s, %d, %d>&'}; vartype_out_list = repmat(vartype_out, 1, length(varname_out_list));
varname_in_list = {'q%d'};
vartype_in = {'const Eigen::Matrix<%s, %d, %d>&'}; vartype_in_list = repmat(vartype_in, 1, length(varname_in_list));
generate_h_file_sub('transformations', includes_list, namespace, sub_obj, sub_varlist, vartype_out_list, varname_out_list, vartype_in_list, varname_in_list, scalar_type, dof);

%% Gemerate Jacobians
sub_obj = J_sub;
sub_varlist = J_var_list;
varname_out_list = {'J1_0', 'J2_0', 'J3_0', 'J4_0', 'J5_0', 'J6_0', 'Jcm1_0', 'Jcm2_0', 'Jcm3_0', 'Jcm4_0', 'Jcm5_0', 'Jcm6_0'};
vartype_out = {'Eigen::Matrix<%s, %d, %d>&'}; vartype_out_list = repmat(vartype_out, 1, length(varname_out_list));
varname_in_list = {'q%d'};
vartype_in = {'const Eigen::Matrix<%s, %d, %d>&'}; vartype_in_list = repmat(vartype_in, 1, length(varname_in_list));
generate_h_file_sub('jacobians', includes_list, namespace, sub_obj, sub_varlist, vartype_out_list, varname_out_list, vartype_in_list, varname_in_list, scalar_type, dof);

%% Gemerate Time derivative of Jacobians 
sub_obj = Jp_sub;
sub_varlist = Jp_var_list;
varname_out_list = {'J1_0p', 'J2_0p', 'J3_0p', 'J4_0p', 'J5_0p', 'J6_0p', 'Jcm1_0p', 'Jcm2_0p', 'Jcm3_0p', 'Jcm4_0p', 'Jcm5_0p', 'Jcm6_0p'};
vartype_out = {'Eigen::Matrix<%s, %d, %d>&'}; vartype_out_list = repmat(vartype_out, 1, length(varname_out_list));
varname_in_list = {'q%d', 'q%dp'};
vartype_in = {'const Eigen::Matrix<%s, %d, %d>&'}; vartype_in_list = repmat(vartype_in, 1, length(varname_in_list));
generate_h_file_sub('jacobians_p', includes_list, namespace, sub_obj, sub_varlist, vartype_out_list, varname_out_list, vartype_in_list, varname_in_list, scalar_type, dof);

%% Generate Dynamic Model M
sub_obj = M_sub;
sub_varlist = M_var_list;
varname_out_list = {'M'};
vartype_out = {'Eigen::Matrix<%s, %d, %d>&'}; vartype_out_list = repmat(vartype_out, 1, length(varname_out_list));
varname_in_list = {'q%d'};
vartype_in = {'const Eigen::Matrix<%s, %d, %d>&'}; vartype_in_list = repmat(vartype_in, 1, length(varname_in_list));
generate_h_file_sub('matrix_m', includes_list, namespace, sub_obj, sub_varlist, vartype_out_list, varname_out_list, vartype_in_list, varname_in_list, scalar_type, dof);

%% Generate Dynamic Model C
sub_obj = C_sub;
sub_varlist = C_var_list;
varname_out_list = {'C'};
vartype_out = {'Eigen::Matrix<%s, %d, %d>&'}; vartype_out_list = repmat(vartype_out, 1, length(varname_out_list));
varname_in_list = {'q%d', 'q%dp'};
vartype_in = {'const Eigen::Matrix<%s, %d, %d>&'}; vartype_in_list = repmat(vartype_in, 1, length(varname_in_list));
generate_h_file_sub('matrix_c', includes_list, namespace, sub_obj, sub_varlist, vartype_out_list, varname_out_list, vartype_in_list, varname_in_list, scalar_type, dof);

%% Generate Dynamic Model G
sub_obj = G_sub;
sub_varlist = G_var_list;
varname_out_list = {'G'};
vartype_out = {'Eigen::Matrix<%s, %d, %d>&'}; vartype_out_list = repmat(vartype_out, 1, length(varname_out_list));
varname_in_list = {'q%d'};
vartype_in = {'const Eigen::Matrix<%s, %d, %d>&'}; vartype_in_list = repmat(vartype_in, 1, length(varname_in_list));
generate_h_file_sub('matrix_g', includes_list, namespace, sub_obj, sub_varlist, vartype_out_list, varname_out_list, vartype_in_list, varname_in_list, scalar_type, dof);

%% Generate Regressor Yr
sub_obj = Y_sub;
sub_varlist = Y_var_list;
varname_out_list = {'Y'};
vartype_out = {'Eigen::Matrix<%s, %d, %d>&'}; vartype_out_list = repmat(vartype_out, 1, length(varname_out_list));
varname_in_list = {'q%d', 'q%dp', 'qr%dp', 'qr%dpp'};
vartype_in = {'const Eigen::Matrix<%s, %d, %d>&'}; vartype_in_list = repmat(vartype_in, 1, length(varname_in_list));
generate_h_file_sub('matrix_yr', includes_list, namespace, sub_obj, sub_varlist, vartype_out_list, varname_out_list, vartype_in_list, varname_in_list, scalar_type, dof);

%% Generate theta vector
sym_obj_list = th_it;
varname_out_list = {'th'};
vartype_out = {'Eigen::Matrix<%s, %d, %d>&'}; vartype_out_list = repmat(vartype_out, 1, length(varname_out_list));
varname_in_list = {};
vartype_in_list = {};
generate_h_file_sym('matrix_th', includes_list, namespace, sym_obj_list, vartype_out_list, varname_out_list, vartype_in_list, varname_in_list, scalar_type, dof);

function generate_h_file_sym(file_name, includes_list, namespace, sym_obj_list, vartype_out_list, varname_out_list, vartype_in_list, varname_in_list, scalar_type, dof)
%%  generate_h_file(file_name, includes_list, sym_obj_list, vartype_out_list, varname_out_list, vartype_in_list, varname_in_list, n)
%
%   Generates a c++ header file based on given SYMBOLIC object and
%
%   file_name: name of the file to create
%   includes_list: list of #includes
%   namespace: namespace prefix for functions
%   sym_obj_list: symbolic object/object_list that is exported
%   vartype_out_list: c++ type of ouput variable
%   varname_out_list: c++ var name of ouput variable
%   vartype_in_list: c++ type of input variable
%   varname_in_list: c++ var name of input variable
%   scalar_type: c++ scalar type 
%   dof: degree of freedom
%
    fd = fopen([file_name '.h'], 'w');
    guards_begin = sprintf('#ifndef %s_H_\n#define %s_H_\n\n', upper(file_name), upper(file_name));
    includes = [];
    for i = 1 : length(includes_list)
        includes = [includes includes_list{i} newline];
    end
    includes = [includes newline];  
    func = sym2cpp(sym_obj_list, vartype_out_list, varname_out_list, vartype_in_list, varname_in_list, 'scalar_type', scalar_type, 'dof', dof, 'namespace', namespace);
    guards_end = sprintf('\n%s','#endif');
    
    str = [guards_begin includes func guards_end];
    fprintf(fd, '%s', str);
    fclose(fd);
end

function generate_h_file_sub(file_name, includes_list, namespace, sub_obj_list, sub_varlist, vartype_out_list, varname_out_list, vartype_in_list, varname_in_list, scalar_type, dof)
%%  generate_h_file(file_name, includes_list, sym_obj_list, vartype_out_list, varname_out_list, vartype_in_list, varname_in_list, n)
%
%   Generates a c++ header file based on given SUBSTITUTED object
%
%   file_name: name of the file to create
%   includes_list: list of #includes
%   namespace: namespace prefix for functions
%   sub_obj_list: object/object_list that is exported
%   sub_varlist: list of substituded variables
%   vartype_out_list: c++ type of ouput variable
%   varname_out_list: c++ var name of ouput variable
%   vartype_in_list: c++ type of input variable
%   varname_in_list: c++ var name of input variable
%   scalar_type: c++ scalar type 
%   dof: degree of freedom
%
    fd = fopen([file_name '.h'], 'w');
    guards_begin = sprintf('#ifndef %s_H_\n#define %s_H_\n\n', upper(file_name), upper(file_name));
    
    includes = [];
    for i = 1 : length(includes_list)
        includes = [includes includes_list{i} newline];
    end
    includes = [includes newline];
    
    func = substitude2cpp(sub_obj_list, sub_varlist, vartype_out_list, varname_out_list, vartype_in_list, varname_in_list, 'scalar_type', scalar_type, 'dof', dof, 'namespace', namespace);
    guards_end = sprintf('\n%s','#endif');
    
    str = [guards_begin includes func guards_end];
    fprintf(fd, '%s', str);
    fclose(fd);
end




