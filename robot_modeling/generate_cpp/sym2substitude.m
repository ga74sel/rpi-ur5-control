function [str_mat, var_list] = sym2substitude(sym_mat_arr, varargin)
%%  [str_mat, var_list] = sym2substitude(sym_mat)
%
%   substitude common variables within sybolic matrix to speed up computation
%   Important: Call simplify on sym_mat before running this function
%   Important: cannot handle fractions of symbolics
%   
% Inputs:
%   sym_mat_arr: symbolic matrix/cell of symbolic matrices to substitude
%
% Optional:
%   min_occ_tig: minimum number of occurance for tigonometric term before substitution
%   min_occ_term: minimum number of occurance for factor term before substitution
%   dof: degrees of freedom
%   check: resubstitude variabes after substitution to compair with original equ
%
% Outputs:
%	str_mat: string/string cell of substituded equations
%   var_list: list of substituded variabes (paris of subname, term )
%
    % Parse Input
    if( iscell(sym_mat_arr) )
        is_cell = true;
        dof = size(sym_mat_arr{1, 1}, 1);
        K = size(sym_mat_arr, 1);
    else
        is_cell = false;
        dof = size(sym_mat_arr, 1);
        K = size(sym_mat_arr, 3);
    end
    
    p = inputParser;
    addRequired(p, 'sym_mat');
    addOptional(p, 'min_occ_tig', 6);
    addOptional(p, 'min_occ_term', 1);
    addOptional(p, 'dof', dof);
    addOptional(p, 'check', false);
    parse(p, sym_mat_arr, varargin{:});
    min_occ_tig = p.Results.min_occ_tig;
    min_occ_term = p.Results.min_occ_term;
    dof = p.Results.dof;
    check = p.Results.check;
    
    str_mat = cell(1, 1, K);
    var_list = cell(1, 1, K);
    
    for k = 1 : K
        if( is_cell )
            sym_mat = sym_mat_arr{k, 1};
            sym_mat = simplify(sym_mat);
        else
            sym_mat = sym_mat_arr(:,:,k);
        end
       
        % 1. run squeeze functions 
        % setup
        [sym_mat, fac_dict, trig_dict] = squeeze_inital(sym_mat, 'a', dof);
        % find common (...) terms within regressor
        [sym_mat, dict_b] = squeeze_brackets(sym_mat, 'b');
        % find common (...) terms within regressor
        [sym_mat, dict_d] = squeeze_brackets(sym_mat, 'd');
        % clean
        sym_mat = simplify( sym_mat );
              
        % 2. substitude variables, build final var_list        
        dict_list = { trig_dict, fac_dict, dict_b, dict_d };
        [str_mat{1, 1, k}, var_list{1, 1, k}, idx_start] = combine_dict(sym_mat, dict_list, min_occ_tig, min_occ_term);
        
        % 3. if check flag is set compair substitude with inital
        if( check )
            sym_resub = resubstitude(str_mat{1, 1, k}, var_list{1, 1, k}, idx_start);
            if( simplify( expand(sym_resub - sym_mat_arr(:,:,k))) == sym( zeros(size(sym_mat)) ) )
                fprintf('resubstiude coorect at %d\n', k);
            else
                fprintf('error in resubstiude detected at %d\n', k);
            end
        end
    end
end

%%-------------------------------------------------------------------------
% Helper functions
%%-------------------------------------------------------------------------

function [Y, var_list, idx_start] = combine_dict(Y, dict_list, min_occ_trig, min_occ_sub)
%%  [Y, var_list, idx_start] = combine_dict(Y, dict_list, min_occ_trig, min_occ_sub)
%   
%   searche for low occurence of substituded variables. If occurence is
%   lower that min_occ the substituded variable will be resubstituded to
%   reduce the number substituded variables
%
    Y_str = char(Y);
    
    % 1. get size
    n = 0;
    for d = 1 : length(dict_list)
        n = n + dict_list{d}.Count;
    end
    
    % 2. combine every dict into a cell array
    var_list_str = cell(n, 3);
    expr = keys(dict_list{1});
    dict_val = values(dict_list{1});
    for i = 1 : dict_list{1}.Count
        var_list_str(dict_val{i}{2}, :) = { dict_val{i}{1}, expr{i}, dict_val{i}{end} }; % name, expr, cnt
    end
    
    offset = 0;
    for d = 2 : length(dict_list)
        dict_val = values(dict_list{d});
        offset = dict_list{d-1}.Count + offset;
        for i = 1 : dict_list{d}.Count
            var_list_str( offset + dict_val{i}{3}, :) = { dict_val{i}{1}, dict_val{i}{2}, dict_val{i}{end} }; % name, expr, cnt
        end
    end
    
    % 4. replace elments that have low occurrence
    idx_list = [0; dict_list{1}.Count; n];
    reg_expr_list_pre = {'', '(?<=\D)', ''};
    reg_expr_list_post = {'', '(?![sc])', '(?!\d)'};
    min_occ = [0, min_occ_trig, min_occ_sub];
    
    % 5. remove all variables that only appear a few times in the equations
    idx_remove = [];
    idx_start = [0 0 0];
    
    for k = 2 : size(idx_list, 1) % trig, sub terms
        for i = idx_list(k-1)+1 : idx_list(k)
            if ( count_dict_regexpr(Y_str, var_list_str, [reg_expr_list_pre{k}, var_list_str{i,1}, reg_expr_list_post{k}]) <= min_occ(k) )
                [Y_str, var_list_str] = substitude_dict_regexpr(Y_str, var_list_str, [reg_expr_list_pre{k}, var_list_str{i,1}, reg_expr_list_post{k}], var_list_str{i,2});
                idx_remove = [idx_remove, i];
            end  
        end
        idx_start(k) = idx_list(k) - length(idx_remove);
    end
    var_list_str(idx_remove, :) = [];
    idx_start = idx_start(2);
      
    % 3. reduce multiplications by factoring out numbers
    for k = 1 : size(var_list_str, 1)
        var_list_str{k, 2} = factor_out_numbers( var_list_str{k, 2} );
    end
    
    Y_str = str2strcell( Y_str );   
    Y = 'matrix([';
    for i = 1 : size(Y_str, 1)
        Y = [Y, '['];
        for j = 1 : size(Y_str, 2)
            Y = [Y factor_out_numbers( Y_str{i,j} ) ', '];
        end
        Y(end-1:end) = '],';
    end
    Y(end) = ']';
    Y = [Y ')'];
    
    % 5. clearn up 
    Y = strrep(Y, '  ', ' ');
    var_list = cell(size(var_list_str, 1), 2);
    for i = 1 : size(var_list_str, 1)
        var_list(i,:) = { var_list_str{i,1}, strrep(var_list_str{i,2}, '  ', ' ') };
    end
end

function sym_resub = resubstitude(Y_str, var_list_str, start_idx)
    % 1. determine regexr for substiution of trig and terms
    n = size(var_list_str, 1);
    idx_list = [0; start_idx; n];
    reg_expr_list_pre = {'', '(?<=\D)', ''};
    reg_expr_list_post = {'', '(?![sc])', '(?!\d)'};
    
    % 2. substitude everyting
    for k = 2 : size(idx_list, 1) % trig, sub terms
        for i = idx_list(k-1)+1 : idx_list(k)
            
            if( strcmp(var_list_str{i,1}, 'd12'))
                x = 1;
            end
            
            [Y_str, var_list_str] = substitude_dict_regexpr(Y_str, var_list_str, [reg_expr_list_pre{k}, var_list_str{i,1}, reg_expr_list_post{k}], var_list_str{i,2}); 
        end
    end
    
    % 3. convert back to symbolic
    sym_resub = str2sym_mat( Y_str );
end

function n = count_dict_regexpr(Y_str, var_list, name)   
    n = 0;
    for i = 1 : size(var_list, 1)
        n = n + length( regexp([var_list{i,2}], name) );
    end
    n = n + length( regexp(Y_str, name) );
end

function [Y_str, var_list] = substitude_dict_regexpr(Y_str, var_list, name, expr)
    for i = 1 : size(var_list, 1)
        var_list{i,2} = substitude_term([var_list{i,2}], name, expr);
    end
    Y_str = substitude_term(Y_str, name, expr);
end

function [str] = substitude_term(str, name, expr)
%%  substitude_term(str, name, expr)
%   substitude name by given expr
%   add brackets if necessary

    % add space to find variables even at the beginning
    str = [' ' str];

    % term is sum check if brackets are needed
    % 2. find start and stop index of replace name
    [s, e] = regexp(str, name);
    len_expr = length(expr);
    len_str = length(str);

    % 2. check if next character is a '*'
    for i = 1 : length(s)
        % left
        idx_s = s(i)-1;
        c_s = ' ';
        while( idx_s > 0 )
           c_s = str(idx_s);
           if c_s ~= ' '
               break;
           end
           idx_s = idx_s - 1;
        end

        % right
        idx_e = e(i)+1;
        c_e = ' ';
        while( idx_e <= len_str)
           c_e = str(idx_e);
           if c_e ~= ' '
               break;
           end
           idx_e = idx_e + 1;
        end

        % replace
        if( c_s == '*' || c_s == '-' || c_e == '*' || c_e == '^' )
            expr_ = ['(' expr ')'];
            len_ = len_expr + 2;
        else
            expr_ = expr;
            len_ = len_expr;
        end
        str = replaceBetween(str, s(i), e(i), expr_);
        len_old = e(i) - s(i);
        s = s + len_ - len_old - 1;
        e = e + len_ - len_old - 1;
        len_str = length(str);
    end
    % remove space at the biginning
    str = str(2:end);
end 

function [Y_sym, fac_dict, trig_dict_] = squeeze_inital(Y, prefix, dof)
%%  squeeze_Yr_inital(Y)
%   
%	replace all trigonometric terms with variables
%	replace all factors infront of trigonometic terms with variabels
%   Input:
%       Y: symbolic matrix of regressor
%       prefix = string, used as replacement
%   Ouput:
%       Y_sym: symbolic matrix of new regressor
%       fac_dict: dictionary with mapping between factors and subsitutes
%                 key=expression, value{1}=substitude, value{2}=cnt
%       trig_dict: dictionary with mapping between trigonometric terms and subsitutes
%                 key=descriptor, value{1}=substitude, value{2}=expression
%   Operation:
%       L1*q1rp*q1*cos(q1)*cos(q2) -> a0*c1c2
%
    [n, m] = size(Y);
    Y_sym = sym(zeros(n, m), 'r');
    n_flag = false;
    
    fac_dict = containers.Map('KeyType','char','ValueType','any');      % dict for pre factors: a0 = L1*q1p*...
    trig_dict = containers.Map('KeyType','char','ValueType','any');   	% dict for trig terms: c1c2 = cos(q1)*cos(q2)*...
    reg_dict_m = {};
    for i = 1 : n
        for j = 1 : m
            reg_dict_m{i,j} = containers.Map('KeyType','char','ValueType','any'); % dict for regressor terms: a0*c1c2*...
        end
    end
    
    % 2. setup dict for trigonometric functions
    TRIG_NAME = {'s', 'c'};
    TRIG_VAL = {'sin', 'cos'};
    JOINT_POS = 'q';
    D = eye(2 * dof);
    for k = 1 : 2
        for i = 1 : dof
            discriptor = D(dof * (k-1) + i,:)';
            name = [TRIG_NAME{k}, num2str(i)];
            val = [TRIG_VAL{k}, '(', JOINT_POS, num2str(i), ')'];
            trig_dict(getkey(discriptor)) = {name, val, discriptor, 0, dof * (k-1) + i };
        end
    end
    
    % 3. setup dict for pre factors
    JOINT_VELO = 'q%dp';
    JOINT_VELO_REF = 'qr%dp';
    k = 1;
    for i = 1 : n
        for j = 1 : n
            name = [prefix, num2str(k)];
            val = [sprintf(JOINT_VELO, i), '*', sprintf(JOINT_VELO_REF, j)];
            fac_dict(val) = {name, val, k, 0 };
            k = k+1;
        end
    end
    
    % 3. run over matrix elements and replace trigonometric terms and
    % multiplication pre fractors
    for i = 1 : n
        for j = 1 : m
            equ = strsplit( char(vpa(expand(Y(i,j)))), ' ');

            for k = 1 : length(equ)
                term = equ{k};

                % 1. remove useless terms
                if strcmp(term(1), '-')
                    n_flag = true;
                    term(1) = '';
                end
                if isempty(term) || strcmp(term,'+') || strcmp(term,'0')
                    continue
                end

                % 2. seperate prefactor form trigonometric part
                TRIG = {'sin', 'cos'};
                [num, fac, trig] = seperate_term_elementary( term, TRIG );
                if( num == 1.0 )
                    num = nan;
                end
                            
                % 3. substitude factor
                [fac_dict, fac_sub] = substitude_factor(fac, fac_dict, prefix, '*', true);
                
                % 4. substitude trigonometric part
                if ~isempty(trig)
                    d = decriptor_trig(trig, dof);
                    [trig_dict, trig_sub] = substitude_trig(d, trig_dict);
                else
                    trig_sub = '';
                end

                % 5. insert into modified regressor ( cluster based on fac_sub, not trig terms -> 1)
                reg_dict_m = insert_to_Yr(reg_dict_m, i, j, n_flag, num, fac_sub, trig_sub, 1);
                n_flag = false;
            end
        end
    end
    
    % 4. form the new regressor, convert to symbolic
    Y_str = form_Yr(reg_dict_m, n, m);
    for i = 1 : n
        for j = 1 : m
            if( ~isempty( Y_str{i,j} ) )                    
                Y_sym(i, j) = str2sym( Y_str{i,j} );
            end
        end
    end
    
    %% 5. remove descriptor form trig_dictionary
    dict_val = values(trig_dict);
    trig_dict_ = containers.Map('KeyType','char','ValueType','any');
    for i = 1 : trig_dict.Count
        trig_dict_(dict_val{i}{2}) = { dict_val{i}{1}, dict_val{i}{5}, dict_val{i}{4} };
    end
    
end

function [Y_sym, bracket_dict] = squeeze_brackets( Y, prefix )
%%  squeeze_Yr_brackets(str)
%   
%	replace all brackets within Y matrix with a new varibale and factors
%	out the new resulting terms
%   Input:
%       Y: symbolic matrix of regressor
%       prefix = string, used as replacement
%   Ouput:
%       Y_sym: symbolic matrix of new regressor
%       bracket_dict: dictionary with mapping between expression and subsitute
%                     key=expression, value{1}=substitude, value{2}=cnt
%   Operation:
%       a0*(expression) + b0*(expression) -> prefix# * (a0 + b0)
%
    [n, m] = size( Y );
    Y_sym = sym(zeros(n, m), 'r');
    n_flag = false;
    in_bracket = false;
    in_bracket_term  = '';
    
    bracket_dict = containers.Map('KeyType','char','ValueType','any');
    reg_dict_m = {};
    for i = 1 : n
        for j = 1 : m
            reg_dict_m{i,j} = containers.Map('KeyType','char','ValueType','any');
        end
    end

    % 3. run over matrix elements and replace trigonometric terms and
    % multiplication pre fractors
    for i = 1 : n
        for j = 1 : m
            equ = strsplit( char(Y(i,j)), ' ');
            
            for k = 1 : length(equ)
                term = equ{k};
                
                % 1. check if term is part of bracket
                if( in_bracket == false && contains(term, '(') )
                    in_bracket = true;
                end
                if( in_bracket == true && contains(term, ')') )
                    in_bracket = false;
                    term = [in_bracket_term, term];
                    in_bracket_term = '';
                end
                if( in_bracket == true )
                    in_bracket_term = [in_bracket_term, term];
                    continue;
                end

                % 2. remove useless terms
                if strcmp(term(1), '-')
                    n_flag = true;
                    term(1) = '';
                end
                if isempty(term) || strcmp(term,'+') || strcmp(term,'0')
                    continue
                end

                % 3. seperate prefactor form bracket part
                % a12*(...) -> fac=a12, bracket=(...)
                [fac, bracket] = seperate_term_bracket( term );
              
                % 4. substitude bracket
                [bracket_dict, bracket_sub] = substitude_factor(bracket, bracket_dict, prefix, '', true);

                % 5. insert into modified regressor
                reg_dict_m = insert_to_Yr(reg_dict_m, i, j, n_flag, '', fac, bracket_sub, 2);
                n_flag = false;
            end
        end
    end
    
    % 4. form the new regressor, convert to symbolic
    Y_str = form_Yr(reg_dict_m, n, m);
    for i = 1 : n
        for j = 1 : m
            if( ~isempty( Y_str{i,j} ) )
                Y_sym(i, j) = str2sym( Y_str{i,j} );
            end
        end
    end  
end

function dict_m = insert_to_Yr(dict_m, i, j, n_flag, num, term_a, term_b, based_on)
%%  insert_to_Yr(reg_dict_m, i, j, n_flag, num, fac_sub, trig_sub)
%
%   insert term into regessor matrix
%   form: +/- num*fac_sub*trig_sub
%   information are saved in reg_dict_m, matrix of dictionaries
%
    % cluster based on fac instead of trig term, since they seem to apear
    % more often in the equation of Y(i,j)
      
    if(based_on == 1)
        key = term_a;
        list = {n_flag, num, term_b};   % flag, num, term
    else
        key = term_b;
        list = {n_flag, num, term_a};   % flag, num, term
    end
    
    if( isempty( key ) )
        key = '1';  % insert as 1 since 1 * x = x
    end
    
    if( dict_m{i,j}.isKey( key ) )
        % allready exsists
        elem = dict_m{i,j}(key);    % extract element for given key
        elem(end+1, :) = list;      % add new term to element
        dict_m{i,j}(key) = elem;    % update element
    else
        dict_m{i, j}(key) = list; % init new element: list, cnt
    end 
end

function Y = form_Yr(dict_m, n, m)
%%  insert_to_Yr(Y, i, j, n_flag, num, fac_sub, trig_sub)
%
%   insert term into regessor matrix
%   form: +/- num*fac_sub*trig_sub
%
    Y = cell(n,m);
    
    for i = 1 : n       % loop rows
        for j = 1 : m   % loop cols
            
            % for each dictionary create a term
            dict_keys = keys(dict_m{i,j});
            dict_val = values(dict_m{i,j});
            
            for k = 1 : dict_m{i,j}.Count   % loop pre factor
                list = dict_val{k};
                cnt = size(list, 1);
                
                if( cnt == 1 )
                    str = combine_term(list{1}, list{2}, dict_keys{k}, list{3});
                else
                    str = ['+', dict_keys{k}, '*('];
                  	for l = 1 : cnt
                        str = [str, combine_term(list{l,1}, list{l,2}, '', list{l,3})];
                    end
                    str = [str, ')'];
                end
                   
                % add term to regressor
                if isempty(Y{i,j})
                    Y{i,j} = str;
                else
                    Y{i,j} = [Y{i,j}, str];
                end
            end
        end
    end 
end

function str = combine_term( n_flag, num, a, b )
%%  combine_term( n_flag, num, a, b )
%
%   combine terms into single string
%   Operation:
%       +/- num*a*b
%   
%
    str = '';
    if ~isnan(num)
        str = num2str(num);
    end
         
    if( ~isempty( a ) )
        if( isempty(str) )
            str = a;
        else
            str = [str, '*', a];
        end
    end
    
    if( ~isempty( b ) )
        if( isempty(str) )
            str = b;
        else
            str = [str, '*', b];
        end
    end
    
    % empty terms not posible
    if(isempty( str) )
        str = '1';
    end
    
    if n_flag == 1
        str = [' - ',str];
    else
        str = [' + ',str];
    end
end

function [dict, str] = substitude_trig(d, dict)
%%  substitude_trig(str)
%   
%	replace the factor with name of fac_dict or expand fac_dict
%   
    e = inf;
    delta_idx = 0;
    k = keys(dict) ;
    v = values(dict) ;

    for i = 1:dict.Count
        err = d - v{i}{3};     % descriptor error
        ndelta = sum(abs(err));
        % perfect match
        if( ndelta == 0 )
            v{i}{4} = v{i}{4} + 1;  % increment cnt
            dict(k{i}) = v{i};
            str = v{i}{1};          % get name
            return
        end
        % seach closest match
        if( ndelta < e && sum( err < 0 ) == 0 )
            e = ndelta;
            delta_idx = i;
            delta = err;
        end  
    end

    delta_key = getkey(delta);   
    if( ~dict.isKey( delta_key ))
        fprintf('warning: substitude_trig(): no key found\n'); % key not found...
        [dict, ~] = substitude_trig(delta, dict);
    end  
    
    % get elements form dict
    parent = dict(k{delta_idx});
    child = dict(delta_key);

    % construct new element
    str = [parent{1}, child{1}];
    val = [parent{1}, '*' child{1}];
    d = parent{3} + delta;
    dict(getkey(d)) = { str, val, d, 1, dict.Count + 1};

    % update parent
    parent{4} = parent{4} + 1;
    dict(k{delta_idx}) = parent;  
end

function d = decriptor_trig(str, n_joints)
    TRIG = {'sin', 'cos'};
    d = zeros(2*n_joints, 1);
    
    for k = 1:2
        prod = strfind(str, TRIG{k});
        for i = 1 : length(prod)
            idx = prod(i) + 3;
            while(~isnum( str(idx) ))
                idx = idx+1;
            end
            temp = idx;
            while(str(idx) ~= ')')
                idx = idx+1;
            end
            q = str2double( str(temp:idx-1) );
            d(q + (k-1) * n_joints ) = 1;
            
            if( idx+1 < length(str) )
                if( str(idx+1) == '^')
                   d(q + (k-1) * n_joints ) = str2double( str(idx+2) );
                end
            end
        end
    end
end

function [dict, str_sub] = substitude_factor(str, dict, prefix, must_contain, find_subfactor)
%%  sub_factor(str)
%   
%	replace the factor with name of fac_dict or expand fac_dict if therm is
%	not found
%   Input:
%       dict = { substitude, cnt }
%       prefix = string, used as replacement
%       must_contain = string, must be present in str otherwise not added
%       find_same = bool, find if one factor is a sub factor of another
%   Operation:
%       str -> prefix#
%
    if(~contains(str, must_contain) || isempty(str))
        str_sub = str;
        return
    end
    
    if( dict.isKey(str) )
        elem = dict(str);
        str_sub = elem{1};      % get name
        
        elem{end} = elem{end} + 1;  % update cnt
        dict(str) = elem;
        return
    else
        % search for common terms within the each factor
        str_subexpr = str;
        if( find_subfactor )
            dict_keys = keys(dict);
            dict_val = values(dict);
            for i = 1 : dict.Count
                if contains( str, dict_keys{i} )
                    dict_val{i}{end} = dict_val{i}{end} + 1; % update count
                    dict(dict_keys{i}) = dict_val{i};
                    
                    regexp = regexptranslate('escape', dict_keys{i});
                    str_subexpr = regexprep([' ', str], ['(?<=\D)', regexp], dict_val{i}{1});
                    break;
                end
            end
        end
        str_sub = strcat(prefix, num2str(dict.Count+1));
        dict(str) = { str_sub, str_subexpr, dict.Count+1, 1}; % new elem: substitude name, substitude expr, idx, cnt
    end
end

function [fac, bracket] = seperate_term_bracket(str)
%%  seperate_term_bracket(str)
%   
%   seperates a term into a factor and a bracket part
%
    n = length(str);
    
    bracket_idx = strfind(str, '(') - 2;
    if( isempty( bracket_idx) )
       bracket_idx = n;
    end
    
    fac = str(1:bracket_idx);
    bracket = str(bracket_idx+2:n);
end

function [num, fac, trig] = seperate_term_elementary(str, TRIG)
%%  seperate_term_elementary(str)
%   
%   seperates str into a term trig with trigonometrics, constatns and in
%   str, which contains part of a possible new theta component
%   2*q1pp*cos(q1)*cos(q2) -> num=2, fac=q1pp, trig=cos(q1)*cos(q2)
%
    n = length(str);
    
    fac_idx = strfind(str, '*') - 1;
    if( isempty( fac_idx) )
       fac_idx = n;
    end
    
    trig_idx = n;
    if(~isempty(TRIG))
        for i = 1 : 2
            idx = strfind(str, TRIG{i});
            if(~isempty( idx ))
                trig_idx = min(trig_idx, idx(1))-2;
            end
        end
    end
    
    idx = 1;
    num = str2double(str(idx:fac_idx));
    if (~isnan( num ))
        idx = fac_idx+2;
    end
    
    fac = str(idx:trig_idx);
    trig = str(trig_idx+2:n);
end

function str = factor_out_numbers( str )
    if( ischar(str) )
        equ = str2sym( str );
    else
        equ = str;
    end
    equ = strsplit( char(expand( equ ) ), ' ');
    n_flag = false;
    fac_smaller = {}; fac_bigger = {}; fac_non  = {};
     
    % 1. build a cell array that contains sign, number, factor
    n = 0;
    for k = 1 : length(equ)
        term = equ{k};
        if isempty(term)
            continue
        end

        % 1. remove useless terms
        if strcmp(term(1), '-')
            n_flag = true;
            term(1) = '';
        end
        if isempty(term) || strcmp(term,'+') || strcmp(term,'0')
            continue
        end

        % 2. seperate into number and factor
        [num, fac] = seperate_term_elementary( term, '' );
        if( isnan(num) )
            num = 1;
        end
        if( num == 1 )
            fac_non(end+1, :) = {n_flag, num, fac};
        elseif( num > 1)
            fac_bigger(end+1, :) = {n_flag, num, fac};
        elseif( num < 1)
            fac_smaller(end+1, :) = {n_flag, num, fac};
        end
        n_flag = false;
        n = n + 1;
    end
    if( n <= 1 )
        return;
    end
    
    % 3. build new simplified str
    str = [];
    str = [str build_str(fac_non)];
    str = [str build_str(fac_bigger)];
    str = [str build_str(fac_smaller)];
    
    % 4. clean up
    str = strrep(str, '( + ', '(');
    if( str(2) == '+' )
        str(2) = [];
    end
    
    function str = build_str(fac_cell)
        str = [];
        substr = [];
        
        if( isempty(fac_cell) )
            return
        end
        
        % 2. get all numbers form cell array
        num = cell2mat( fac_cell(:, 2) );
        
        if( num(1) == 1)
            % no number factors found, just combines into one string
            for i = 1 : length(num)
                substr = [substr, combine_term(fac_cell{i, 1}, '', '', fac_cell{i, 3})];
            end
            str = [str char( collectterms( str2sym(substr), 4) ) ];
        else
            % number factors found, sort them based on magnitude
            if( num(1) < 1 )
                [num, sort_idx] = sort(num, 'descend');
            else
                [num, sort_idx] = sort(num);
            end
            num_unique = unique(num, 'stable');
            n_flag_cell = fac_cell(sort_idx, 1);
            term_cell = fac_cell(sort_idx, 3);
            % construct a string that factors out all numbers
            div = 1;
            close = length(num_unique);
            if(~isempty(num_unique))
                for i = 1 : length(num_unique)
                    fac = num_unique(i) / div;
                    idx = find( num == num_unique(i) );
                    if( length(idx) == 1)
                        str = [str, combine_term(n_flag_cell{idx(1)}, fac, '', term_cell{idx(1)})];
                        close = close-1;
                    else   
                        str = [str ' + ' num2str(fac) '*(']; 
                        substr = '';
                        for j = 1 : length( idx )
                           substr = [substr, combine_term(n_flag_cell{idx(j)}, '', '', term_cell{idx(j)})];
                        end
                        div = div * fac;
                        str = [str char( collectterms( str2sym(substr), 4) ) ];
                    end
                end
                str = [str repmat(')', 1, close)];
            end         
        end
    end
end

function equ = collectterms( equ, n_max )
%%  equ = collectterms( equ )
%   
%   collect common terms in equ to reduce number of multiplications
%   n_max is the maximum number of factors used in collect()
%   a1*b1 + a2*b2 + a1*b3  ->  a1(b1+b3) + a2*b2

    % extract all factors
    equ = expand( equ );
    str = strsplit( char(expand( equ ) ), ' ');
    str = str(~contains(str, {'+', '-'}));
    if( length(str) <= 1 )
        return
    end
    fac_cell = [];
    for i = 1 : length(str)
        fac_cell = [fac_cell, strsplit( str{i}, '*')];
    end
              
    % determine frequency of factors
    fac_cell = fac_cell(~contains(fac_cell, '('));
    [colleced,~,labels] = unique(fac_cell,'stable');
    counts = histc(labels,1:max(labels));
    idx1 = find(counts > 1);
    colleced = colleced(idx1);
    if( isempty(idx1) )
        return
    elseif(length(idx1) > 1)
        [~, idx2] = sort(counts(idx1), 'descend');
        colleced = colleced(idx2(1:min(end,n_max)));
    end
    colleced = strrep(colleced, '^2', '');
    
    n = length(strfind(char(equ), '*'));
    equ = collect( equ, colleced);
    if( n == length(strfind(char(equ), '*')) )
        equ = collect( equ, colleced{1});
    end
end

function [str_cell, n, m] = str2strcell( str )
%%  str2sym_matrix(str)
%   
% convert a str containing a symbolic matrix into a cell array, containing
% strings of each element in a different cell
%
    % get dimensions
    row_start = strfind(str,'[');
    row_stop = strfind(str, ']');
    elems = strfind(str, ',');

    n = size(row_start, 2) - 1;
    m = (size(elems, 2) + 1) / n;
    
    str_cell = cell(n, m);

    % matrix
    for i = 1 : n
        row = str(row_start(i+1)+1:row_stop(i)-1);
        elem = [0, strfind(row, ','), length(row)+1];  
        for j = 1 : m
            str_cell{i, j} = row( elem(j)+1 : elem(j+1)-1);
        end
    end
end

function sym_mat = str2sym_mat( str )
%%  str2sym_matrix(str)
%   
% convert a str containing a symbolic matrix into a cell array back into a
% symbolic matrix
%
    % get dimensions
    row_start = strfind(str,'[');
    row_stop = strfind(str, ']');
    elems = strfind(str, ',');

    n = size(row_start, 2) - 1;
    m = (size(elems, 2) + 1) / n;
   
    sym_mat = sym(zeros(n, m)); 

    % matrix
    for i = 1 : n
        row = str(row_start(i+1)+1:row_stop(i)-1);
        elem = [0, strfind(row, ','), length(row)+1];  
        for j = 1 : m
            sym_mat(i, j) = str2sym( row( elem(j)+1 : elem(j+1)-1) );
        end
    end
end

function res = isnum(c)
    res = (c <= '9' && c >= '0');
end

function k = getkey( v )
    k = char(hlp_serialize(v));
end
